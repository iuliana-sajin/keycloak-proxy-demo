-- note that utf8mb4 character set does not work due to different storage requirements to utf8 character set
-- link to official documentation https://www.keycloak.org/docs/latest/server_installation/#mysql-database
CREATE DATABASE IF NOT EXISTS keycloak CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

GRANT ALL PRIVILEGES ON keycloak.* TO 'keycloak'@'%';

USE keycloak;

-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: keycloak
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADMIN_EVENT_ENTITY`
--

DROP TABLE IF EXISTS `ADMIN_EVENT_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ADMIN_EVENT_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `ADMIN_EVENT_TIME` bigint DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `OPERATION_TYPE` varchar(255) DEFAULT NULL,
  `AUTH_REALM_ID` varchar(255) DEFAULT NULL,
  `AUTH_CLIENT_ID` varchar(255) DEFAULT NULL,
  `AUTH_USER_ID` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `RESOURCE_PATH` text,
  `REPRESENTATION` text,
  `ERROR` varchar(255) DEFAULT NULL,
  `RESOURCE_TYPE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADMIN_EVENT_ENTITY`
--

LOCK TABLES `ADMIN_EVENT_ENTITY` WRITE;
/*!40000 ALTER TABLE `ADMIN_EVENT_ENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADMIN_EVENT_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ASSOCIATED_POLICY`
--

DROP TABLE IF EXISTS `ASSOCIATED_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ASSOCIATED_POLICY` (
  `POLICY_ID` varchar(36) NOT NULL,
  `ASSOCIATED_POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`POLICY_ID`,`ASSOCIATED_POLICY_ID`),
  KEY `IDX_ASSOC_POL_ASSOC_POL_ID` (`ASSOCIATED_POLICY_ID`),
  CONSTRAINT `FK_FRSR5S213XCX4WNKOG82SSRFY` FOREIGN KEY (`ASSOCIATED_POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`),
  CONSTRAINT `FK_FRSRPAS14XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ASSOCIATED_POLICY`
--

LOCK TABLES `ASSOCIATED_POLICY` WRITE;
/*!40000 ALTER TABLE `ASSOCIATED_POLICY` DISABLE KEYS */;
INSERT INTO `ASSOCIATED_POLICY` VALUES ('817ac65c-c0b0-4d46-8091-d6a89f8371cb','f57573f1-9f27-4f2d-9399-aa353efd077d');
/*!40000 ALTER TABLE `ASSOCIATED_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATION_EXECUTION`
--

DROP TABLE IF EXISTS `AUTHENTICATION_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATION_EXECUTION` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `AUTHENTICATOR` varchar(36) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `FLOW_ID` varchar(36) DEFAULT NULL,
  `REQUIREMENT` int DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  `AUTHENTICATOR_FLOW` bit(1) NOT NULL DEFAULT b'0',
  `AUTH_FLOW_ID` varchar(36) DEFAULT NULL,
  `AUTH_CONFIG` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_EXEC_REALM_FLOW` (`REALM_ID`,`FLOW_ID`),
  KEY `IDX_AUTH_EXEC_FLOW` (`FLOW_ID`),
  CONSTRAINT `FK_AUTH_EXEC_FLOW` FOREIGN KEY (`FLOW_ID`) REFERENCES `AUTHENTICATION_FLOW` (`ID`),
  CONSTRAINT `FK_AUTH_EXEC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATION_EXECUTION`
--

LOCK TABLES `AUTHENTICATION_EXECUTION` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATION_EXECUTION` DISABLE KEYS */;
INSERT INTO `AUTHENTICATION_EXECUTION` VALUES ('02f474b0-8fe9-4df2-b0db-9cca34927a03',NULL,NULL,'local','b42d9386-412c-48e1-ba6f-627c82945792',1,20,_binary '','754bbff4-ef9b-49cd-a145-40c8516b31a1',NULL),('05b21f8c-6d9d-4bbd-a4ab-3f384aec4f0b',NULL,'registration-user-creation','local','785bcc6b-097c-4aef-82f2-24b420257d62',0,20,_binary '\0',NULL,NULL),('0a2aae6c-80e8-478c-81e6-570c4e2be630',NULL,NULL,'master','febe769c-6716-4ebf-bce7-1ea605c2790a',2,20,_binary '','360ecbea-2f53-471c-a1cb-6ad9f0a17e9f',NULL),('0ebaef16-f557-4573-b26a-e3563d790694',NULL,'conditional-user-configured','local','754bbff4-ef9b-49cd-a145-40c8516b31a1',0,10,_binary '\0',NULL,NULL),('1289ed14-977c-4050-a35c-f4a064ce7628',NULL,NULL,'master','ca058d70-a38f-429e-94ca-55719ceef211',1,40,_binary '','0b5b0720-6c94-4897-95e2-0016f45b03bb',NULL),('133148e9-a9f0-4a21-8bfc-74c3ab637979',NULL,'auth-otp-form','local','754bbff4-ef9b-49cd-a145-40c8516b31a1',0,20,_binary '\0',NULL,NULL),('1397325f-f7cf-4783-9cd3-c6a44465f606',NULL,'reset-credentials-choose-user','master','ca058d70-a38f-429e-94ca-55719ceef211',0,10,_binary '\0',NULL,NULL),('1457ee36-31f3-4de6-810e-1e9917dd95b5',NULL,'client-secret-jwt','master','449265ae-aefd-44df-a3b2-4e76f490e099',2,30,_binary '\0',NULL,NULL),('1a5145f5-45b1-4661-b7d1-ec8bb92523d4',NULL,'basic-auth','master','fe5185af-17aa-4360-b7da-5110bbeb6b6a',0,10,_binary '\0',NULL,NULL),('1a7d7cb2-8d08-4793-b66b-caae59a1c630',NULL,'basic-auth','local','60bb0421-6321-4d9d-b034-8a03500dc244',0,10,_binary '\0',NULL,NULL),('1e20740d-930c-4fe3-89c4-26c5ec1e90c4',NULL,'registration-recaptcha-action','master','98e837ae-15b0-4ae3-b493-74286fde4d71',3,60,_binary '\0',NULL,NULL),('1ed58c94-1043-47cb-a19f-0213a7d75b39',NULL,'docker-http-basic-authenticator','master','430755bf-e16a-45af-a87c-5c60a2e7113a',0,10,_binary '\0',NULL,NULL),('1fe49712-83d7-4e02-a66a-2ad95c2a2bfe',NULL,'conditional-user-configured','local','0c4f4722-54cb-4d36-8301-94d88ec08990',0,10,_binary '\0',NULL,NULL),('1ff6dac1-225c-44b3-92c3-c05667a809bf',NULL,NULL,'local','c4f85414-c975-4601-af4f-6de37a42ddc6',0,20,_binary '','60bb0421-6321-4d9d-b034-8a03500dc244',NULL),('20294b6a-21c1-4ede-8b8c-7e9b52d0f30f',NULL,'client-jwt','local','a979bb85-af13-45a5-8c7b-4bcc75e896ce',2,20,_binary '\0',NULL,NULL),('220f3f2d-0ee0-4005-9121-f3627a9b2cb3',NULL,'registration-user-creation','master','98e837ae-15b0-4ae3-b493-74286fde4d71',0,20,_binary '\0',NULL,NULL),('221b751d-ab44-40e3-aeda-d2f6883d3a37',NULL,NULL,'local','e1a8d483-30bc-4667-a611-2ab3330af85d',0,20,_binary '','dcd4faf8-415a-4e01-8bf0-9f3600d3800e',NULL),('24804a55-59eb-457f-91ce-66a7d72a7822',NULL,'http-basic-authenticator','master','ee3eda91-f5d2-4a82-b5b7-6355c2d84fed',0,10,_binary '\0',NULL,NULL),('25e8f4b1-e363-43c8-b65d-d8f27c611afc',NULL,'reset-credential-email','master','ca058d70-a38f-429e-94ca-55719ceef211',0,20,_binary '\0',NULL,NULL),('29b87ee1-7b0f-40cc-a781-61a234dff1f4',NULL,'direct-grant-validate-password','master','6c9584c6-9c33-4f4c-b180-81d3932b27f3',0,20,_binary '\0',NULL,NULL),('2a5ea1c4-44fd-4101-ba2f-77405f7e1777',NULL,NULL,'master','6c9584c6-9c33-4f4c-b180-81d3932b27f3',1,30,_binary '','a819b946-ab38-44c8-a8c5-840a7c574b15',NULL),('2b6e7745-692d-4a9f-bd94-a6eda754e130',NULL,'registration-profile-action','local','785bcc6b-097c-4aef-82f2-24b420257d62',0,40,_binary '\0',NULL,NULL),('2d31ff67-808f-4e1d-84f8-c951d6b9f331',NULL,'idp-confirm-link','local','e1a8d483-30bc-4667-a611-2ab3330af85d',0,10,_binary '\0',NULL,NULL),('311cd797-7740-4b70-8cad-fd8e9d6e3926',NULL,'idp-username-password-form','local','720bd12b-169c-46d1-9da4-96e268520266',0,10,_binary '\0',NULL,NULL),('3234f35a-1c6d-431c-963b-920cd0ee21b2',NULL,NULL,'local','6e2d142c-752e-41d3-8080-f12664cdb0b4',2,30,_binary '','b42d9386-412c-48e1-ba6f-627c82945792',NULL),('3362723b-fe62-4f1d-9973-6448b91c9aac',NULL,NULL,'master','360ecbea-2f53-471c-a1cb-6ad9f0a17e9f',0,20,_binary '','eea1631e-a320-43a9-a58c-c48f13e19458',NULL),('3376ae92-943d-4368-b527-f8c8ba93c096',NULL,'conditional-user-configured','master','0b5b0720-6c94-4897-95e2-0016f45b03bb',0,10,_binary '\0',NULL,NULL),('38c2cfd6-917c-42f9-9dd1-45a3244ba018',NULL,'auth-cookie','master','206a6fd5-e35a-4c13-81fa-55c9ec9aadc2',2,10,_binary '\0',NULL,NULL),('3971a2f1-b353-4c9b-ba22-e0b0cbf0c604',NULL,'reset-password','master','ca058d70-a38f-429e-94ca-55719ceef211',0,30,_binary '\0',NULL,NULL),('3ba0d092-7e90-4e26-8b4a-31c698083df2',NULL,'idp-review-profile','local','236736ae-fac2-4877-977f-e619514a03ec',0,10,_binary '\0',NULL,'e1357f08-fe63-417d-ae79-93c9ae07ea00'),('3e23c445-c757-4b95-8594-b1a179ba01e6',NULL,'conditional-user-configured','master','a819b946-ab38-44c8-a8c5-840a7c574b15',0,10,_binary '\0',NULL,NULL),('3f61c7c5-c639-4298-9224-80500ddf7f1a',NULL,'registration-page-form','local','71b3a190-38a6-49fd-b5db-3414e00e8765',0,10,_binary '','785bcc6b-097c-4aef-82f2-24b420257d62',NULL),('40d068ba-bdb8-4cdf-ae88-a242ac79f58b',NULL,'direct-grant-validate-username','master','6c9584c6-9c33-4f4c-b180-81d3932b27f3',0,10,_binary '\0',NULL,NULL),('4d3e6841-7190-4ca1-b73e-edaabc8e2f93',NULL,NULL,'local','dcd4faf8-415a-4e01-8bf0-9f3600d3800e',2,20,_binary '','720bd12b-169c-46d1-9da4-96e268520266',NULL),('51109db9-3a43-417c-8113-9725488a31d1',NULL,NULL,'master','eea1631e-a320-43a9-a58c-c48f13e19458',2,20,_binary '','35250a43-53ad-4adf-90f7-22d73c80897a',NULL),('54ad1807-8f58-48a8-8a79-f52d5185c13e',NULL,NULL,'local','236736ae-fac2-4877-977f-e619514a03ec',0,20,_binary '','5c2cbcd5-1ccf-4676-b871-eea95d6e4d51',NULL),('560b939a-dca0-4891-8b43-4ed76edbd15d',NULL,'auth-cookie','local','6e2d142c-752e-41d3-8080-f12664cdb0b4',2,10,_binary '\0',NULL,NULL),('5798b40e-8ffc-4646-a7c3-2bc65ffa00b0',NULL,'conditional-user-configured','master','e38b8513-b1b5-45c2-90db-ef8675b0f733',0,10,_binary '\0',NULL,NULL),('5eabff92-a79e-4241-a5c8-e8bca2a2b2a9',NULL,'idp-email-verification','master','eea1631e-a320-43a9-a58c-c48f13e19458',2,10,_binary '\0',NULL,NULL),('5ece4619-fc2d-478c-9941-9ba0e4766d13',NULL,'idp-create-user-if-unique','local','5c2cbcd5-1ccf-4676-b871-eea95d6e4d51',2,10,_binary '\0',NULL,'79a0872e-3407-4789-8365-1ae826e3e01e'),('5ef4976d-6fb8-4620-b26c-e013bb2c069e',NULL,'direct-grant-validate-otp','local','d2923165-7a1b-426f-99c4-cb5c22e98c1d',0,20,_binary '\0',NULL,NULL),('6d91c380-60f7-4722-aedd-128da034a4d8',NULL,'http-basic-authenticator','local','0baacc05-1332-4d3d-8f4a-b0dc74bc0d98',0,10,_binary '\0',NULL,NULL),('6f47a612-bb45-4579-8719-985fb38e12e1',NULL,'auth-spnego','master','206a6fd5-e35a-4c13-81fa-55c9ec9aadc2',3,20,_binary '\0',NULL,NULL),('6fc639c8-7485-489b-a092-347305879fbc',NULL,'reset-credentials-choose-user','local','0df0da2a-7ae2-4b9c-a6cc-74de03562634',0,10,_binary '\0',NULL,NULL),('7162e62b-7b05-4f0f-baa7-9a65fb2127dd',NULL,'conditional-user-configured','local','51dc1ff8-f5e9-4563-9868-e60c25056044',0,10,_binary '\0',NULL,NULL),('73640c90-7386-4fc8-84e7-4d4d4b5704b1',NULL,'idp-email-verification','local','dcd4faf8-415a-4e01-8bf0-9f3600d3800e',2,10,_binary '\0',NULL,NULL),('75572389-6c6a-4f21-b9f5-9bb2e064443f',NULL,'registration-password-action','local','785bcc6b-097c-4aef-82f2-24b420257d62',0,50,_binary '\0',NULL,NULL),('77070923-a61d-4625-812f-a8a863d9ce5a',NULL,'client-secret-jwt','local','a979bb85-af13-45a5-8c7b-4bcc75e896ce',2,30,_binary '\0',NULL,NULL),('7b030c3c-8bf5-4bf2-80f8-e3564b617946',NULL,'client-jwt','master','449265ae-aefd-44df-a3b2-4e76f490e099',2,20,_binary '\0',NULL,NULL),('86bfbf9d-2bca-4464-8472-d5f155a2afff',NULL,'auth-otp-form','master','bcf286c3-49e2-44b9-b6da-bf6ca070c35f',0,20,_binary '\0',NULL,NULL),('89cbdc6f-1aa6-40ac-ab52-5676383f4722',NULL,'auth-username-password-form','local','b42d9386-412c-48e1-ba6f-627c82945792',0,10,_binary '\0',NULL,NULL),('91f3e8b6-ca7a-49d2-bcda-71f4009613cb',NULL,NULL,'master','ceceff61-fe48-44a4-bc9c-3d55fdda241a',0,20,_binary '','fe5185af-17aa-4360-b7da-5110bbeb6b6a',NULL),('950f205c-fb25-4b96-8da0-0bf0f07465ef',NULL,'no-cookie-redirect','master','ceceff61-fe48-44a4-bc9c-3d55fdda241a',0,10,_binary '\0',NULL,NULL),('95c6c0e0-1ae3-4e32-a1c1-b5235b7f77d0',NULL,'idp-confirm-link','master','360ecbea-2f53-471c-a1cb-6ad9f0a17e9f',0,10,_binary '\0',NULL,NULL),('98044da8-58e5-4b6d-947a-217b86dc53fc',NULL,'client-secret','local','a979bb85-af13-45a5-8c7b-4bcc75e896ce',2,10,_binary '\0',NULL,NULL),('992d2827-d909-4c2c-9be0-a614f00ce168',NULL,'registration-page-form','master','7820aec0-0c47-4de7-8c4f-16cccde7b59c',0,10,_binary '','98e837ae-15b0-4ae3-b493-74286fde4d71',NULL),('9ccf43c0-bb52-4061-85b6-7ebb74902a56',NULL,'reset-otp','local','0c4f4722-54cb-4d36-8301-94d88ec08990',0,20,_binary '\0',NULL,NULL),('9ff3d94f-90f9-4a6e-82ba-82125823d4ad',NULL,'auth-username-password-form','master','51552855-4c91-4896-b067-0cc3093c9ebb',0,10,_binary '\0',NULL,NULL),('a1ec571b-524e-4f2c-8aea-ed1b2ca6f678',NULL,'auth-spnego','master','fe5185af-17aa-4360-b7da-5110bbeb6b6a',3,30,_binary '\0',NULL,NULL),('a4bdddf9-4f4a-41fd-922d-f2595578a255',NULL,'conditional-user-configured','local','d2923165-7a1b-426f-99c4-cb5c22e98c1d',0,10,_binary '\0',NULL,NULL),('a5003a02-3fc0-4723-be29-c16bb3bfd539',NULL,'idp-create-user-if-unique','master','febe769c-6716-4ebf-bce7-1ea605c2790a',2,10,_binary '\0',NULL,'404394a0-ec72-4370-9ed5-3287a98c3ab5'),('a6286cff-d9bf-4795-963e-6d1577428828',NULL,NULL,'master','51552855-4c91-4896-b067-0cc3093c9ebb',1,20,_binary '','e38b8513-b1b5-45c2-90db-ef8675b0f733',NULL),('a89fc626-ce0b-48e9-96dd-5ce2946a61ff',NULL,'auth-spnego','local','6e2d142c-752e-41d3-8080-f12664cdb0b4',3,20,_binary '\0',NULL,NULL),('ad524c50-17e6-46b8-99d7-adacc1f64b25',NULL,NULL,'local','5c2cbcd5-1ccf-4676-b871-eea95d6e4d51',2,20,_binary '','e1a8d483-30bc-4667-a611-2ab3330af85d',NULL),('adc5dd7a-84ee-46ed-a2ae-a8d3cc2d8a0d',NULL,'reset-otp','master','0b5b0720-6c94-4897-95e2-0016f45b03bb',0,20,_binary '\0',NULL,NULL),('aeb2e8c5-9a99-45e5-b47b-7d4d9ed8bbe0',NULL,'basic-auth-otp','master','fe5185af-17aa-4360-b7da-5110bbeb6b6a',3,20,_binary '\0',NULL,NULL),('b0bd8c42-0b83-42b0-831c-7a8344b53dc8',NULL,'idp-review-profile','master','e71ffa25-84ad-4f02-91b3-41e6e4c553dc',0,10,_binary '\0',NULL,'104df280-c684-48f2-82ed-d1db72324a37'),('b12de52f-15dc-40a4-ab28-4f0b9f1bedbb',NULL,'direct-grant-validate-username','local','4072fe0f-7f2c-4083-adee-3e78e274a7e4',0,10,_binary '\0',NULL,NULL),('b2720bcd-1a0f-4826-95ab-23e2eeeae32c',NULL,NULL,'local','720bd12b-169c-46d1-9da4-96e268520266',1,20,_binary '','51dc1ff8-f5e9-4563-9868-e60c25056044',NULL),('bd321d9b-9cea-42fb-b75a-b46b03b49205',NULL,'identity-provider-redirector','master','206a6fd5-e35a-4c13-81fa-55c9ec9aadc2',2,25,_binary '\0',NULL,NULL),('bdc4b986-6c60-4c5a-b183-253713857798',NULL,'reset-password','local','0df0da2a-7ae2-4b9c-a6cc-74de03562634',0,30,_binary '\0',NULL,NULL),('c427dd7d-ba3a-475b-a15f-68bf9f8b5181',NULL,'auth-otp-form','local','51dc1ff8-f5e9-4563-9868-e60c25056044',0,20,_binary '\0',NULL,NULL),('c6f82091-37ee-4575-ae62-079a1d2263b9',NULL,'auth-otp-form','master','e38b8513-b1b5-45c2-90db-ef8675b0f733',0,20,_binary '\0',NULL,NULL),('cb2bbba3-fa33-4139-9335-336e235499b7',NULL,NULL,'master','206a6fd5-e35a-4c13-81fa-55c9ec9aadc2',2,30,_binary '','51552855-4c91-4896-b067-0cc3093c9ebb',NULL),('ccdc7bf4-016d-4a70-93b5-03e1e37e6f70',NULL,'reset-credential-email','local','0df0da2a-7ae2-4b9c-a6cc-74de03562634',0,20,_binary '\0',NULL,NULL),('d28db0ed-ea5a-4dbd-9e05-9358f0a7ff6d',NULL,'direct-grant-validate-otp','master','a819b946-ab38-44c8-a8c5-840a7c574b15',0,20,_binary '\0',NULL,NULL),('d430d079-5612-4eb0-a22d-695b76e12671',NULL,'docker-http-basic-authenticator','local','be0d678d-1bc1-40fe-85b1-8ff79bb050b7',0,10,_binary '\0',NULL,NULL),('d7cfea6a-c96e-4d9f-9005-3419c2f7936d',NULL,'auth-spnego','local','60bb0421-6321-4d9d-b034-8a03500dc244',3,30,_binary '\0',NULL,NULL),('d8c9e53b-5d7a-41ff-8637-eba17c80e7f9',NULL,'no-cookie-redirect','local','c4f85414-c975-4601-af4f-6de37a42ddc6',0,10,_binary '\0',NULL,NULL),('e5639885-db7e-4d7a-80f8-59a40ccd0fa4',NULL,NULL,'master','e71ffa25-84ad-4f02-91b3-41e6e4c553dc',0,20,_binary '','febe769c-6716-4ebf-bce7-1ea605c2790a',NULL),('e5ff1428-f2e2-491d-832f-46eb01e185e7',NULL,'client-x509','local','a979bb85-af13-45a5-8c7b-4bcc75e896ce',2,40,_binary '\0',NULL,NULL),('e76bbf73-bc53-4be7-be5b-63f6e794776c',NULL,'client-secret','master','449265ae-aefd-44df-a3b2-4e76f490e099',2,10,_binary '\0',NULL,NULL),('eb0750de-be47-44ab-8c26-4486ca7b0208',NULL,'direct-grant-validate-password','local','4072fe0f-7f2c-4083-adee-3e78e274a7e4',0,20,_binary '\0',NULL,NULL),('ed2ff011-365b-4112-b63a-61ba9918dc4b',NULL,'client-x509','master','449265ae-aefd-44df-a3b2-4e76f490e099',2,40,_binary '\0',NULL,NULL),('efb828e6-8eed-4c49-8828-b462409df813',NULL,'conditional-user-configured','master','bcf286c3-49e2-44b9-b6da-bf6ca070c35f',0,10,_binary '\0',NULL,NULL),('f15b0239-609a-419f-8492-75b27c5e0c37',NULL,'idp-username-password-form','master','35250a43-53ad-4adf-90f7-22d73c80897a',0,10,_binary '\0',NULL,NULL),('f25a9080-1410-4b48-b014-4cd9fc8da332',NULL,NULL,'master','35250a43-53ad-4adf-90f7-22d73c80897a',1,20,_binary '','bcf286c3-49e2-44b9-b6da-bf6ca070c35f',NULL),('f2668d9a-00ed-4c49-9c97-43e6024b35bb',NULL,'basic-auth-otp','local','60bb0421-6321-4d9d-b034-8a03500dc244',3,20,_binary '\0',NULL,NULL),('f305fbde-0282-48a9-ac64-13937d99e992',NULL,'registration-recaptcha-action','local','785bcc6b-097c-4aef-82f2-24b420257d62',3,60,_binary '\0',NULL,NULL),('f7083020-e1a8-44ee-820f-67f67ca73077',NULL,NULL,'local','4072fe0f-7f2c-4083-adee-3e78e274a7e4',1,30,_binary '','d2923165-7a1b-426f-99c4-cb5c22e98c1d',NULL),('f9cbad4e-d7b6-442e-83db-8c93201adaee',NULL,'identity-provider-redirector','local','6e2d142c-752e-41d3-8080-f12664cdb0b4',2,25,_binary '\0',NULL,NULL),('fa6bba72-9154-4e2e-82cc-16058311c6ad',NULL,'registration-password-action','master','98e837ae-15b0-4ae3-b493-74286fde4d71',0,50,_binary '\0',NULL,NULL),('fb955044-9cc8-498a-b00d-d6cbc5b26cbe',NULL,NULL,'local','0df0da2a-7ae2-4b9c-a6cc-74de03562634',1,40,_binary '','0c4f4722-54cb-4d36-8301-94d88ec08990',NULL),('feb96715-3570-4e26-92ca-de65375cb3b7',NULL,'registration-profile-action','master','98e837ae-15b0-4ae3-b493-74286fde4d71',0,40,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `AUTHENTICATION_EXECUTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATION_FLOW`
--

DROP TABLE IF EXISTS `AUTHENTICATION_FLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATION_FLOW` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_ID` varchar(36) NOT NULL DEFAULT 'basic-flow',
  `TOP_LEVEL` bit(1) NOT NULL DEFAULT b'0',
  `BUILT_IN` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_FLOW_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_FLOW_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATION_FLOW`
--

LOCK TABLES `AUTHENTICATION_FLOW` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATION_FLOW` DISABLE KEYS */;
INSERT INTO `AUTHENTICATION_FLOW` VALUES ('0b5b0720-6c94-4897-95e2-0016f45b03bb','Reset - Conditional OTP','Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.','master','basic-flow',_binary '\0',_binary ''),('0baacc05-1332-4d3d-8f4a-b0dc74bc0d98','saml ecp','SAML ECP Profile Authentication Flow','local','basic-flow',_binary '',_binary ''),('0c4f4722-54cb-4d36-8301-94d88ec08990','Reset - Conditional OTP','Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.','local','basic-flow',_binary '\0',_binary ''),('0df0da2a-7ae2-4b9c-a6cc-74de03562634','reset credentials','Reset credentials for a user if they forgot their password or something','local','basic-flow',_binary '',_binary ''),('206a6fd5-e35a-4c13-81fa-55c9ec9aadc2','browser','browser based authentication','master','basic-flow',_binary '',_binary ''),('236736ae-fac2-4877-977f-e619514a03ec','first broker login','Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account','local','basic-flow',_binary '',_binary ''),('35250a43-53ad-4adf-90f7-22d73c80897a','Verify Existing Account by Re-authentication','Reauthentication of existing account','master','basic-flow',_binary '\0',_binary ''),('360ecbea-2f53-471c-a1cb-6ad9f0a17e9f','Handle Existing Account','Handle what to do if there is existing account with same email/username like authenticated identity provider','master','basic-flow',_binary '\0',_binary ''),('4072fe0f-7f2c-4083-adee-3e78e274a7e4','direct grant','OpenID Connect Resource Owner Grant','local','basic-flow',_binary '',_binary ''),('430755bf-e16a-45af-a87c-5c60a2e7113a','docker auth','Used by Docker clients to authenticate against the IDP','master','basic-flow',_binary '',_binary ''),('449265ae-aefd-44df-a3b2-4e76f490e099','clients','Base authentication for clients','master','client-flow',_binary '',_binary ''),('51552855-4c91-4896-b067-0cc3093c9ebb','forms','Username, password, otp and other auth forms.','master','basic-flow',_binary '\0',_binary ''),('51dc1ff8-f5e9-4563-9868-e60c25056044','First broker login - Conditional OTP','Flow to determine if the OTP is required for the authentication','local','basic-flow',_binary '\0',_binary ''),('5c2cbcd5-1ccf-4676-b871-eea95d6e4d51','User creation or linking','Flow for the existing/non-existing user alternatives','local','basic-flow',_binary '\0',_binary ''),('60bb0421-6321-4d9d-b034-8a03500dc244','Authentication Options','Authentication options.','local','basic-flow',_binary '\0',_binary ''),('6c9584c6-9c33-4f4c-b180-81d3932b27f3','direct grant','OpenID Connect Resource Owner Grant','master','basic-flow',_binary '',_binary ''),('6e2d142c-752e-41d3-8080-f12664cdb0b4','browser','browser based authentication','local','basic-flow',_binary '',_binary ''),('71b3a190-38a6-49fd-b5db-3414e00e8765','registration','registration flow','local','basic-flow',_binary '',_binary ''),('720bd12b-169c-46d1-9da4-96e268520266','Verify Existing Account by Re-authentication','Reauthentication of existing account','local','basic-flow',_binary '\0',_binary ''),('754bbff4-ef9b-49cd-a145-40c8516b31a1','Browser - Conditional OTP','Flow to determine if the OTP is required for the authentication','local','basic-flow',_binary '\0',_binary ''),('7820aec0-0c47-4de7-8c4f-16cccde7b59c','registration','registration flow','master','basic-flow',_binary '',_binary ''),('785bcc6b-097c-4aef-82f2-24b420257d62','registration form','registration form','local','form-flow',_binary '\0',_binary ''),('98e837ae-15b0-4ae3-b493-74286fde4d71','registration form','registration form','master','form-flow',_binary '\0',_binary ''),('a819b946-ab38-44c8-a8c5-840a7c574b15','Direct Grant - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('a979bb85-af13-45a5-8c7b-4bcc75e896ce','clients','Base authentication for clients','local','client-flow',_binary '',_binary ''),('b42d9386-412c-48e1-ba6f-627c82945792','forms','Username, password, otp and other auth forms.','local','basic-flow',_binary '\0',_binary ''),('bcf286c3-49e2-44b9-b6da-bf6ca070c35f','First broker login - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('be0d678d-1bc1-40fe-85b1-8ff79bb050b7','docker auth','Used by Docker clients to authenticate against the IDP','local','basic-flow',_binary '',_binary ''),('c4f85414-c975-4601-af4f-6de37a42ddc6','http challenge','An authentication flow based on challenge-response HTTP Authentication Schemes','local','basic-flow',_binary '',_binary ''),('ca058d70-a38f-429e-94ca-55719ceef211','reset credentials','Reset credentials for a user if they forgot their password or something','master','basic-flow',_binary '',_binary ''),('ceceff61-fe48-44a4-bc9c-3d55fdda241a','http challenge','An authentication flow based on challenge-response HTTP Authentication Schemes','master','basic-flow',_binary '',_binary ''),('d2923165-7a1b-426f-99c4-cb5c22e98c1d','Direct Grant - Conditional OTP','Flow to determine if the OTP is required for the authentication','local','basic-flow',_binary '\0',_binary ''),('dcd4faf8-415a-4e01-8bf0-9f3600d3800e','Account verification options','Method with which to verity the existing account','local','basic-flow',_binary '\0',_binary ''),('e1a8d483-30bc-4667-a611-2ab3330af85d','Handle Existing Account','Handle what to do if there is existing account with same email/username like authenticated identity provider','local','basic-flow',_binary '\0',_binary ''),('e38b8513-b1b5-45c2-90db-ef8675b0f733','Browser - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('e71ffa25-84ad-4f02-91b3-41e6e4c553dc','first broker login','Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account','master','basic-flow',_binary '',_binary ''),('ee3eda91-f5d2-4a82-b5b7-6355c2d84fed','saml ecp','SAML ECP Profile Authentication Flow','master','basic-flow',_binary '',_binary ''),('eea1631e-a320-43a9-a58c-c48f13e19458','Account verification options','Method with which to verity the existing account','master','basic-flow',_binary '\0',_binary ''),('fe5185af-17aa-4360-b7da-5110bbeb6b6a','Authentication Options','Authentication options.','master','basic-flow',_binary '\0',_binary ''),('febe769c-6716-4ebf-bce7-1ea605c2790a','User creation or linking','Flow for the existing/non-existing user alternatives','master','basic-flow',_binary '\0',_binary '');
/*!40000 ALTER TABLE `AUTHENTICATION_FLOW` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATOR_CONFIG`
--

DROP TABLE IF EXISTS `AUTHENTICATOR_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATOR_CONFIG` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_CONFIG_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATOR_CONFIG`
--

LOCK TABLES `AUTHENTICATOR_CONFIG` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG` DISABLE KEYS */;
INSERT INTO `AUTHENTICATOR_CONFIG` VALUES ('104df280-c684-48f2-82ed-d1db72324a37','review profile config','master'),('404394a0-ec72-4370-9ed5-3287a98c3ab5','create unique user config','master'),('79a0872e-3407-4789-8365-1ae826e3e01e','create unique user config','local'),('e1357f08-fe63-417d-ae79-93c9ae07ea00','review profile config','local');
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATOR_CONFIG_ENTRY`
--

DROP TABLE IF EXISTS `AUTHENTICATOR_CONFIG_ENTRY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATOR_CONFIG_ENTRY` (
  `AUTHENTICATOR_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`AUTHENTICATOR_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATOR_CONFIG_ENTRY`
--

LOCK TABLES `AUTHENTICATOR_CONFIG_ENTRY` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG_ENTRY` DISABLE KEYS */;
INSERT INTO `AUTHENTICATOR_CONFIG_ENTRY` VALUES ('104df280-c684-48f2-82ed-d1db72324a37','missing','update.profile.on.first.login'),('404394a0-ec72-4370-9ed5-3287a98c3ab5','false','require.password.update.after.registration'),('79a0872e-3407-4789-8365-1ae826e3e01e','false','require.password.update.after.registration'),('e1357f08-fe63-417d-ae79-93c9ae07ea00','missing','update.profile.on.first.login');
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG_ENTRY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BROKER_LINK`
--

DROP TABLE IF EXISTS `BROKER_LINK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BROKER_LINK` (
  `IDENTITY_PROVIDER` varchar(255) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `BROKER_USER_ID` varchar(255) DEFAULT NULL,
  `BROKER_USERNAME` varchar(255) DEFAULT NULL,
  `TOKEN` text,
  `USER_ID` varchar(255) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BROKER_LINK`
--

LOCK TABLES `BROKER_LINK` WRITE;
/*!40000 ALTER TABLE `BROKER_LINK` DISABLE KEYS */;
/*!40000 ALTER TABLE `BROKER_LINK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT`
--

DROP TABLE IF EXISTS `CLIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT` (
  `ID` varchar(36) NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FULL_SCOPE_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int DEFAULT NULL,
  `PUBLIC_CLIENT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` varchar(255) DEFAULT NULL,
  `BASE_URL` varchar(255) DEFAULT NULL,
  `BEARER_ONLY` bit(1) NOT NULL DEFAULT b'0',
  `MANAGEMENT_URL` varchar(255) DEFAULT NULL,
  `SURROGATE_AUTH_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) DEFAULT NULL,
  `PROTOCOL` varchar(255) DEFAULT NULL,
  `NODE_REREG_TIMEOUT` int DEFAULT '0',
  `FRONTCHANNEL_LOGOUT` bit(1) NOT NULL DEFAULT b'0',
  `CONSENT_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `SERVICE_ACCOUNTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_AUTHENTICATOR_TYPE` varchar(255) DEFAULT NULL,
  `ROOT_URL` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REGISTRATION_TOKEN` varchar(255) DEFAULT NULL,
  `STANDARD_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'1',
  `IMPLICIT_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DIRECT_ACCESS_GRANTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ALWAYS_DISPLAY_IN_CONSOLE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_B71CJLBENV945RB6GCON438AT` (`REALM_ID`,`CLIENT_ID`),
  KEY `IDX_CLIENT_ID` (`CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT`
--

LOCK TABLES `CLIENT` WRITE;
/*!40000 ALTER TABLE `CLIENT` DISABLE KEYS */;
INSERT INTO `CLIENT` VALUES ('047d93f5-955c-41c1-9513-75a8745618c3',_binary '',_binary '','pubkong',0,_binary '',NULL,NULL,_binary '\0',NULL,_binary '\0','local','openid-connect',-1,_binary '\0',_binary '\0',NULL,_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '',_binary '\0'),('1da632b4-3088-4044-a3db-99b423a1872c',_binary '',_binary '','kong',0,_binary '\0','f4c2b016-f073-427d-b3bd-a6a06ee98d7d',NULL,_binary '\0',NULL,_binary '\0','local','openid-connect',-1,_binary '\0',_binary '\0',NULL,_binary '','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '',_binary '\0'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc',_binary '',_binary '\0','account-console',0,_binary '',NULL,'/realms/local/account/',_binary '\0',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_account-console}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('47757fda-9024-445a-9d4b-00fc169bb329',_binary '',_binary '\0','broker',0,_binary '\0',NULL,NULL,_binary '',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_broker}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('55e8799a-e472-4690-a56f-f07d3920595d',_binary '',_binary '\0','security-admin-console',0,_binary '',NULL,'/admin/local/console/',_binary '\0',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_security-admin-console}',_binary '\0','client-secret','${authAdminUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('5d8641b5-9790-423d-a629-0fce0ad67389',_binary '',_binary '\0','realm-management',0,_binary '\0',NULL,NULL,_binary '',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_realm-management}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '',_binary '\0','account',0,_binary '',NULL,'/realms/master/account/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_account}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('6cc49300-8dc8-488f-9658-0aca8035e02a',_binary '',_binary '\0','broker',0,_binary '\0',NULL,NULL,_binary '',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_broker}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8',_binary '',_binary '\0','security-admin-console',0,_binary '',NULL,'/admin/master/console/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_security-admin-console}',_binary '\0','client-secret','${authAdminUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '',_binary '\0','account',0,_binary '',NULL,'/realms/local/account/',_binary '\0',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_account}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('8f0ab675-b796-433f-872c-1f9c0d15650f',_binary '',_binary '\0','admin-cli',0,_binary '',NULL,NULL,_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_admin-cli}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '\0',_binary '\0',_binary '',_binary '\0'),('8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '',_binary '\0','master-realm',0,_binary '\0',NULL,NULL,_binary '',NULL,_binary '\0','master',NULL,0,_binary '\0',_binary '\0','master Realm',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '',_binary '\0','local-realm',0,_binary '\0',NULL,NULL,_binary '',NULL,_binary '\0','master',NULL,0,_binary '\0',_binary '\0','local Realm',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b',_binary '',_binary '\0','admin-cli',0,_binary '',NULL,NULL,_binary '\0',NULL,_binary '\0','local','openid-connect',0,_binary '\0',_binary '\0','${client_admin-cli}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '\0',_binary '\0',_binary '',_binary '\0'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91',_binary '',_binary '','gatekeeper',0,_binary '\0','9df47ac2-bfe9-46cf-910f-512a5d008117',NULL,_binary '\0',NULL,_binary '\0','local','openid-connect',-1,_binary '\0',_binary '\0',NULL,_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '',_binary '\0'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0',_binary '',_binary '\0','account-console',0,_binary '',NULL,'/realms/master/account/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_account-console}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0');
/*!40000 ALTER TABLE `CLIENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_ATTRIBUTES`
--

DROP TABLE IF EXISTS `CLIENT_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_ATTRIBUTES` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` text,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  KEY `IDX_CLIENT_ATT_BY_NAME_VALUE` (`NAME`,`VALUE`(255)),
  CONSTRAINT `FK3C47C64BEACCA966` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_ATTRIBUTES`
--

LOCK TABLES `CLIENT_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `CLIENT_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `CLIENT_ATTRIBUTES` VALUES ('047d93f5-955c-41c1-9513-75a8745618c3','false','backchannel.logout.revoke.offline.tokens'),('047d93f5-955c-41c1-9513-75a8745618c3','true','backchannel.logout.session.required'),('047d93f5-955c-41c1-9513-75a8745618c3','false','client_credentials.use_refresh_token'),('047d93f5-955c-41c1-9513-75a8745618c3','false','display.on.consent.screen'),('047d93f5-955c-41c1-9513-75a8745618c3','false','exclude.session.state.from.auth.response'),('047d93f5-955c-41c1-9513-75a8745618c3','false','id.token.as.detached.signature'),('047d93f5-955c-41c1-9513-75a8745618c3','false','oauth2.device.authorization.grant.enabled'),('047d93f5-955c-41c1-9513-75a8745618c3','false','oidc.ciba.grant.enabled'),('047d93f5-955c-41c1-9513-75a8745618c3','false','require.pushed.authorization.requests'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml_force_name_id_format'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.artifact.binding'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.assertion.signature'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.authnstatement'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.client.signature'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.encrypt'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.force.post.binding'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.multivalued.roles'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.onetimeuse.condition'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.server.signature'),('047d93f5-955c-41c1-9513-75a8745618c3','false','saml.server.signature.keyinfo.ext'),('047d93f5-955c-41c1-9513-75a8745618c3','false','tls.client.certificate.bound.access.tokens'),('047d93f5-955c-41c1-9513-75a8745618c3','true','use.refresh.tokens'),('1da632b4-3088-4044-a3db-99b423a1872c','false','backchannel.logout.revoke.offline.tokens'),('1da632b4-3088-4044-a3db-99b423a1872c','true','backchannel.logout.session.required'),('1da632b4-3088-4044-a3db-99b423a1872c','false','client_credentials.use_refresh_token'),('1da632b4-3088-4044-a3db-99b423a1872c','false','display.on.consent.screen'),('1da632b4-3088-4044-a3db-99b423a1872c','false','exclude.session.state.from.auth.response'),('1da632b4-3088-4044-a3db-99b423a1872c','false','id.token.as.detached.signature'),('1da632b4-3088-4044-a3db-99b423a1872c','false','oauth2.device.authorization.grant.enabled'),('1da632b4-3088-4044-a3db-99b423a1872c','false','oidc.ciba.grant.enabled'),('1da632b4-3088-4044-a3db-99b423a1872c','false','require.pushed.authorization.requests'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml_force_name_id_format'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.artifact.binding'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.assertion.signature'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.authnstatement'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.client.signature'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.encrypt'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.force.post.binding'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.multivalued.roles'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.onetimeuse.condition'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.server.signature'),('1da632b4-3088-4044-a3db-99b423a1872c','false','saml.server.signature.keyinfo.ext'),('1da632b4-3088-4044-a3db-99b423a1872c','false','tls.client.certificate.bound.access.tokens'),('1da632b4-3088-4044-a3db-99b423a1872c','true','use.refresh.tokens'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','S256','pkce.code.challenge.method'),('55e8799a-e472-4690-a56f-f07d3920595d','S256','pkce.code.challenge.method'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','S256','pkce.code.challenge.method'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','backchannel.logout.revoke.offline.tokens'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','true','backchannel.logout.session.required'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','client_credentials.use_refresh_token'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','display.on.consent.screen'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','exclude.session.state.from.auth.response'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','id.token.as.detached.signature'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','oauth2.device.authorization.grant.enabled'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','oidc.ciba.grant.enabled'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','require.pushed.authorization.requests'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml_force_name_id_format'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.artifact.binding'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.assertion.signature'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.authnstatement'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.client.signature'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.encrypt'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.force.post.binding'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.multivalued.roles'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.onetimeuse.condition'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.server.signature'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','saml.server.signature.keyinfo.ext'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','false','tls.client.certificate.bound.access.tokens'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','true','use.refresh.tokens'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','S256','pkce.code.challenge.method');
/*!40000 ALTER TABLE `CLIENT_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_AUTH_FLOW_BINDINGS`
--

DROP TABLE IF EXISTS `CLIENT_AUTH_FLOW_BINDINGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_AUTH_FLOW_BINDINGS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `FLOW_ID` varchar(36) DEFAULT NULL,
  `BINDING_NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`BINDING_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_AUTH_FLOW_BINDINGS`
--

LOCK TABLES `CLIENT_AUTH_FLOW_BINDINGS` WRITE;
/*!40000 ALTER TABLE `CLIENT_AUTH_FLOW_BINDINGS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_AUTH_FLOW_BINDINGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_INITIAL_ACCESS`
--

DROP TABLE IF EXISTS `CLIENT_INITIAL_ACCESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_INITIAL_ACCESS` (
  `ID` varchar(36) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `EXPIRATION` int DEFAULT NULL,
  `COUNT` int DEFAULT NULL,
  `REMAINING_COUNT` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_INIT_ACC_REALM` (`REALM_ID`),
  CONSTRAINT `FK_CLIENT_INIT_ACC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_INITIAL_ACCESS`
--

LOCK TABLES `CLIENT_INITIAL_ACCESS` WRITE;
/*!40000 ALTER TABLE `CLIENT_INITIAL_ACCESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_INITIAL_ACCESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_NODE_REGISTRATIONS`
--

DROP TABLE IF EXISTS `CLIENT_NODE_REGISTRATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_NODE_REGISTRATIONS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` int DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  CONSTRAINT `FK4129723BA992F594` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_NODE_REGISTRATIONS`
--

LOCK TABLES `CLIENT_NODE_REGISTRATIONS` WRITE;
/*!40000 ALTER TABLE `CLIENT_NODE_REGISTRATIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_NODE_REGISTRATIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `PROTOCOL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CLI_SCOPE` (`REALM_ID`,`NAME`),
  KEY `IDX_REALM_CLSCOPE` (`REALM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE`
--

LOCK TABLES `CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE` VALUES ('04af3e2e-a87a-410a-933a-30f5a4193427','profile','master','OpenID Connect built-in scope: profile','openid-connect'),('0945f2d8-d930-45ea-a070-a8b13d9f5149','roles','local','OpenID Connect scope for add user roles to the access token','openid-connect'),('0a8833d9-b89f-46d6-8cd7-c22f628a2497','offline_access','master','OpenID Connect built-in scope: offline_access','openid-connect'),('2399f441-5504-49de-9066-7c690099d20e','microprofile-jwt','master','Microprofile - JWT built-in scope','openid-connect'),('34d869de-d2b2-4fb5-a03c-407c22c59329','address','local','OpenID Connect built-in scope: address','openid-connect'),('47a8bced-b96d-4a9e-920c-f3f1e90e0f74','email','master','OpenID Connect built-in scope: email','openid-connect'),('4b6132da-5079-4cfd-8581-57690401da19','roles','master','OpenID Connect scope for add user roles to the access token','openid-connect'),('62f11fbd-04d4-4e3d-a0d8-f5966383904b','phone','local','OpenID Connect built-in scope: phone','openid-connect'),('6a5c0891-915c-4d8d-8c78-c516a9e3d0a7','profile','local','OpenID Connect built-in scope: profile','openid-connect'),('80d6ad2e-32a5-415e-9251-21934602c5d7','microprofile-jwt','local','Microprofile - JWT built-in scope','openid-connect'),('81cd040d-2997-4011-bd1e-6726f4bd250c','offline_access','local','OpenID Connect built-in scope: offline_access','openid-connect'),('8520fdd0-4204-4a5d-b7db-5ed8df4c823a','role_list','master','SAML role list','saml'),('9747c4af-10f4-42ce-adeb-9d73451826d9','email','local','OpenID Connect built-in scope: email','openid-connect'),('bb5d53f5-6ff3-447e-8667-3944dc28a6fb','address','master','OpenID Connect built-in scope: address','openid-connect'),('d4cc8b3e-63b2-47a8-87f1-163377fdd1ce','phone','master','OpenID Connect built-in scope: phone','openid-connect'),('ed8830e6-d6b8-4fc0-80f4-f4713245af26','web-origins','master','OpenID Connect scope for add allowed web origins to the access token','openid-connect'),('f1206ae8-f5c9-4d63-bf04-60c145a41d86','role_list','local','SAML role list','saml'),('fa041adc-b7df-4b0d-a512-4fe37811ad49','web-origins','local','OpenID Connect scope for add allowed web origins to the access token','openid-connect');
/*!40000 ALTER TABLE `CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_ATTRIBUTES`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_ATTRIBUTES` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `VALUE` text,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`NAME`),
  KEY `IDX_CLSCOPE_ATTRS` (`SCOPE_ID`),
  CONSTRAINT `FK_CL_SCOPE_ATTR_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_ATTRIBUTES`
--

LOCK TABLES `CLIENT_SCOPE_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_ATTRIBUTES` VALUES ('04af3e2e-a87a-410a-933a-30f5a4193427','${profileScopeConsentText}','consent.screen.text'),('04af3e2e-a87a-410a-933a-30f5a4193427','true','display.on.consent.screen'),('04af3e2e-a87a-410a-933a-30f5a4193427','true','include.in.token.scope'),('0945f2d8-d930-45ea-a070-a8b13d9f5149','${rolesScopeConsentText}','consent.screen.text'),('0945f2d8-d930-45ea-a070-a8b13d9f5149','true','display.on.consent.screen'),('0945f2d8-d930-45ea-a070-a8b13d9f5149','false','include.in.token.scope'),('0a8833d9-b89f-46d6-8cd7-c22f628a2497','${offlineAccessScopeConsentText}','consent.screen.text'),('0a8833d9-b89f-46d6-8cd7-c22f628a2497','true','display.on.consent.screen'),('2399f441-5504-49de-9066-7c690099d20e','false','display.on.consent.screen'),('2399f441-5504-49de-9066-7c690099d20e','true','include.in.token.scope'),('34d869de-d2b2-4fb5-a03c-407c22c59329','${addressScopeConsentText}','consent.screen.text'),('34d869de-d2b2-4fb5-a03c-407c22c59329','true','display.on.consent.screen'),('34d869de-d2b2-4fb5-a03c-407c22c59329','true','include.in.token.scope'),('47a8bced-b96d-4a9e-920c-f3f1e90e0f74','${emailScopeConsentText}','consent.screen.text'),('47a8bced-b96d-4a9e-920c-f3f1e90e0f74','true','display.on.consent.screen'),('47a8bced-b96d-4a9e-920c-f3f1e90e0f74','true','include.in.token.scope'),('4b6132da-5079-4cfd-8581-57690401da19','${rolesScopeConsentText}','consent.screen.text'),('4b6132da-5079-4cfd-8581-57690401da19','true','display.on.consent.screen'),('4b6132da-5079-4cfd-8581-57690401da19','false','include.in.token.scope'),('62f11fbd-04d4-4e3d-a0d8-f5966383904b','${phoneScopeConsentText}','consent.screen.text'),('62f11fbd-04d4-4e3d-a0d8-f5966383904b','true','display.on.consent.screen'),('62f11fbd-04d4-4e3d-a0d8-f5966383904b','true','include.in.token.scope'),('6a5c0891-915c-4d8d-8c78-c516a9e3d0a7','${profileScopeConsentText}','consent.screen.text'),('6a5c0891-915c-4d8d-8c78-c516a9e3d0a7','true','display.on.consent.screen'),('6a5c0891-915c-4d8d-8c78-c516a9e3d0a7','true','include.in.token.scope'),('80d6ad2e-32a5-415e-9251-21934602c5d7','false','display.on.consent.screen'),('80d6ad2e-32a5-415e-9251-21934602c5d7','true','include.in.token.scope'),('81cd040d-2997-4011-bd1e-6726f4bd250c','${offlineAccessScopeConsentText}','consent.screen.text'),('81cd040d-2997-4011-bd1e-6726f4bd250c','true','display.on.consent.screen'),('8520fdd0-4204-4a5d-b7db-5ed8df4c823a','${samlRoleListScopeConsentText}','consent.screen.text'),('8520fdd0-4204-4a5d-b7db-5ed8df4c823a','true','display.on.consent.screen'),('9747c4af-10f4-42ce-adeb-9d73451826d9','${emailScopeConsentText}','consent.screen.text'),('9747c4af-10f4-42ce-adeb-9d73451826d9','true','display.on.consent.screen'),('9747c4af-10f4-42ce-adeb-9d73451826d9','true','include.in.token.scope'),('bb5d53f5-6ff3-447e-8667-3944dc28a6fb','${addressScopeConsentText}','consent.screen.text'),('bb5d53f5-6ff3-447e-8667-3944dc28a6fb','true','display.on.consent.screen'),('bb5d53f5-6ff3-447e-8667-3944dc28a6fb','true','include.in.token.scope'),('d4cc8b3e-63b2-47a8-87f1-163377fdd1ce','${phoneScopeConsentText}','consent.screen.text'),('d4cc8b3e-63b2-47a8-87f1-163377fdd1ce','true','display.on.consent.screen'),('d4cc8b3e-63b2-47a8-87f1-163377fdd1ce','true','include.in.token.scope'),('ed8830e6-d6b8-4fc0-80f4-f4713245af26','','consent.screen.text'),('ed8830e6-d6b8-4fc0-80f4-f4713245af26','false','display.on.consent.screen'),('ed8830e6-d6b8-4fc0-80f4-f4713245af26','false','include.in.token.scope'),('f1206ae8-f5c9-4d63-bf04-60c145a41d86','${samlRoleListScopeConsentText}','consent.screen.text'),('f1206ae8-f5c9-4d63-bf04-60c145a41d86','true','display.on.consent.screen'),('fa041adc-b7df-4b0d-a512-4fe37811ad49','','consent.screen.text'),('fa041adc-b7df-4b0d-a512-4fe37811ad49','false','display.on.consent.screen'),('fa041adc-b7df-4b0d-a512-4fe37811ad49','false','include.in.token.scope');
/*!40000 ALTER TABLE `CLIENT_SCOPE_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_CLIENT`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_CLIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_CLIENT` (
  `CLIENT_ID` varchar(255) NOT NULL,
  `SCOPE_ID` varchar(255) NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`CLIENT_ID`,`SCOPE_ID`),
  KEY `IDX_CLSCOPE_CL` (`CLIENT_ID`),
  KEY `IDX_CL_CLSCOPE` (`SCOPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_CLIENT`
--

LOCK TABLES `CLIENT_SCOPE_CLIENT` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_CLIENT` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_CLIENT` VALUES ('047d93f5-955c-41c1-9513-75a8745618c3','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('047d93f5-955c-41c1-9513-75a8745618c3','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('047d93f5-955c-41c1-9513-75a8745618c3','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('047d93f5-955c-41c1-9513-75a8745618c3','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('047d93f5-955c-41c1-9513-75a8745618c3','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('047d93f5-955c-41c1-9513-75a8745618c3','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('047d93f5-955c-41c1-9513-75a8745618c3','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('047d93f5-955c-41c1-9513-75a8745618c3','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('1da632b4-3088-4044-a3db-99b423a1872c','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('1da632b4-3088-4044-a3db-99b423a1872c','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('1da632b4-3088-4044-a3db-99b423a1872c','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('1da632b4-3088-4044-a3db-99b423a1872c','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('1da632b4-3088-4044-a3db-99b423a1872c','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('1da632b4-3088-4044-a3db-99b423a1872c','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('1da632b4-3088-4044-a3db-99b423a1872c','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('1da632b4-3088-4044-a3db-99b423a1872c','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('47757fda-9024-445a-9d4b-00fc169bb329','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('47757fda-9024-445a-9d4b-00fc169bb329','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('47757fda-9024-445a-9d4b-00fc169bb329','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('47757fda-9024-445a-9d4b-00fc169bb329','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('47757fda-9024-445a-9d4b-00fc169bb329','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('47757fda-9024-445a-9d4b-00fc169bb329','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('47757fda-9024-445a-9d4b-00fc169bb329','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('47757fda-9024-445a-9d4b-00fc169bb329','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('55e8799a-e472-4690-a56f-f07d3920595d','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('55e8799a-e472-4690-a56f-f07d3920595d','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('55e8799a-e472-4690-a56f-f07d3920595d','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('55e8799a-e472-4690-a56f-f07d3920595d','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('55e8799a-e472-4690-a56f-f07d3920595d','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('55e8799a-e472-4690-a56f-f07d3920595d','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('55e8799a-e472-4690-a56f-f07d3920595d','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('55e8799a-e472-4690-a56f-f07d3920595d','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('5d8641b5-9790-423d-a629-0fce0ad67389','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('5d8641b5-9790-423d-a629-0fce0ad67389','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('5d8641b5-9790-423d-a629-0fce0ad67389','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('5d8641b5-9790-423d-a629-0fce0ad67389','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('5d8641b5-9790-423d-a629-0fce0ad67389','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('5d8641b5-9790-423d-a629-0fce0ad67389','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('5d8641b5-9790-423d-a629-0fce0ad67389','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('5d8641b5-9790-423d-a629-0fce0ad67389','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary ''),('6cc49300-8dc8-488f-9658-0aca8035e02a','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('6cc49300-8dc8-488f-9658-0aca8035e02a','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('6cc49300-8dc8-488f-9658-0aca8035e02a','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('6cc49300-8dc8-488f-9658-0aca8035e02a','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('6cc49300-8dc8-488f-9658-0aca8035e02a','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('6cc49300-8dc8-488f-9658-0aca8035e02a','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('6cc49300-8dc8-488f-9658-0aca8035e02a','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('6cc49300-8dc8-488f-9658-0aca8035e02a','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary ''),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary ''),('86204c34-f02a-4a5b-968b-a1563ad2be76','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('86204c34-f02a-4a5b-968b-a1563ad2be76','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('86204c34-f02a-4a5b-968b-a1563ad2be76','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('86204c34-f02a-4a5b-968b-a1563ad2be76','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('86204c34-f02a-4a5b-968b-a1563ad2be76','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('86204c34-f02a-4a5b-968b-a1563ad2be76','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('86204c34-f02a-4a5b-968b-a1563ad2be76','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('86204c34-f02a-4a5b-968b-a1563ad2be76','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('8f0ab675-b796-433f-872c-1f9c0d15650f','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('8f0ab675-b796-433f-872c-1f9c0d15650f','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('8f0ab675-b796-433f-872c-1f9c0d15650f','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('8f0ab675-b796-433f-872c-1f9c0d15650f','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('8f0ab675-b796-433f-872c-1f9c0d15650f','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('8f0ab675-b796-433f-872c-1f9c0d15650f','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('8f0ab675-b796-433f-872c-1f9c0d15650f','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('8f0ab675-b796-433f-872c-1f9c0d15650f','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary ''),('8f806415-f35d-48c2-bf8a-0f00003bbc72','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('8f806415-f35d-48c2-bf8a-0f00003bbc72','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('8f806415-f35d-48c2-bf8a-0f00003bbc72','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('8f806415-f35d-48c2-bf8a-0f00003bbc72','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('8f806415-f35d-48c2-bf8a-0f00003bbc72','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('8f806415-f35d-48c2-bf8a-0f00003bbc72','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('8f806415-f35d-48c2-bf8a-0f00003bbc72','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('8f806415-f35d-48c2-bf8a-0f00003bbc72','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary ''),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('c178caf2-5e06-49dd-ac7f-8d7377af1c7b','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary '');
/*!40000 ALTER TABLE `CLIENT_SCOPE_CLIENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_ROLE_MAPPING` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`ROLE_ID`),
  KEY `IDX_CLSCOPE_ROLE` (`SCOPE_ID`),
  KEY `IDX_ROLE_CLSCOPE` (`ROLE_ID`),
  CONSTRAINT `FK_CL_SCOPE_RM_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_ROLE_MAPPING`
--

LOCK TABLES `CLIENT_SCOPE_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_ROLE_MAPPING` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_ROLE_MAPPING` VALUES ('0a8833d9-b89f-46d6-8cd7-c22f628a2497','2ac60d98-7d47-40b2-80f9-14082f3fb035'),('81cd040d-2997-4011-bd1e-6726f4bd250c','f53f5178-4c49-4103-b299-18831080e719');
/*!40000 ALTER TABLE `CLIENT_SCOPE_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION`
--

DROP TABLE IF EXISTS `CLIENT_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(36) DEFAULT NULL,
  `REDIRECT_URI` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `SESSION_ID` varchar(36) DEFAULT NULL,
  `AUTH_METHOD` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `AUTH_USER_ID` varchar(36) DEFAULT NULL,
  `CURRENT_ACTION` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_SESSION_SESSION` (`SESSION_ID`),
  CONSTRAINT `FK_B4AO2VCVAT6UKAU74WBWTFQO1` FOREIGN KEY (`SESSION_ID`) REFERENCES `USER_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION`
--

LOCK TABLES `CLIENT_SESSION` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_AUTH_STATUS`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_AUTH_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_AUTH_STATUS` (
  `AUTHENTICATOR` varchar(36) NOT NULL,
  `STATUS` int DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`AUTHENTICATOR`),
  CONSTRAINT `AUTH_STATUS_CONSTRAINT` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_AUTH_STATUS`
--

LOCK TABLES `CLIENT_SESSION_AUTH_STATUS` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_AUTH_STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_AUTH_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_NOTE`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_NOTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51C2736` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_NOTE`
--

LOCK TABLES `CLIENT_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_PROT_MAPPER`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_PROT_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_PROT_MAPPER` (
  `PROTOCOL_MAPPER_ID` varchar(36) NOT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`PROTOCOL_MAPPER_ID`),
  CONSTRAINT `FK_33A8SGQW18I532811V7O2DK89` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_PROT_MAPPER`
--

LOCK TABLES `CLIENT_SESSION_PROT_MAPPER` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_PROT_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_PROT_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_ROLE`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_ROLE` (
  `ROLE_ID` varchar(255) NOT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`ROLE_ID`),
  CONSTRAINT `FK_11B7SGQW18I532811V7O2DV76` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_ROLE`
--

LOCK TABLES `CLIENT_SESSION_ROLE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_ROLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_USER_SESSION_NOTE`
--

DROP TABLE IF EXISTS `CLIENT_USER_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_USER_SESSION_NOTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` text,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK_CL_USR_SES_NOTE` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_USER_SESSION_NOTE`
--

LOCK TABLES `CLIENT_USER_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `CLIENT_USER_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_USER_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPONENT`
--

DROP TABLE IF EXISTS `COMPONENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPONENT` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PARENT_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_TYPE` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `SUB_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPONENT_REALM` (`REALM_ID`),
  KEY `IDX_COMPONENT_PROVIDER_TYPE` (`PROVIDER_TYPE`),
  CONSTRAINT `FK_COMPONENT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPONENT`
--

LOCK TABLES `COMPONENT` WRITE;
/*!40000 ALTER TABLE `COMPONENT` DISABLE KEYS */;
INSERT INTO `COMPONENT` VALUES ('00fc84a7-5bd5-4b61-82f6-683d5bb2fa78','Allowed Client Scopes','master','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('0db2c9c7-97d9-4af5-a358-45eb92a52614','rsa-enc-generated','local','rsa-generated','org.keycloak.keys.KeyProvider','local',NULL),('199c2ee4-5182-4a8c-be11-17894753a299','rsa-generated','local','rsa-generated','org.keycloak.keys.KeyProvider','local',NULL),('1c2b0ddd-e196-4692-83fb-ab9039b362c2','Allowed Client Scopes','local','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('1fd8e91a-81ea-4343-b61f-478ec612e538','Allowed Protocol Mapper Types','master','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','authenticated'),('2f7ddf5b-0314-4dc6-9dd6-becefeba98a6','Max Clients Limit','local','max-clients','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('4856a375-6d59-4baf-99c0-de53b9b46ac6','aes-generated','master','aes-generated','org.keycloak.keys.KeyProvider','master',NULL),('4efd5697-0fdd-4242-bdae-82f1d9af0db9','Full Scope Disabled','master','scope','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('58ddf5c4-9b40-4b9c-8cd2-11093a53dbb3','Full Scope Disabled','local','scope','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('668bc673-0ab5-4433-8147-1a1efe50f42f','Consent Required','local','consent-required','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('6daf90ac-b17c-4769-bf79-be2a93731df3','rsa-generated','master','rsa-generated','org.keycloak.keys.KeyProvider','master',NULL),('6dc9886f-ecf8-49b9-993b-40a0f78302d5','rsa-enc-generated','master','rsa-generated','org.keycloak.keys.KeyProvider','master',NULL),('72f4642b-ce7c-413a-ab0c-12b5432050af','Trusted Hosts','master','trusted-hosts','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('79d2c2b0-e94c-4114-bd52-115a91a5cfe4','Allowed Protocol Mapper Types','local','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('7c1f4cbb-ebf8-40b6-bb2d-438d366b40b6','Allowed Client Scopes','master','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','authenticated'),('99334415-8ba7-4cb4-bed5-6f9ac6bd82aa','Consent Required','master','consent-required','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('9a380e82-f3ec-409c-933b-1af963dfa3c6','Trusted Hosts','local','trusted-hosts','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','anonymous'),('a74cd1af-73c0-4d60-b5a9-766959c9a70a','hmac-generated','local','hmac-generated','org.keycloak.keys.KeyProvider','local',NULL),('b2996eec-0f49-45fd-87f0-c152dc31eeb6','aes-generated','local','aes-generated','org.keycloak.keys.KeyProvider','local',NULL),('c9b8104b-ecbb-4a33-a930-17adbe2a134e','Max Clients Limit','master','max-clients','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('ce0a6da1-f71f-43d8-a7c9-0855a55d332d','Allowed Client Scopes','local','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','authenticated'),('ea1deba2-1449-4edb-8a2e-4a06a27649af','Allowed Protocol Mapper Types','master','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('f4cd7efd-56b2-430f-86a4-7a3d0027269e','Allowed Protocol Mapper Types','local','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','local','authenticated'),('f83b0c5f-dff4-406c-8e59-e4c52f9b40cb','hmac-generated','master','hmac-generated','org.keycloak.keys.KeyProvider','master',NULL);
/*!40000 ALTER TABLE `COMPONENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPONENT_CONFIG`
--

DROP TABLE IF EXISTS `COMPONENT_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPONENT_CONFIG` (
  `ID` varchar(36) NOT NULL,
  `COMPONENT_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPO_CONFIG_COMPO` (`COMPONENT_ID`),
  CONSTRAINT `FK_COMPONENT_CONFIG` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `COMPONENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPONENT_CONFIG`
--

LOCK TABLES `COMPONENT_CONFIG` WRITE;
/*!40000 ALTER TABLE `COMPONENT_CONFIG` DISABLE KEYS */;
INSERT INTO `COMPONENT_CONFIG` VALUES ('0a629bef-48a0-4b8c-902a-e71da96145f2','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('0a93f4f9-f007-4880-b227-31ba0da83ce7','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('0f8e5018-f1aa-4529-a33b-0f52b8e5e111','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('115cfae6-d31e-4516-9347-3f8d712a1e52','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','saml-user-property-mapper'),('1457f2c1-3a8f-4896-bb87-9e5fd4458110','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','oidc-full-name-mapper'),('14efdbb3-c653-4b24-a5a4-a33da7758569','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','oidc-address-mapper'),('1590302f-62c3-4a4c-8502-12b9936e1faa','0db2c9c7-97d9-4af5-a358-45eb92a52614','priority','100'),('1620c988-f7e3-439f-902c-a3a1cca3466e','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('2b3556f7-f00c-45a8-a3f4-d590561b34e0','c9b8104b-ecbb-4a33-a930-17adbe2a134e','max-clients','200'),('2bd24102-b758-4003-8624-3adca6dcb805','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('3159a481-e297-4b44-a20f-14c8b2870807','b2996eec-0f49-45fd-87f0-c152dc31eeb6','priority','100'),('31753264-4f43-4248-8aeb-aa5e0b0769d9','199c2ee4-5182-4a8c-be11-17894753a299','keyUse','sig'),('3489b845-c47d-44e6-a8cd-9db178829e1e','a74cd1af-73c0-4d60-b5a9-766959c9a70a','secret','g2TXfcc5mXXoMMOBUb6UMPz-qVxM4gEdebsVasaVCrO4HodbBDQRe6GAcOwwhrnfIUvoXe94dh1BoOyAt7eNsw'),('39013464-ed7a-457f-8a21-8ac5568810e8','6dc9886f-ecf8-49b9-993b-40a0f78302d5','priority','100'),('3aa9724d-f8dc-4e03-a1ee-d796f989c134','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('3d95e9ae-a40d-4f1d-b213-d06443509109','72f4642b-ce7c-413a-ab0c-12b5432050af','client-uris-must-match','true'),('3ed24502-700d-440a-854c-3c296fb901a5','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','saml-role-list-mapper'),('42f01514-82ec-4586-8262-b735da6df82b','7c1f4cbb-ebf8-40b6-bb2d-438d366b40b6','allow-default-scopes','true'),('48269758-c04e-4c46-a39c-9d4d72a483ad','6daf90ac-b17c-4769-bf79-be2a93731df3','certificate','MIICmzCCAYMCBgF7xEMsNTANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjEwOTA4MDcxNDAxWhcNMzEwOTA4MDcxNTQxWjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCbWjwRz/fKe8ZTNxwXPPaJg2k29mvpMy4Zfu8wlsGkfuBmsMUGOZ109oIYEaTSo9mSAe/X5RiXc751U3b2mWKv9xT/xAyVaIFu3YgWtr/ubO0O/fHQLU3Eo3TToKARlY2wjs6T+06Rdjnj6eH+unO6yL2h/yCl+QwNgFkAbUvqrSKoKWpfb+0yZjFq3r8XwPzzMq3lnKu/cyLauwXZh9h9b35/QgXZAWXZT64Ai5C59yMdJfV6Dn+TXxLT9u+57k+U8tG+Wwg2hMe0yByY3ol0fB7jw/s0UOFVYzPz9WJI4FsrQotKSlys+HCa6hwp5GFh1OGXc+LkPWwLkeYwOS0rAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAIJ4QnMAaQBj6S5xN6iv8sa1Ey9a9e9ubs3wcx9gSGUnpbgJXksGsLmYW1fHJxItR7SB4a3NanRAQJqjta98FrjL/VV8uNmWXbbfYOUMROxXxNez9NwaVF7Otye83i28hC7ojpKirj2ZIrWAX4wXnR0qGDIR7VtF3VbNuwD2guSFpIy8CsqeKEpbjUchdK/CXInMTX1QN3HSzMcM7mIRkBswP499cysDyikfCg8Kfy1Ko2Im43XXk9/Xu/n/IAJ4fyjYGoblTHOKfprJyeWAdabfJ4IXRCW0zVV0amouRHlEHZ44S22YdSu4jVqERCm8VvEd3ZVxazzdnRjWA5Hl0tI='),('4841ab3d-1395-49b7-ba5a-a7a6eaae2946','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','oidc-full-name-mapper'),('4d7acffa-a898-493c-967e-2ab55f7ed3cc','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('4db11773-045e-4ca8-9489-47d9adae5624','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','oidc-full-name-mapper'),('4de83979-bba1-4c82-b7b6-7e7276187fe4','6dc9886f-ecf8-49b9-993b-40a0f78302d5','keyUse','enc'),('4e72d06f-6487-45eb-a385-bf5548cf98a8','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('54149054-b972-4e94-bf86-afcb54687b6f','6daf90ac-b17c-4769-bf79-be2a93731df3','privateKey','MIIEowIBAAKCAQEAm1o8Ec/3ynvGUzccFzz2iYNpNvZr6TMuGX7vMJbBpH7gZrDFBjmddPaCGBGk0qPZkgHv1+UYl3O+dVN29plir/cU/8QMlWiBbt2IFra/7mztDv3x0C1NxKN006CgEZWNsI7Ok/tOkXY54+nh/rpzusi9of8gpfkMDYBZAG1L6q0iqClqX2/tMmYxat6/F8D88zKt5Zyrv3Mi2rsF2YfYfW9+f0IF2QFl2U+uAIuQufcjHSX1eg5/k18S0/bvue5PlPLRvlsINoTHtMgcmN6JdHwe48P7NFDhVWMz8/ViSOBbK0KLSkpcrPhwmuocKeRhYdThl3Pi5D1sC5HmMDktKwIDAQABAoIBAQCZ2+2ASrOD88BBLGSC01rsaJzs1XOcm5a9o4yrV7PjAdGngs0vVCKQ84923Qqbn7FcvNRYw0nuvbBUrNw2cUVfN1uxz+4Sj1UZgwhETqyaK4FBBtrWLfhGlwiejAfcQnz47wbDYvK2eBvvkPTFfs4doLDHX1Frb9OzD3mWRakZeuJQBsQT/hz6RoNVCbH0lCHptC3dG71FfUBddVtBo8BpieDFq/VwVggMPSKNk3w+JvgS58c0vZlauGW/6LTEma3+acAeYRc2gJETcONfmboqx1R4OSilzphTNlouVJGV6bW7r6Xh+or2oFHVOfcKIpQVKHF0hoeFT0gX5vajzMyZAoGBANEauTyvCaRX4xIA7cn1QPAbGDxJHKxXs2MwsioeBexj6F15NZ7esvKo6S82xjvIpV70fFCiFDAn4A/J4JcwAUakWyyiXFJGivplRo1jC1oOcLthDDxtafwM30P1cSGRzVmHdoWS5jnLK2tA9goaU2sG6+XvdNyjG5IRDcJMF7d3AoGBAL4xdhwokVmyglfkKDfrUWCtPwz8+nHhyxeullKZkojdaiN7ilpU8KtiM60h7ebOEfIfudWDCc5mKyuRXEKk0qyTj1EdfSwQoD6wEdrvZQj9KjL24osP3vCahcEQhsDxp6U0SKZceVL/25avf3VGPKwIkCC5GhGoT4/jSgOH20ztAoGAKLtJ5fY6rJaqFrGzoClz8tF0URn0yeVzfhjYO7gJc1iqxLp9pWq/9ODOO3Ix/ydAf1Skeph30kGSNcEfTJP/S6FsWS9vSF+RZuEfcx8d8PbrIrTuyz6M7JodS4Gd/eoG1C+8+Jv62LsoxAWtZ55lLW31uZ6BZxEmp7dGtNhZrlUCgYBzUFvTEMmnv7KFhYDY/qTVX9UFk/Bo0cSoNCNSK2iL8OrkXofw4uUUlDLOuu6Z7ME2swOiL5Pnf0yXWse8ZGCTX9gaDy4QZHgz6d65hkIU5CIMymFqfRHngVitSM7a9ul1IbEWpKSurRA/WWKHATHSAFMadAAZSpoKnH0IjPtqtQKBgAnQjM3OKPeEFCa+lCQAosFfLlWFbP4R/vNsTFuDNBTj5L7K5Em+rg2VI6ItlDzVOIzJcp5mKQeNH/gFJKmpqjWpmKigPt1wSJopyIpX1eMdnDISrv1jHfmdLnELNBIrRSAHcErDCv1ByoW/5u1q8rByVkj8VB4MVYGRjxjfmrBQ'),('567b3c86-8169-4a63-86f8-39132ad8de30','f83b0c5f-dff4-406c-8e59-e4c52f9b40cb','priority','100'),('5d9a939f-6c84-4927-887e-15b3c40515c7','b2996eec-0f49-45fd-87f0-c152dc31eeb6','kid','02f0dac6-882d-4dfa-a598-699990c2130e'),('5dffcc4d-172c-411f-bcd7-5fdaf3fdf1bb','199c2ee4-5182-4a8c-be11-17894753a299','certificate','MIICmTCCAYECBgF7xEMyJjANBgkqhkiG9w0BAQsFADAQMQ4wDAYDVQQDDAVsb2NhbDAeFw0yMTA5MDgwNzE0MDNaFw0zMTA5MDgwNzE1NDNaMBAxDjAMBgNVBAMMBWxvY2FsMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmQkhOdu17VOTKowLj/2Rprx6V7Ad4t+1aLnYJncRfOikyPKB5VIKFUdgZfv/kCnqb9Lw400qFp2rbAAVHGG8m72tPkO8xw4KBR6O1vPabBQAFdBRsed0JAvbiSyW0PP4J0Ngqgt6k4I7sxsRGTR6ZRfYzx/8J1B0rP+NNIaUEDpsQko34MS9kGycjo8iPNos4KxloUQ3IAO6Y24Gaqthq9XH3NWAisYBLLEcQqSgVj1SwjJMb31/tqLFxPLCjQSc8wMr3CEXX0jlDM935PqaEONE98aTh6ZzvG63ZADwsMLfzTVOYFoGeEVpK76QomynDDhhAN6+rXxNJ6QPlZ8YXQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAi82CdXqCHNlL9339VbMXUkTHdxVN0oHgklZxL77Tx9RoJOFY5OzO7Ke48VlzvTfkEwS53rWeCRsd6LGBUDpeGhG88uZkkgfXwkf0K2R/f36PZ3CIfzzEnHk0XMb/GXGwaVYgDD1lmLWCwNBJyYnJE0/SJo2R8hudtTkD47Fz+CuG0HM7N0h993WI8uISWpxTc9UUZfrDhZ9GR3DqF4t649bg6m827iwd3VDfqGVsebVVbx9s+JiMdUQ0+pQR0R4hB4WPhDrL6Zoaphj2VaqmiMwumMGALCus5UhHoopdG9mVaCfdJHVcYJnSfrYAzuLBGcnIlqO40q5rJw6yGmsTw'),('5fca55b9-ce2f-4de1-827f-598b0d0ec080','1c2b0ddd-e196-4692-83fb-ab9039b362c2','allow-default-scopes','true'),('66736d00-ad89-4ccf-875a-d6d9d564ceda','2f7ddf5b-0314-4dc6-9dd6-becefeba98a6','max-clients','200'),('683482fc-79d0-4b39-aa47-e89c70382e80','f83b0c5f-dff4-406c-8e59-e4c52f9b40cb','kid','9b4c313d-4f25-46c4-8fd2-8fb23bb8d545'),('6cfd74c9-0b67-4279-8f31-80307f610c13','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','oidc-address-mapper'),('724e0f5b-3fc5-42ca-ab8a-1988ab6ebc35','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('7357fd8a-5b21-4e65-b568-9e3132231aa5','9a380e82-f3ec-409c-933b-1af963dfa3c6','host-sending-registration-request-must-match','true'),('7adb9d3e-0973-4e86-823a-ed82d63cc74c','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','oidc-full-name-mapper'),('81480cc0-4850-4870-908d-eceb913ee6a0','72f4642b-ce7c-413a-ab0c-12b5432050af','host-sending-registration-request-must-match','true'),('841a43a2-e71c-4d61-9e8a-105ee518fe0d','f83b0c5f-dff4-406c-8e59-e4c52f9b40cb','algorithm','HS256'),('878cac24-31c0-4186-a8fa-9ace115d6c2a','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('8ac0cb85-7b56-408b-a9aa-faa8439e489b','a74cd1af-73c0-4d60-b5a9-766959c9a70a','priority','100'),('8d85d0ca-60ac-4a19-8581-bdd2db0a5591','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('923ad7a4-0de4-4096-9866-5341749ec151','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('9619ca2d-0ce8-4db7-a76a-a6708b736447','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','saml-role-list-mapper'),('9769e952-978d-40f6-b9b8-7f922750819b','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','oidc-address-mapper'),('97aa247c-ce13-4cbc-ac84-08c40612f12c','00fc84a7-5bd5-4b61-82f6-683d5bb2fa78','allow-default-scopes','true'),('98b113d7-d4dd-4a40-86dc-efedd5ddb7d0','6dc9886f-ecf8-49b9-993b-40a0f78302d5','certificate','MIICmzCCAYMCBgF7xEMsvDANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjEwOTA4MDcxNDAxWhcNMzEwOTA4MDcxNTQxWjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCeFdgIixWLxp9fPsuou5vwvzR065WeRpFMj52pfT2639OYyqzM5BzcLeoPkKdelJccfvqVX+FtmEyuGU4WwsVXA8VADm4xpkTYzG4Tbeipfzcj9Ag6Are10Sm4WvbwmfhaQbiLLOuKrBu/nX9tv1OK/lEe1DwxW2SPcvn2REHk9cu+YaGaHKvIxqbVX0FevnCX6+FEoQxNMJvDoGSc5sqsYXEJlevMqQRp/DzIR4HqrB1TV1sVAOs2fZDAgUVY7OEtUu/DtoLBam9d7FhjGyOsW3fyy5ZFYHhGbJaB+2spd9tNWRYF7qL3j/68Ecpr5YUf4XzQXudiSL0wlijsmDLVAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFpMt7ADlm8isicoWxqr0lpozyuNcNvgbxA0+dEINjYZPAQxDGdWHiynEeWaXlArdn5oe64vUOXJIMAd7J+pVmGwHpp56/4QkbwiUImK415P85ue8sE4rnGblZnhYJM9Qu3Wfi/jXpEPJpK6L8O5klWCTNYhrXbXR6JDTYA6DQSooS8LHz2BWXX2Y7pPLqcK7/Y8EMyMK3uPzUuPpCzuUC4GmO2mOCVUltq4Our62pcMzd1tP17+IRkE5yTH5DxqaIkWRai0Kc025vOdPec5CJPGyANKxwPU96zNHQKuVt0wN1ATgqg5z8vwJy0AlhpnVYsu6LLLzFUashXAR0tbNNg='),('9d9cf2a7-1caf-45c2-9f35-e907462dfc55','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('a210943b-e94f-41bc-9368-768a19e56ec2','6dc9886f-ecf8-49b9-993b-40a0f78302d5','privateKey','MIIEoQIBAAKCAQEAnhXYCIsVi8afXz7LqLub8L80dOuVnkaRTI+dqX09ut/TmMqszOQc3C3qD5CnXpSXHH76lV/hbZhMrhlOFsLFVwPFQA5uMaZE2MxuE23oqX83I/QIOgK3tdEpuFr28Jn4WkG4iyzriqwbv51/bb9Tiv5RHtQ8MVtkj3L59kRB5PXLvmGhmhyryMam1V9BXr5wl+vhRKEMTTCbw6BknObKrGFxCZXrzKkEafw8yEeB6qwdU1dbFQDrNn2QwIFFWOzhLVLvw7aCwWpvXexYYxsjrFt38suWRWB4RmyWgftrKXfbTVkWBe6i94/+vBHKa+WFH+F80F7nYki9MJYo7Jgy1QIDAQABAoIBAGVKicmvlHajJarZiZTr4Nc5LFteN9faT29G9jaLUOpUUDguZNpDr1pE8RxYC2XGv6w6kET10nNuinNjM23SDq/acqDAnkqhxqtDEaTRX6p7INGgo0TISP3ur1S4tFEl1sO+KIjEl4ebD4yPVf70AIumuEOQqaDWdnCTsyXgRnh1IyKpu4Me41NUjv7W+NlsOKhahCp4LWVrOlT5FJP8xo2jwOyXyo6JAm/i6yEX2yiffNhQJjSE+YbBCN38rgvs1F0PBTJ8IqOs65WIxo6YCA6OpM0vgj6Fkr/dQlYbImyuOz9IDww0Kfe21SokJTUGwLOyeQ66GwJXC1pkhRAE8WECgYEA2cgVBH/h7ayVkVdS0T1R2ZugBxN+w7fW72DSrlm94lJC1dIQbxlaAd8x+CHEgm+XCGM++vKBSzsN3TqdTqHaRUDpNCwRpFbLsUmubqdfHbpIHkktXu1+pL/M2SPGHo/j5q17zcj1bWG0WSTe6i2BuAuBvAas70gXGYdLUCallR8CgYEAudPiypOOvHA5p0wrBPpT2aC7wTDQl7R2ZrDREVYGp54wqRDXbYrz/zhiBpbKCBxBSK9xZFlxvK+IYb1buKAudKayxz1Px9Ypyy79m46UuMxVqJmLyL4Cl799u5d7IubT2KUcjHFug+NmOY518LiilV4xu5xeEnXfCPNHb6TFZYsCf1KEfXI3AmQMAKARdk/R7DGHxhroSB/giJArH6jwWAJpKOnbrl5qPLB44XYjVVIW9iM3d6WePV9EAPzSOMiSiyGydqpOCGE8gfGq+Wzz/hZFfw36rDZLxrCl+/hKlcb+vlupYD+Y5WWMn8knL0+L7prNwVp9LQl9R0NlACRegF8CgYB5YWXqXKgHfmaqWJ5hCgJrTQpXDsOx1DbirOxjHoYKRtISHOYar4DhXLrrAuPEmHAsQi0XFzWrwhybe0zGNBskJx7v9aR/K+zwwyj0yxVqAE6TanhcD4EyDera9f+AtLX79R3Ivu8pRwgm4F85deD9ICHsIh9wf33IIuCulDo8MQKBgQCSaizIYOWWUlhUOljQl0Q8biI7OFDh4Hy6vhD+m/KAKHD1MrMK2zf6eM8z2vMVymv1BDBh981l3Ptk6kvSjj0IDA5pxKP0sJjh+pu1Y26v3wOLk/LIXFgJHW/ZEBmLL0yyN1T/MFDhaLDzhOkEdkPgXqgNvVhgXh0z5UrwiP4/zw=='),('a4ab61f9-2a95-41a3-8a9a-f2c08cbb8def','b2996eec-0f49-45fd-87f0-c152dc31eeb6','secret','1pEypqKrq1MwCJardOQFxg'),('a4fdc575-4f83-4d1d-8678-a5818e19f2fd','6daf90ac-b17c-4769-bf79-be2a93731df3','keyUse','sig'),('a680dd2b-5a58-40c7-8df6-566a998197dd','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','saml-user-property-mapper'),('aaab10cd-ec38-44e8-8851-7eed6b3cf04f','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('ad53ba76-4378-4f9d-a194-bac9302875c9','4856a375-6d59-4baf-99c0-de53b9b46ac6','priority','100'),('b667712b-4cbf-4724-a394-8003bbee4818','4856a375-6d59-4baf-99c0-de53b9b46ac6','secret','paztF7akHf_y59JcN9BIfA'),('b7d0baaa-868b-4e9b-9c1c-e06d8f70945d','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','saml-role-list-mapper'),('b93d6626-790d-4339-a87c-919f433d7e72','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','oidc-address-mapper'),('bcb29fbb-346b-4d37-8c24-b5241ddc508f','a74cd1af-73c0-4d60-b5a9-766959c9a70a','algorithm','HS256'),('c175a861-3ebd-4e98-9b84-8eac07aeddb2','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('c3df9d52-5fd0-48e2-b3fd-3af4579e7373','0db2c9c7-97d9-4af5-a358-45eb92a52614','certificate','MIICmTCCAYECBgF7xEMykjANBgkqhkiG9w0BAQsFADAQMQ4wDAYDVQQDDAVsb2NhbDAeFw0yMTA5MDgwNzE0MDNaFw0zMTA5MDgwNzE1NDNaMBAxDjAMBgNVBAMMBWxvY2FsMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAisgqb62dKKFi1ThcY4bzEeUPXZ0iLvJrrAbJs78f7cHIMDScKFVtoLFb58eQ2olch8ECxYhS64PQDeUXQHIaAbpx5H7BVvmMvjLoo5pVTDFIPprgSxISt0q24VO/aHeGcySX+aMB/xNf3ag8WZVpIN2fx/0+rAGaASThrHuPXmcFhxEcu09dg8S+fcueELZ/W0jqOB5WB9iIX+AmJXACx39MhhtzhBilWTIQHRSlGcBKN4tbRWGSgen9m2jS/GOAnI8quTN4gDJQFHSFd6v9biMVy3B5jhhdbXocMen9qkXjOwrzEVPZ7whdPQC7Cyhv7z7CAT94F2dZ3xiYHVthCQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBoAAGqVCmyqpQ3zoCpsFxSKzeFK/guEXJsYYnWYK6Bx8SZAUYGBiOG9MtcV382t605QIX/jjRNlLOZ3nfID2MbQBZnJmaX8Oh8Sm0IpLaK7lmIaJAVsBZMZhaL7y/AEYJYHFXziqLO8wIX53vhvKqUKVDtSiT+lG3n/xDuXHQ/PLjDUVbHatWpKNucEx3J2VBkjbST34KoNqKjqerjpP5I+sg7izOevUXS6nJ0kknfjz+5Et0gc2BmDW75tkWvIa8zhACR/jRs4fZs9nn/FJS3redoVjjN8RjvQ1n2RIzgO5qyznMWdigyYM6d6Me57oH/qzrXXHDErU7gkOHOSirx'),('c5ea174e-17d2-4a67-bbb4-b505fa971571','199c2ee4-5182-4a8c-be11-17894753a299','privateKey','MIIEogIBAAKCAQEAmQkhOdu17VOTKowLj/2Rprx6V7Ad4t+1aLnYJncRfOikyPKB5VIKFUdgZfv/kCnqb9Lw400qFp2rbAAVHGG8m72tPkO8xw4KBR6O1vPabBQAFdBRsed0JAvbiSyW0PP4J0Ngqgt6k4I7sxsRGTR6ZRfYzx/8J1B0rP+NNIaUEDpsQko34MS9kGycjo8iPNos4KxloUQ3IAO6Y24Gaqthq9XH3NWAisYBLLEcQqSgVj1SwjJMb31/tqLFxPLCjQSc8wMr3CEXX0jlDM935PqaEONE98aTh6ZzvG63ZADwsMLfzTVOYFoGeEVpK76QomynDDhhAN6+rXxNJ6QPlZ8YXQIDAQABAoIBAG4TDg4SxbmtyoXo84wfZDtdCFa4O9bq+A4a1qlcLXz9zUEohVwP9Wa2z1SrvHfSXmkOfevQnfxkbl+hc43VkxcmiEzn5gn7RCPIhg+AmNPyUtld3NUs2PHwgXkaVGR5hz2ejhHhbcpZ+1zplBPgFkVOsMjShck1aqbgrY1hW3CLo61MEilhiMNA+OEMfW9KXcOJKnDg+z0q52f0sWiJoLC6Nuolt4Ryow/XPB8vgG6gqBDUzB//jtUiQSn7NlbmErI1MaEluhLtH/CyNEVWXxDJ1FFPLbERRBLGZ4W3viKE7/gIu+bhFnHV3YxvBEVkkLlDBVRD+y/060RbIOCKe/UCgYEA7e8mTGSwNg4iI27tPB1wmAqVH/87YAENBOBkUm2/9NKvP5PoGh/hD3li4D2OuODPWM31v6siGkgRCIHHY8AKdibEUgCzWlV0p9Rk7dsRoVyQ8vLHCz8r+Q8qWLEyvlC2PqdBDqtd1zSSKcYgvAvKgPRFDU5kHRmOYeMkxe3VZFMCgYEApKfDemic1FbBCVemCNZU+hJGW1FTblV4qeg5GcUQbzr8Zw6+i59qCybX2CgxJDNvRqfAKKnFEZEJq5mt3pMgaG6soQcntS6WtaHC0lXW2jLyCFyOL+3P9VoMlAcZnqsV6HF5xmWkVckAap5exSsVcWRNgbihYIi8bAYOydxh+o8CgYBI1uCTnHNxaOw8irXnpF+WvA6hCALGRiwVJRixP1X3fhQWU5JHhSDiGtgRK+coY/I2Dn9AGQTMpOlKAGjo+Z56Qy3wqLQ/YE/hHR10z47bJjpdlcMnh0RP77uYAU82ggD4tz0sX3vP6REh5ryYtMdeqOkGxMvTOMKk5uOYEFI2bwKBgFER+QFwOKej/2+mJC39ueFW1iVoKXgNxVeC63pwuOLLXDMx40fDuea7vVLEl02HtaZIOijLTiiUDPrcs+r+7dn9hCnU6AwhYDsV5mLBuQO+ScUsLSYUJWxcZg0MYeTpv+Ou25J/wSYrzXSxfV//EpO+Gxtf2LU7HeriIK3rQKKpAoGAUBta0KR4zFG9pDfqPG1MOKLvuQCjp7l7nR0ZpBkkpcAoiiFhMq0cLHvRoAAna+sY1krPme7PS+kRl64JVDYo09tFmDCr6tQ9rTu961H2CCX7FOKnnhlYyMujvYQtMgYhXYyXLjH0QyPHDoY1JWG8KWRRnWB+JTu7l7CkD6TiZEU='),('c6f2cc9c-710d-469b-8dbe-be340b8d5c50','79d2c2b0-e94c-4114-bd52-115a91a5cfe4','allowed-protocol-mapper-types','saml-role-list-mapper'),('c7ae4539-d661-41d6-b8e0-7cb003da7ce5','0db2c9c7-97d9-4af5-a358-45eb92a52614','keyUse','enc'),('cb37ed86-4f33-4196-ae7b-1999b4387301','ce0a6da1-f71f-43d8-a7c9-0855a55d332d','allow-default-scopes','true'),('cba000d0-6ced-44a2-ad8c-31d5af3596ad','0db2c9c7-97d9-4af5-a358-45eb92a52614','privateKey','MIIEowIBAAKCAQEAisgqb62dKKFi1ThcY4bzEeUPXZ0iLvJrrAbJs78f7cHIMDScKFVtoLFb58eQ2olch8ECxYhS64PQDeUXQHIaAbpx5H7BVvmMvjLoo5pVTDFIPprgSxISt0q24VO/aHeGcySX+aMB/xNf3ag8WZVpIN2fx/0+rAGaASThrHuPXmcFhxEcu09dg8S+fcueELZ/W0jqOB5WB9iIX+AmJXACx39MhhtzhBilWTIQHRSlGcBKN4tbRWGSgen9m2jS/GOAnI8quTN4gDJQFHSFd6v9biMVy3B5jhhdbXocMen9qkXjOwrzEVPZ7whdPQC7Cyhv7z7CAT94F2dZ3xiYHVthCQIDAQABAoIBAEQFnBck51TU1VZgy5fUuyrYNNHThL+x/mhQ42gAk6u7QV2vS6LRTGxowf026Z7TmNDJH4ymJs5cywskcGMcCfcy5UPzhNwWcI+A8FnP+WLg/OICk1D4SMgiDMUggT645uNk2kB3TzEw09oW7Y2TF/YN6pThYWh5WHybESmzXjAMYbLd5Dz9OvckVcoATz0IKoPZi2GiH4Gn5rHzpjXgxJqZaOeNfvgJso3xloARUHTJbnuUumxEW+VHTiN5EMub6O2Q3eW2vP8UfEAvpxN2U+GxKhWEpCZPkeiU/9xuptmLXXj7zBM0SMq4dSdAsvNJ5DeTfB0vq9lFe2BNuEhyEIkCgYEAxw53s7GJFa8+/NAo2b40StRQ/68sgdwS7n1dUA2BIZxPIY0p2sCZuGqiBLTG6w6bwWRCxyfjiJrIkpXDbDcuDYR8w1hP6sUijuNwS1t3MeF0P+UnjMiWu5C62UJmdsX2nmJWp1PqeSAYKWrvPp72efIhoThHokVWsQzbM8bNSOcCgYEAsnuZXp9yuj0vX9tE3/tr4MAgQ7SfQSY2vCfDqOzr2Oa6osvAuQ9szEJizjQapzwDZxLrUW73q28VJkUbRRnvpby1uLIgRsVMdOKQe2+rNoBq1y9ohONQoiHN+EfgdVdXY8w2t9kD+QkzwP8lR+s9dP8sHQix4rlNBCOJf0IWGI8CgYBMBuuTN2C7ZoaxOrQPjkUcu4EAVyCxrKOScVFdjPrgInpavzbYNS3BMQEWuREsoBS/RZL3y/VsrGmlCwxr3Zt6UDryxV0otwie+KFgNtwMsiabbOOxMr/XfczPxDusa6npDng4SqXIg4iW8R/VlBNhuig8EmnRwZf94YIrgWfQywKBgQCLt9PUzc762cDHMhfVmrTVSLE5LyDn2ClK23oAOA9cSSpbHFNuyxNpRqKgNwuSVJl5S8cohUyeSCPvRHPXgTknrfU3c0FvBS4X5fAioMUPsjFw6CzSsjvB3+Kj9GhSkx/XANroypqgjcKU12bwyMbgWozG4bKXAGW40rTlCO/RIwKBgG/3Bqm3D4JvAstqr1PsXZUuSjP5+w2cVe7BKJdU9x5BKRAeE7ed/uAeqMjqomfmeSt39LqnGMpB6F+OoTEzyreZ2QAyS8e13LHKIFxX+PvUDT4KMQVe9MhaRVcC57E8Cq5s498UifeL8ib31Z1E9fQxc+GMQp9GCmMCzL9wGc7S'),('cd65fc8f-3ec7-4869-8724-d1d4847cba51','f4cd7efd-56b2-430f-86a4-7a3d0027269e','allowed-protocol-mapper-types','saml-user-property-mapper'),('cdc4a82f-8ac2-4f28-9168-536687822a59','199c2ee4-5182-4a8c-be11-17894753a299','priority','100'),('d83a5c94-ab70-41fd-895b-5f2b82476788','ea1deba2-1449-4edb-8a2e-4a06a27649af','allowed-protocol-mapper-types','saml-user-property-mapper'),('de7485c6-0cf0-43a1-88ca-4eed686262ee','1fd8e91a-81ea-4343-b61f-478ec612e538','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('e30e496b-abf2-4190-9cb1-a4b2b3512635','a74cd1af-73c0-4d60-b5a9-766959c9a70a','kid','d7e3d7df-44e6-44c1-9433-ccf5b729f881'),('e8a227a3-4316-42c0-b372-6fe15b55c9e8','4856a375-6d59-4baf-99c0-de53b9b46ac6','kid','6c0a39b0-e16b-4468-9cf4-84a90cf2a150'),('f0b820d0-472a-4b7b-b6de-249e57c7ac04','6daf90ac-b17c-4769-bf79-be2a93731df3','priority','100'),('f6335efa-714a-420a-8ac1-13f678b6b982','9a380e82-f3ec-409c-933b-1af963dfa3c6','client-uris-must-match','true'),('fcee4767-80ee-4205-8d05-bff2e9465f17','f83b0c5f-dff4-406c-8e59-e4c52f9b40cb','secret','685bdkfxPr9Xrt-w_v_mtLaakoiwZ6XQSA60KgGcZcgUR6qFtm1Xhoevl3DlOLrTfG6ipiQHn7yEpGS7vMVXaw');
/*!40000 ALTER TABLE `COMPONENT_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPOSITE_ROLE`
--

DROP TABLE IF EXISTS `COMPOSITE_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPOSITE_ROLE` (
  `COMPOSITE` varchar(36) NOT NULL,
  `CHILD_ROLE` varchar(36) NOT NULL,
  PRIMARY KEY (`COMPOSITE`,`CHILD_ROLE`),
  KEY `IDX_COMPOSITE` (`COMPOSITE`),
  KEY `IDX_COMPOSITE_CHILD` (`CHILD_ROLE`),
  CONSTRAINT `FK_A63WVEKFTU8JO1PNJ81E7MCE2` FOREIGN KEY (`COMPOSITE`) REFERENCES `KEYCLOAK_ROLE` (`ID`),
  CONSTRAINT `FK_GR7THLLB9LU8Q4VQA4524JJY8` FOREIGN KEY (`CHILD_ROLE`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPOSITE_ROLE`
--

LOCK TABLES `COMPOSITE_ROLE` WRITE;
/*!40000 ALTER TABLE `COMPOSITE_ROLE` DISABLE KEYS */;
INSERT INTO `COMPOSITE_ROLE` VALUES ('01510ddd-eac3-4d27-87f5-b2073abceda0','d6600a99-fe67-456f-8ec3-bca6a9f8f2a2'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','06ad8eee-6bde-42e3-9362-212bb3874b25'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','17beb223-28d4-41e3-967b-8f3c3a67122b'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','b92960e3-b610-4524-bb65-5419773e429a'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','f53f5178-4c49-4103-b299-18831080e719'),('20c76a23-b87c-4a58-a881-ee204de3b846','1e719afc-3279-4103-9f66-a60a3f8d05c1'),('2d756545-820d-4cbc-b379-799edda1f215','b91a8a31-b3c2-40b4-879a-08f81746dc61'),('416dab4b-6a89-4b83-b93c-54c9cb06e8c2','213fdfba-4f44-49bc-9eeb-e67532302ea0'),('416dab4b-6a89-4b83-b93c-54c9cb06e8c2','3945086a-c229-40d5-9ba0-4a0d93e25154'),('42d5cde3-10be-4922-900e-76eaa8dd1800','2ac60d98-7d47-40b2-80f9-14082f3fb035'),('42d5cde3-10be-4922-900e-76eaa8dd1800','9f368f44-a5cc-4962-9ce1-bc3dcbda77bf'),('42d5cde3-10be-4922-900e-76eaa8dd1800','f01e2c42-90df-4a42-bd08-4eb0d6d73211'),('42d5cde3-10be-4922-900e-76eaa8dd1800','fb0ee30e-91d7-41f1-b3d4-f67e07af2bc6'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','01216dcc-c4c7-49bd-b348-763d91844b49'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','01510ddd-eac3-4d27-87f5-b2073abceda0'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','03d6afd7-f2cf-42c4-a526-e1b6d17745ef'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','1b7c21a7-4cb1-4130-b8cb-33bcc1712fc4'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','213fdfba-4f44-49bc-9eeb-e67532302ea0'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','216ffa95-308f-490e-91da-6a5be36b664f'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','2e7a0c98-bdf1-4fef-bb51-dabb572997c6'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','3945086a-c229-40d5-9ba0-4a0d93e25154'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','3ab03f72-bd92-4e9d-9fb4-bde7ba19da2c'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','416dab4b-6a89-4b83-b93c-54c9cb06e8c2'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','57fa9b29-114e-4925-9f9d-41742331f262'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','5d831418-88a4-43d7-86f0-2ce0dbed5249'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','76f2d4d9-aeea-495c-97a0-ea9f7bedc7dc'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','7b2f2afa-efa7-4b08-8b0e-dadb113b8c53'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','7f2fadf0-cf66-44c3-b59b-0b3511828c96'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','89260c4a-d4bb-42ec-8178-e2eccc260ba2'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','8937eff8-584b-432f-9e58-d17dad9bb7f2'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','89cf2724-6d1f-4dd5-97b0-c6c78d397591'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','8ab23308-383e-4739-928d-772d57c59b6d'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','904dbb57-b146-4106-876a-fad4b87acc8b'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','949b0c8a-df4e-45df-93cd-fd05751d6988'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','957ab799-e4e9-4f38-8c2c-d885edd30412'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','a642f38f-1935-46eb-9268-b3d97b797273'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','a8e41e99-dc5f-4780-90c1-50788f769c4c'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','b37ff5b8-190c-465b-bc11-9ec13b5d0646'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','bda5a968-b1f7-42cc-8d3e-7074934ca4e4'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','be415c40-fac3-4aec-a97c-36af2749a464'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','c1556287-405b-4b8a-9836-17a8b1e7aca0'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','c5e2e223-f202-4bbb-9aef-caa1a31d8e06'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','d1d2a346-5419-49f9-8710-371898d9e865'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','d544863a-4afe-419a-8f68-49afb552f336'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','d5470f9f-164e-4d73-bf73-449839b4af30'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','d6600a99-fe67-456f-8ec3-bca6a9f8f2a2'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','dc9ffc08-fef9-419b-a2d8-f9c4f90c1535'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','e1219c8e-40b5-49a2-bf9e-e49849950d91'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','ebd60f62-1d8f-4a10-aab8-5958063a496c'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','f7f2a3e5-7153-4f78-8666-a81ad59a4250'),('770a9e69-89ce-4165-b334-91a9610922c3','bed1266f-0b1a-4342-9cd0-686c5ce71981'),('770a9e69-89ce-4165-b334-91a9610922c3','db12d2c2-8969-486a-b410-dca0ae52ba07'),('8021fc26-a418-473a-bebb-60299cfaf572','1140d4dd-800c-404e-8edd-f462a4683723'),('8021fc26-a418-473a-bebb-60299cfaf572','1f09ec7b-c8d9-49c4-a74f-6a85cb7756c3'),('8021fc26-a418-473a-bebb-60299cfaf572','477bc87f-70a3-4fdc-92de-d117dab5712c'),('8021fc26-a418-473a-bebb-60299cfaf572','49f1cf0d-0992-4766-9870-f28b0b16adef'),('8021fc26-a418-473a-bebb-60299cfaf572','5f030686-62a7-4e8d-a5f1-3281f312cde3'),('8021fc26-a418-473a-bebb-60299cfaf572','6428972c-ddd7-477e-979f-96aa6833230d'),('8021fc26-a418-473a-bebb-60299cfaf572','770a9e69-89ce-4165-b334-91a9610922c3'),('8021fc26-a418-473a-bebb-60299cfaf572','7c9d7d82-c3b9-4ec5-990b-6e71b04327f7'),('8021fc26-a418-473a-bebb-60299cfaf572','978b6aab-f691-4b98-840e-5cd0a32d18e9'),('8021fc26-a418-473a-bebb-60299cfaf572','98dbb4e4-ffbb-4007-9721-25ddcf10c7ac'),('8021fc26-a418-473a-bebb-60299cfaf572','a6ccea86-ccc9-41ef-bd37-756d6329a11e'),('8021fc26-a418-473a-bebb-60299cfaf572','b46b4f62-0d69-42a7-9481-2eda7746d8da'),('8021fc26-a418-473a-bebb-60299cfaf572','bed1266f-0b1a-4342-9cd0-686c5ce71981'),('8021fc26-a418-473a-bebb-60299cfaf572','d06bbb68-7c74-44f4-a2de-3051a4491fc2'),('8021fc26-a418-473a-bebb-60299cfaf572','d5ea15a5-bab2-4127-9c95-c2f3e999d005'),('8021fc26-a418-473a-bebb-60299cfaf572','db12d2c2-8969-486a-b410-dca0ae52ba07'),('8021fc26-a418-473a-bebb-60299cfaf572','f316daf5-45ac-4008-9bfb-e3696556c735'),('8021fc26-a418-473a-bebb-60299cfaf572','f9757cc8-7e2d-44e3-a27b-736674328854'),('a6ccea86-ccc9-41ef-bd37-756d6329a11e','49f1cf0d-0992-4766-9870-f28b0b16adef'),('b92960e3-b610-4524-bb65-5419773e429a','ebc04247-2c5f-4695-ab6d-187b27e348a2'),('d1d2a346-5419-49f9-8710-371898d9e865','8ab23308-383e-4739-928d-772d57c59b6d'),('d1d2a346-5419-49f9-8710-371898d9e865','f7f2a3e5-7153-4f78-8666-a81ad59a4250'),('d5470f9f-164e-4d73-bf73-449839b4af30','8937eff8-584b-432f-9e58-d17dad9bb7f2'),('fb0ee30e-91d7-41f1-b3d4-f67e07af2bc6','ff2c1a1b-d7f5-4b02-b211-7a6dce6395f0');
/*!40000 ALTER TABLE `COMPOSITE_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CREDENTIAL`
--

DROP TABLE IF EXISTS `CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CREDENTIAL` (
  `ID` varchar(36) NOT NULL,
  `SALT` tinyblob,
  `TYPE` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `USER_LABEL` varchar(255) DEFAULT NULL,
  `SECRET_DATA` longtext,
  `CREDENTIAL_DATA` longtext,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_CREDENTIAL` (`USER_ID`),
  CONSTRAINT `FK_PFYR0GLASQYL0DEI3KL69R6V0` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CREDENTIAL`
--

LOCK TABLES `CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `CREDENTIAL` DISABLE KEYS */;
INSERT INTO `CREDENTIAL` VALUES ('22383eee-0c98-469e-9dc1-f25d9acf702a',NULL,'password','5ced3fd1-588e-4b21-adb2-9137a8081ae4',1631085791971,NULL,'{\"value\":\"55j7mL4CJnDi4qYIxiHeVK591r39ZHyJKiyWNFd8Bka7IGdsR95msyhJD1g3xoclLSvOJEeNnnOyNvBAvh5xcg==\",\"salt\":\"qrPdaMp7Xt8+cT+BD6eeZg==\",\"additionalParameters\":{}}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\",\"additionalParameters\":{}}',10),('9f1b33c1-408a-4967-a23c-df02778449fc',NULL,'password','c76178e9-8786-4fc8-a9b7-081013372cdf',1631086014497,NULL,'{\"value\":\"z2ObMLjgnTNX9U0FQeRCvCr0s3ZqRcT4Dox9ZP5rS3Ic0HgBRoc3J9J+4XubOfDpnIIvUAbGwy5YGfsPwKsSjQ==\",\"salt\":\"NHfjEBUbK+wjP3sucFxh7w==\",\"additionalParameters\":{}}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\",\"additionalParameters\":{}}',10),('a9783d05-22aa-445c-80d4-299658bd8cc7',NULL,'password','461c341c-ed1d-4d26-aab4-1b053c7d6ec2',1631085343604,NULL,'{\"value\":\"K5idYRPxLLuq5HsbCdrVgQBV9RKSCa8J3yp/C40hHWUwnIdXEQ7+VWCzXvx1cXYctD0gkwixsYNYveMLoxl/6w==\",\"salt\":\"z6x/FEcgs4Z0QVhrbzj57w==\",\"additionalParameters\":{}}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\",\"additionalParameters\":{}}',10);
/*!40000 ALTER TABLE `CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOG`
--

LOCK TABLES `DATABASECHANGELOG` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOG` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOG` VALUES ('1.0.0.Final-KEYCLOAK-5461','sthorger@redhat.com','META-INF/jpa-changelog-1.0.0.Final.xml','2021-09-08 07:15:34',1,'EXECUTED','7:4e70412f24a3f382c82183742ec79317','createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.0.0.Final-KEYCLOAK-5461','sthorger@redhat.com','META-INF/db2-jpa-changelog-1.0.0.Final.xml','2021-09-08 07:15:34',2,'MARK_RAN','7:cb16724583e9675711801c6875114f28','createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.1.0.Beta1','sthorger@redhat.com','META-INF/jpa-changelog-1.1.0.Beta1.xml','2021-09-08 07:15:34',3,'EXECUTED','7:0310eb8ba07cec616460794d42ade0fa','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.1.0.Final','sthorger@redhat.com','META-INF/jpa-changelog-1.1.0.Final.xml','2021-09-08 07:15:34',4,'EXECUTED','7:5d25857e708c3233ef4439df1f93f012','renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.2.0.Beta1','psilva@redhat.com','META-INF/jpa-changelog-1.2.0.Beta1.xml','2021-09-08 07:15:34',5,'EXECUTED','7:c7a54a1041d58eb3817a4a883b4d4e84','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.2.0.Beta1','psilva@redhat.com','META-INF/db2-jpa-changelog-1.2.0.Beta1.xml','2021-09-08 07:15:34',6,'MARK_RAN','7:2e01012df20974c1c2a605ef8afe25b7','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.2.0.RC1','bburke@redhat.com','META-INF/jpa-changelog-1.2.0.CR1.xml','2021-09-08 07:15:34',7,'EXECUTED','7:0f08df48468428e0f30ee59a8ec01a41','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.2.0.RC1','bburke@redhat.com','META-INF/db2-jpa-changelog-1.2.0.CR1.xml','2021-09-08 07:15:34',8,'MARK_RAN','7:a77ea2ad226b345e7d689d366f185c8c','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.2.0.Final','keycloak','META-INF/jpa-changelog-1.2.0.Final.xml','2021-09-08 07:15:34',9,'EXECUTED','7:a3377a2059aefbf3b90ebb4c4cc8e2ab','update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.3.0','bburke@redhat.com','META-INF/jpa-changelog-1.3.0.xml','2021-09-08 07:15:35',10,'EXECUTED','7:04c1dbedc2aa3e9756d1a1668e003451','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.4.0','bburke@redhat.com','META-INF/jpa-changelog-1.4.0.xml','2021-09-08 07:15:35',11,'EXECUTED','7:36ef39ed560ad07062d956db861042ba','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.4.0','bburke@redhat.com','META-INF/db2-jpa-changelog-1.4.0.xml','2021-09-08 07:15:35',12,'MARK_RAN','7:d909180b2530479a716d3f9c9eaea3d7','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.5.0','bburke@redhat.com','META-INF/jpa-changelog-1.5.0.xml','2021-09-08 07:15:35',13,'EXECUTED','7:cf12b04b79bea5152f165eb41f3955f6','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.6.1_from15','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2021-09-08 07:15:35',14,'EXECUTED','7:7e32c8f05c755e8675764e7d5f514509','addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.6.1_from16-pre','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2021-09-08 07:15:35',15,'MARK_RAN','7:980ba23cc0ec39cab731ce903dd01291','delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.6.1_from16','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2021-09-08 07:15:35',16,'MARK_RAN','7:2fa220758991285312eb84f3b4ff5336','dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.6.1','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2021-09-08 07:15:35',17,'EXECUTED','7:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.7.0','bburke@redhat.com','META-INF/jpa-changelog-1.7.0.xml','2021-09-08 07:15:35',18,'EXECUTED','7:91ace540896df890cc00a0490ee52bbc','createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.8.0','mposolda@redhat.com','META-INF/jpa-changelog-1.8.0.xml','2021-09-08 07:15:35',19,'EXECUTED','7:c31d1646dfa2618a9335c00e07f89f24','addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.8.0-2','keycloak','META-INF/jpa-changelog-1.8.0.xml','2021-09-08 07:15:35',20,'EXECUTED','7:df8bc21027a4f7cbbb01f6344e89ce07','dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.8.0','mposolda@redhat.com','META-INF/db2-jpa-changelog-1.8.0.xml','2021-09-08 07:15:35',21,'MARK_RAN','7:f987971fe6b37d963bc95fee2b27f8df','addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.8.0-2','keycloak','META-INF/db2-jpa-changelog-1.8.0.xml','2021-09-08 07:15:35',22,'MARK_RAN','7:df8bc21027a4f7cbbb01f6344e89ce07','dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.9.0','mposolda@redhat.com','META-INF/jpa-changelog-1.9.0.xml','2021-09-08 07:15:35',23,'EXECUTED','7:ed2dc7f799d19ac452cbcda56c929e47','update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.9.1','keycloak','META-INF/jpa-changelog-1.9.1.xml','2021-09-08 07:15:35',24,'EXECUTED','7:80b5db88a5dda36ece5f235be8757615','modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.9.1','keycloak','META-INF/db2-jpa-changelog-1.9.1.xml','2021-09-08 07:15:35',25,'MARK_RAN','7:1437310ed1305a9b93f8848f301726ce','modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('1.9.2','keycloak','META-INF/jpa-changelog-1.9.2.xml','2021-09-08 07:15:36',26,'EXECUTED','7:b82ffb34850fa0836be16deefc6a87c4','createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-2.0.0','psilva@redhat.com','META-INF/jpa-changelog-authz-2.0.0.xml','2021-09-08 07:15:36',27,'EXECUTED','7:9cc98082921330d8d9266decdd4bd658','createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-2.5.1','psilva@redhat.com','META-INF/jpa-changelog-authz-2.5.1.xml','2021-09-08 07:15:36',28,'EXECUTED','7:03d64aeed9cb52b969bd30a7ac0db57e','update tableName=RESOURCE_SERVER_POLICY','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.1.0-KEYCLOAK-5461','bburke@redhat.com','META-INF/jpa-changelog-2.1.0.xml','2021-09-08 07:15:36',29,'EXECUTED','7:f1f9fd8710399d725b780f463c6b21cd','createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.2.0','bburke@redhat.com','META-INF/jpa-changelog-2.2.0.xml','2021-09-08 07:15:36',30,'EXECUTED','7:53188c3eb1107546e6f765835705b6c1','addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.3.0','bburke@redhat.com','META-INF/jpa-changelog-2.3.0.xml','2021-09-08 07:15:36',31,'EXECUTED','7:d6e6f3bc57a0c5586737d1351725d4d4','createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.4.0','bburke@redhat.com','META-INF/jpa-changelog-2.4.0.xml','2021-09-08 07:15:36',32,'EXECUTED','7:454d604fbd755d9df3fd9c6329043aa5','customChange','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.0','bburke@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2021-09-08 07:15:36',33,'EXECUTED','7:57e98a3077e29caf562f7dbf80c72600','customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.0-unicode-oracle','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2021-09-08 07:15:36',34,'MARK_RAN','7:e4c7e8f2256210aee71ddc42f538b57a','modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.0-unicode-other-dbs','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2021-09-08 07:15:36',35,'EXECUTED','7:09a43c97e49bc626460480aa1379b522','modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.0-duplicate-email-support','slawomir@dabek.name','META-INF/jpa-changelog-2.5.0.xml','2021-09-08 07:15:36',36,'EXECUTED','7:26bfc7c74fefa9126f2ce702fb775553','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.0-unique-group-names','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2021-09-08 07:15:36',37,'EXECUTED','7:a161e2ae671a9020fff61e996a207377','addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'1085333512'),('2.5.1','bburke@redhat.com','META-INF/jpa-changelog-2.5.1.xml','2021-09-08 07:15:36',38,'EXECUTED','7:37fc1781855ac5388c494f1442b3f717','addColumn tableName=FED_USER_CONSENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.0.0','bburke@redhat.com','META-INF/jpa-changelog-3.0.0.xml','2021-09-08 07:15:36',39,'EXECUTED','7:13a27db0dae6049541136adad7261d27','addColumn tableName=IDENTITY_PROVIDER','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.2.0-fix','keycloak','META-INF/jpa-changelog-3.2.0.xml','2021-09-08 07:15:36',40,'MARK_RAN','7:550300617e3b59e8af3a6294df8248a3','addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.2.0-fix-with-keycloak-5416','keycloak','META-INF/jpa-changelog-3.2.0.xml','2021-09-08 07:15:36',41,'MARK_RAN','7:e3a9482b8931481dc2772a5c07c44f17','dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.2.0-fix-offline-sessions','hmlnarik','META-INF/jpa-changelog-3.2.0.xml','2021-09-08 07:15:36',42,'EXECUTED','7:72b07d85a2677cb257edb02b408f332d','customChange','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.2.0-fixed','keycloak','META-INF/jpa-changelog-3.2.0.xml','2021-09-08 07:15:37',43,'EXECUTED','7:a72a7858967bd414835d19e04d880312','addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.3.0','keycloak','META-INF/jpa-changelog-3.3.0.xml','2021-09-08 07:15:37',44,'EXECUTED','7:94edff7cf9ce179e7e85f0cd78a3cf2c','addColumn tableName=USER_ENTITY','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-3.4.0.CR1-resource-server-pk-change-part1','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2021-09-08 07:15:37',45,'EXECUTED','7:6a48ce645a3525488a90fbf76adf3bb3','addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095','hmlnarik@redhat.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2021-09-08 07:15:37',46,'EXECUTED','7:e64b5dcea7db06077c6e57d3b9e5ca14','customChange','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2021-09-08 07:15:37',47,'MARK_RAN','7:fd8cf02498f8b1e72496a20afc75178c','dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2021-09-08 07:15:37',48,'EXECUTED','7:542794f25aa2b1fbabb7e577d6646319','addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authn-3.4.0.CR1-refresh-token-max-reuse','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2021-09-08 07:15:37',49,'EXECUTED','7:edad604c882df12f74941dac3cc6d650','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.4.0','keycloak','META-INF/jpa-changelog-3.4.0.xml','2021-09-08 07:15:37',50,'EXECUTED','7:0f88b78b7b46480eb92690cbf5e44900','addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.4.0-KEYCLOAK-5230','hmlnarik@redhat.com','META-INF/jpa-changelog-3.4.0.xml','2021-09-08 07:15:37',51,'EXECUTED','7:d560e43982611d936457c327f872dd59','createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.4.1','psilva@redhat.com','META-INF/jpa-changelog-3.4.1.xml','2021-09-08 07:15:37',52,'EXECUTED','7:c155566c42b4d14ef07059ec3b3bbd8e','modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.4.2','keycloak','META-INF/jpa-changelog-3.4.2.xml','2021-09-08 07:15:37',53,'EXECUTED','7:b40376581f12d70f3c89ba8ddf5b7dea','update tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('3.4.2-KEYCLOAK-5172','mkanis@redhat.com','META-INF/jpa-changelog-3.4.2.xml','2021-09-08 07:15:37',54,'EXECUTED','7:a1132cc395f7b95b3646146c2e38f168','update tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.0.0-KEYCLOAK-6335','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2021-09-08 07:15:37',55,'EXECUTED','7:d8dc5d89c789105cfa7ca0e82cba60af','createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.0.0-CLEANUP-UNUSED-TABLE','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2021-09-08 07:15:37',56,'EXECUTED','7:7822e0165097182e8f653c35517656a3','dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.0.0-KEYCLOAK-6228','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2021-09-08 07:15:37',57,'EXECUTED','7:c6538c29b9c9a08f9e9ea2de5c2b6375','dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.0.0-KEYCLOAK-5579-fixed','mposolda@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2021-09-08 07:15:38',58,'EXECUTED','7:6d4893e36de22369cf73bcb051ded875','dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-4.0.0.CR1','psilva@redhat.com','META-INF/jpa-changelog-authz-4.0.0.CR1.xml','2021-09-08 07:15:38',59,'EXECUTED','7:57960fc0b0f0dd0563ea6f8b2e4a1707','createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-4.0.0.Beta3','psilva@redhat.com','META-INF/jpa-changelog-authz-4.0.0.Beta3.xml','2021-09-08 07:15:38',60,'EXECUTED','7:2b4b8bff39944c7097977cc18dbceb3b','addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-4.2.0.Final','mhajas@redhat.com','META-INF/jpa-changelog-authz-4.2.0.Final.xml','2021-09-08 07:15:38',61,'EXECUTED','7:2aa42a964c59cd5b8ca9822340ba33a8','createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-4.2.0.Final-KEYCLOAK-9944','hmlnarik@redhat.com','META-INF/jpa-changelog-authz-4.2.0.Final.xml','2021-09-08 07:15:38',62,'EXECUTED','7:9ac9e58545479929ba23f4a3087a0346','addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.2.0-KEYCLOAK-6313','wadahiro@gmail.com','META-INF/jpa-changelog-4.2.0.xml','2021-09-08 07:15:38',63,'EXECUTED','7:14d407c35bc4fe1976867756bcea0c36','addColumn tableName=REQUIRED_ACTION_PROVIDER','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.3.0-KEYCLOAK-7984','wadahiro@gmail.com','META-INF/jpa-changelog-4.3.0.xml','2021-09-08 07:15:38',64,'EXECUTED','7:241a8030c748c8548e346adee548fa93','update tableName=REQUIRED_ACTION_PROVIDER','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.6.0-KEYCLOAK-7950','psilva@redhat.com','META-INF/jpa-changelog-4.6.0.xml','2021-09-08 07:15:38',65,'EXECUTED','7:7d3182f65a34fcc61e8d23def037dc3f','update tableName=RESOURCE_SERVER_RESOURCE','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.6.0-KEYCLOAK-8377','keycloak','META-INF/jpa-changelog-4.6.0.xml','2021-09-08 07:15:38',66,'EXECUTED','7:b30039e00a0b9715d430d1b0636728fa','createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.6.0-KEYCLOAK-8555','gideonray@gmail.com','META-INF/jpa-changelog-4.6.0.xml','2021-09-08 07:15:38',67,'EXECUTED','7:3797315ca61d531780f8e6f82f258159','createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.7.0-KEYCLOAK-1267','sguilhen@redhat.com','META-INF/jpa-changelog-4.7.0.xml','2021-09-08 07:15:38',68,'EXECUTED','7:c7aa4c8d9573500c2d347c1941ff0301','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.7.0-KEYCLOAK-7275','keycloak','META-INF/jpa-changelog-4.7.0.xml','2021-09-08 07:15:38',69,'EXECUTED','7:b207faee394fc074a442ecd42185a5dd','renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('4.8.0-KEYCLOAK-8835','sguilhen@redhat.com','META-INF/jpa-changelog-4.8.0.xml','2021-09-08 07:15:38',70,'EXECUTED','7:ab9a9762faaba4ddfa35514b212c4922','addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'1085333512'),('authz-7.0.0-KEYCLOAK-10443','psilva@redhat.com','META-INF/jpa-changelog-authz-7.0.0.xml','2021-09-08 07:15:38',71,'EXECUTED','7:b9710f74515a6ccb51b72dc0d19df8c4','addColumn tableName=RESOURCE_SERVER','',NULL,'3.5.4',NULL,NULL,'1085333512'),('8.0.0-adding-credential-columns','keycloak','META-INF/jpa-changelog-8.0.0.xml','2021-09-08 07:15:38',72,'EXECUTED','7:ec9707ae4d4f0b7452fee20128083879','addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('8.0.0-updating-credential-data-not-oracle-fixed','keycloak','META-INF/jpa-changelog-8.0.0.xml','2021-09-08 07:15:38',73,'EXECUTED','7:3979a0ae07ac465e920ca696532fc736','update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('8.0.0-updating-credential-data-oracle-fixed','keycloak','META-INF/jpa-changelog-8.0.0.xml','2021-09-08 07:15:38',74,'MARK_RAN','7:5abfde4c259119d143bd2fbf49ac2bca','update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('8.0.0-credential-cleanup-fixed','keycloak','META-INF/jpa-changelog-8.0.0.xml','2021-09-08 07:15:38',75,'EXECUTED','7:b48da8c11a3d83ddd6b7d0c8c2219345','dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('8.0.0-resource-tag-support','keycloak','META-INF/jpa-changelog-8.0.0.xml','2021-09-08 07:15:38',76,'EXECUTED','7:a73379915c23bfad3e8f5c6d5c0aa4bd','addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.0-always-display-client','keycloak','META-INF/jpa-changelog-9.0.0.xml','2021-09-08 07:15:38',77,'EXECUTED','7:39e0073779aba192646291aa2332493d','addColumn tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.0-drop-constraints-for-column-increase','keycloak','META-INF/jpa-changelog-9.0.0.xml','2021-09-08 07:15:38',78,'MARK_RAN','7:81f87368f00450799b4bf42ea0b3ec34','dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.0-increase-column-size-federated-fk','keycloak','META-INF/jpa-changelog-9.0.0.xml','2021-09-08 07:15:38',79,'EXECUTED','7:20b37422abb9fb6571c618148f013a15','modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.0-recreate-constraints-after-column-increase','keycloak','META-INF/jpa-changelog-9.0.0.xml','2021-09-08 07:15:38',80,'MARK_RAN','7:1970bb6cfb5ee800736b95ad3fb3c78a','addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.1-add-index-to-client.client_id','keycloak','META-INF/jpa-changelog-9.0.1.xml','2021-09-08 07:15:38',81,'EXECUTED','7:45d9b25fc3b455d522d8dcc10a0f4c80','createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.1-KEYCLOAK-12579-drop-constraints','keycloak','META-INF/jpa-changelog-9.0.1.xml','2021-09-08 07:15:38',82,'MARK_RAN','7:890ae73712bc187a66c2813a724d037f','dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.1-KEYCLOAK-12579-add-not-null-constraint','keycloak','META-INF/jpa-changelog-9.0.1.xml','2021-09-08 07:15:39',83,'EXECUTED','7:0a211980d27fafe3ff50d19a3a29b538','addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.1-KEYCLOAK-12579-recreate-constraints','keycloak','META-INF/jpa-changelog-9.0.1.xml','2021-09-08 07:15:39',84,'MARK_RAN','7:a161e2ae671a9020fff61e996a207377','addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'1085333512'),('9.0.1-add-index-to-events','keycloak','META-INF/jpa-changelog-9.0.1.xml','2021-09-08 07:15:39',85,'EXECUTED','7:01c49302201bdf815b0a18d1f98a55dc','createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY','',NULL,'3.5.4',NULL,NULL,'1085333512'),('map-remove-ri','keycloak','META-INF/jpa-changelog-11.0.0.xml','2021-09-08 07:15:39',86,'EXECUTED','7:3dace6b144c11f53f1ad2c0361279b86','dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9','',NULL,'3.5.4',NULL,NULL,'1085333512'),('map-remove-ri','keycloak','META-INF/jpa-changelog-12.0.0.xml','2021-09-08 07:15:39',87,'EXECUTED','7:578d0b92077eaf2ab95ad0ec087aa903','dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('12.1.0-add-realm-localization-table','keycloak','META-INF/jpa-changelog-12.0.0.xml','2021-09-08 07:15:39',88,'EXECUTED','7:c95abe90d962c57a09ecaee57972835d','createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS','',NULL,'3.5.4',NULL,NULL,'1085333512'),('default-roles','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',89,'EXECUTED','7:f1313bcc2994a5c4dc1062ed6d8282d3','addColumn tableName=REALM; customChange','',NULL,'3.5.4',NULL,NULL,'1085333512'),('default-roles-cleanup','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',90,'EXECUTED','7:90d763b52eaffebefbcbde55f269508b','dropTable tableName=REALM_DEFAULT_ROLES; dropTable tableName=CLIENT_DEFAULT_ROLES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('13.0.0-KEYCLOAK-16844','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',91,'EXECUTED','7:d554f0cb92b764470dccfa5e0014a7dd','createIndex indexName=IDX_OFFLINE_USS_PRELOAD, tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'1085333512'),('map-remove-ri-13.0.0','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',92,'EXECUTED','7:73193e3ab3c35cf0f37ccea3bf783764','dropForeignKeyConstraint baseTableName=DEFAULT_CLIENT_SCOPE, constraintName=FK_R_DEF_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SCOPE_CLIENT, constraintName=FK_C_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SC...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('13.0.0-KEYCLOAK-17992-drop-constraints','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',93,'MARK_RAN','7:90a1e74f92e9cbaa0c5eab80b8a037f3','dropPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CLSCOPE_CL, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CL_CLSCOPE, tableName=CLIENT_SCOPE_CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('13.0.0-increase-column-size-federated','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',94,'EXECUTED','7:5b9248f29cd047c200083cc6d8388b16','modifyDataType columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; modifyDataType columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT','',NULL,'3.5.4',NULL,NULL,'1085333512'),('13.0.0-KEYCLOAK-17992-recreate-constraints','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',95,'MARK_RAN','7:64db59e44c374f13955489e8990d17a1','addNotNullConstraint columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; addNotNullConstraint columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT; addPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; createIndex indexName=...','',NULL,'3.5.4',NULL,NULL,'1085333512'),('json-string-accomodation-fixed','keycloak','META-INF/jpa-changelog-13.0.0.xml','2021-09-08 07:15:39',96,'EXECUTED','7:329a578cdb43262fff975f0a7f6cda60','addColumn tableName=REALM_ATTRIBUTE; update tableName=REALM_ATTRIBUTE; dropColumn columnName=VALUE, tableName=REALM_ATTRIBUTE; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=REALM_ATTRIBUTE','',NULL,'3.5.4',NULL,NULL,'1085333512'),('14.0.0-KEYCLOAK-11019','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',97,'EXECUTED','7:fae0de241ac0fd0bbc2b380b85e4f567','createIndex indexName=IDX_OFFLINE_CSS_PRELOAD, tableName=OFFLINE_CLIENT_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USER, tableName=OFFLINE_USER_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USERSESS, tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'1085333512'),('14.0.0-KEYCLOAK-18286','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',98,'MARK_RAN','7:075d54e9180f49bb0c64ca4218936e81','createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('14.0.0-KEYCLOAK-18286-revert','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',99,'MARK_RAN','7:06499836520f4f6b3d05e35a59324910','dropIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('14.0.0-KEYCLOAK-18286-supported-dbs','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',100,'EXECUTED','7:b558ad47ea0e4d3c3514225a49cc0d65','createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('14.0.0-KEYCLOAK-18286-unsupported-dbs','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',101,'MARK_RAN','7:3d2b23076e59c6f70bae703aa01be35b','createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'1085333512'),('KEYCLOAK-17267-add-index-to-user-attributes','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',102,'EXECUTED','7:1a7f28ff8d9e53aeb879d76ea3d9341a','createIndex indexName=IDX_USER_ATTRIBUTE_NAME, tableName=USER_ATTRIBUTE','',NULL,'3.5.4',NULL,NULL,'1085333512'),('KEYCLOAK-18146-add-saml-art-binding-identifier','keycloak','META-INF/jpa-changelog-14.0.0.xml','2021-09-08 07:15:39',103,'EXECUTED','7:2fd554456fed4a82c698c555c5b751b6','customChange','',NULL,'3.5.4',NULL,NULL,'1085333512'),('15.0.0-KEYCLOAK-18467','keycloak','META-INF/jpa-changelog-15.0.0.xml','2021-09-08 07:15:39',104,'EXECUTED','7:b06356d66c2790ecc2ae54ba0458397a','addColumn tableName=REALM_LOCALIZATIONS; update tableName=REALM_LOCALIZATIONS; dropColumn columnName=TEXTS, tableName=REALM_LOCALIZATIONS; renameColumn newColumnName=TEXTS, oldColumnName=TEXTS_NEW, tableName=REALM_LOCALIZATIONS; addNotNullConstrai...','',NULL,'3.5.4',NULL,NULL,'1085333512');
/*!40000 ALTER TABLE `DATABASECHANGELOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOGLOCK`
--

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOGLOCK`
--

LOCK TABLES `DATABASECHANGELOGLOCK` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOGLOCK` VALUES (1,_binary '\0',NULL,NULL),(1000,_binary '\0',NULL,NULL),(1001,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DEFAULT_CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `DEFAULT_CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DEFAULT_CLIENT_SCOPE` (
  `REALM_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`REALM_ID`,`SCOPE_ID`),
  KEY `IDX_DEFCLS_REALM` (`REALM_ID`),
  KEY `IDX_DEFCLS_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_R_DEF_CLI_SCOPE_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DEFAULT_CLIENT_SCOPE`
--

LOCK TABLES `DEFAULT_CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `DEFAULT_CLIENT_SCOPE` DISABLE KEYS */;
INSERT INTO `DEFAULT_CLIENT_SCOPE` VALUES ('local','0945f2d8-d930-45ea-a070-a8b13d9f5149',_binary ''),('local','34d869de-d2b2-4fb5-a03c-407c22c59329',_binary '\0'),('local','62f11fbd-04d4-4e3d-a0d8-f5966383904b',_binary '\0'),('local','6a5c0891-915c-4d8d-8c78-c516a9e3d0a7',_binary ''),('local','80d6ad2e-32a5-415e-9251-21934602c5d7',_binary '\0'),('local','81cd040d-2997-4011-bd1e-6726f4bd250c',_binary '\0'),('local','9747c4af-10f4-42ce-adeb-9d73451826d9',_binary ''),('local','f1206ae8-f5c9-4d63-bf04-60c145a41d86',_binary ''),('local','fa041adc-b7df-4b0d-a512-4fe37811ad49',_binary ''),('master','04af3e2e-a87a-410a-933a-30f5a4193427',_binary ''),('master','0a8833d9-b89f-46d6-8cd7-c22f628a2497',_binary '\0'),('master','2399f441-5504-49de-9066-7c690099d20e',_binary '\0'),('master','47a8bced-b96d-4a9e-920c-f3f1e90e0f74',_binary ''),('master','4b6132da-5079-4cfd-8581-57690401da19',_binary ''),('master','8520fdd0-4204-4a5d-b7db-5ed8df4c823a',_binary ''),('master','bb5d53f5-6ff3-447e-8667-3944dc28a6fb',_binary '\0'),('master','d4cc8b3e-63b2-47a8-87f1-163377fdd1ce',_binary '\0'),('master','ed8830e6-d6b8-4fc0-80f4-f4713245af26',_binary '');
/*!40000 ALTER TABLE `DEFAULT_CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENT_ENTITY`
--

DROP TABLE IF EXISTS `EVENT_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `DETAILS_JSON` text,
  `ERROR` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `SESSION_ID` varchar(255) DEFAULT NULL,
  `EVENT_TIME` bigint DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_EVENT_TIME` (`REALM_ID`,`EVENT_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENT_ENTITY`
--

LOCK TABLES `EVENT_ENTITY` WRITE;
/*!40000 ALTER TABLE `EVENT_ENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FEDERATED_IDENTITY`
--

DROP TABLE IF EXISTS `FEDERATED_IDENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FEDERATED_IDENTITY` (
  `IDENTITY_PROVIDER` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `FEDERATED_USER_ID` varchar(255) DEFAULT NULL,
  `FEDERATED_USERNAME` varchar(255) DEFAULT NULL,
  `TOKEN` text,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`),
  KEY `IDX_FEDIDENTITY_USER` (`USER_ID`),
  KEY `IDX_FEDIDENTITY_FEDUSER` (`FEDERATED_USER_ID`),
  CONSTRAINT `FK404288B92EF007A6` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FEDERATED_IDENTITY`
--

LOCK TABLES `FEDERATED_IDENTITY` WRITE;
/*!40000 ALTER TABLE `FEDERATED_IDENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `FEDERATED_IDENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FEDERATED_USER`
--

DROP TABLE IF EXISTS `FEDERATED_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FEDERATED_USER` (
  `ID` varchar(255) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FEDERATED_USER`
--

LOCK TABLES `FEDERATED_USER` WRITE;
/*!40000 ALTER TABLE `FEDERATED_USER` DISABLE KEYS */;
/*!40000 ALTER TABLE `FEDERATED_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_ATTRIBUTE`
--

DROP TABLE IF EXISTS `FED_USER_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `VALUE` text,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_ATTRIBUTE` (`USER_ID`,`REALM_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_ATTRIBUTE`
--

LOCK TABLES `FED_USER_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `FED_USER_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CONSENT`
--

DROP TABLE IF EXISTS `FED_USER_CONSENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CONSENT` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CONSENT` (`USER_ID`,`CLIENT_ID`),
  KEY `IDX_FU_CONSENT_RU` (`REALM_ID`,`USER_ID`),
  KEY `IDX_FU_CNSNT_EXT` (`USER_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CONSENT`
--

LOCK TABLES `FED_USER_CONSENT` WRITE;
/*!40000 ALTER TABLE `FED_USER_CONSENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CONSENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CONSENT_CL_SCOPE`
--

DROP TABLE IF EXISTS `FED_USER_CONSENT_CL_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CONSENT_CL_SCOPE` (
  `USER_CONSENT_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CONSENT_CL_SCOPE`
--

LOCK TABLES `FED_USER_CONSENT_CL_SCOPE` WRITE;
/*!40000 ALTER TABLE `FED_USER_CONSENT_CL_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CONSENT_CL_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CREDENTIAL`
--

DROP TABLE IF EXISTS `FED_USER_CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CREDENTIAL` (
  `ID` varchar(36) NOT NULL,
  `SALT` tinyblob,
  `TYPE` varchar(255) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `USER_LABEL` varchar(255) DEFAULT NULL,
  `SECRET_DATA` longtext,
  `CREDENTIAL_DATA` longtext,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CREDENTIAL` (`USER_ID`,`TYPE`),
  KEY `IDX_FU_CREDENTIAL_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CREDENTIAL`
--

LOCK TABLES `FED_USER_CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `FED_USER_CREDENTIAL` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_GROUP_MEMBERSHIP`
--

DROP TABLE IF EXISTS `FED_USER_GROUP_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_GROUP_MEMBERSHIP` (
  `GROUP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP` (`USER_ID`,`GROUP_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_GROUP_MEMBERSHIP`
--

LOCK TABLES `FED_USER_GROUP_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `FED_USER_GROUP_MEMBERSHIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_GROUP_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_REQUIRED_ACTION`
--

DROP TABLE IF EXISTS `FED_USER_REQUIRED_ACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_REQUIRED_ACTION` (
  `REQUIRED_ACTION` varchar(255) NOT NULL DEFAULT ' ',
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_FU_REQUIRED_ACTION` (`USER_ID`,`REQUIRED_ACTION`),
  KEY `IDX_FU_REQUIRED_ACTION_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_REQUIRED_ACTION`
--

LOCK TABLES `FED_USER_REQUIRED_ACTION` WRITE;
/*!40000 ALTER TABLE `FED_USER_REQUIRED_ACTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_REQUIRED_ACTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `FED_USER_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_ROLE_MAPPING` (
  `ROLE_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_FU_ROLE_MAPPING` (`USER_ID`,`ROLE_ID`),
  KEY `IDX_FU_ROLE_MAPPING_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_ROLE_MAPPING`
--

LOCK TABLES `FED_USER_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `FED_USER_ROLE_MAPPING` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GROUP_ATTRIBUTE`
--

DROP TABLE IF EXISTS `GROUP_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GROUP_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_GROUP_ATTR_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_GROUP_ATTRIBUTE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `KEYCLOAK_GROUP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GROUP_ATTRIBUTE`
--

LOCK TABLES `GROUP_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `GROUP_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUP_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GROUP_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `GROUP_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GROUP_ROLE_MAPPING` (
  `ROLE_ID` varchar(36) NOT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`GROUP_ID`),
  KEY `IDX_GROUP_ROLE_MAPP_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_GROUP_ROLE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `KEYCLOAK_GROUP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GROUP_ROLE_MAPPING`
--

LOCK TABLES `GROUP_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `GROUP_ROLE_MAPPING` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUP_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER` (
  `INTERNAL_ID` varchar(36) NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ALIAS` varchar(255) DEFAULT NULL,
  `PROVIDER_ID` varchar(255) DEFAULT NULL,
  `STORE_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `AUTHENTICATE_BY_DEFAULT` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) DEFAULT NULL,
  `ADD_TOKEN_ROLE` bit(1) NOT NULL DEFAULT b'1',
  `TRUST_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `FIRST_BROKER_LOGIN_FLOW_ID` varchar(36) DEFAULT NULL,
  `POST_BROKER_LOGIN_FLOW_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `LINK_ONLY` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`INTERNAL_ID`),
  UNIQUE KEY `UK_2DAELWNIBJI49AVXSRTUF6XJ33` (`PROVIDER_ALIAS`,`REALM_ID`),
  KEY `IDX_IDENT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK2B4EBC52AE5C3B34` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER`
--

LOCK TABLES `IDENTITY_PROVIDER` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER_CONFIG`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER_CONFIG` (
  `IDENTITY_PROVIDER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FKDC4897CF864C4E43` FOREIGN KEY (`IDENTITY_PROVIDER_ID`) REFERENCES `IDENTITY_PROVIDER` (`INTERNAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER_CONFIG`
--

LOCK TABLES `IDENTITY_PROVIDER_CONFIG` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER_MAPPER`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `IDP_ALIAS` varchar(255) NOT NULL,
  `IDP_MAPPER_NAME` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ID_PROV_MAPP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_IDPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER_MAPPER`
--

LOCK TABLES `IDENTITY_PROVIDER_MAPPER` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDP_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `IDP_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDP_MAPPER_CONFIG` (
  `IDP_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`IDP_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_IDPMCONFIG` FOREIGN KEY (`IDP_MAPPER_ID`) REFERENCES `IDENTITY_PROVIDER_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDP_MAPPER_CONFIG`
--

LOCK TABLES `IDP_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `IDP_MAPPER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDP_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KEYCLOAK_GROUP`
--

DROP TABLE IF EXISTS `KEYCLOAK_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `KEYCLOAK_GROUP` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `PARENT_GROUP` varchar(36) NOT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SIBLING_NAMES` (`REALM_ID`,`PARENT_GROUP`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KEYCLOAK_GROUP`
--

LOCK TABLES `KEYCLOAK_GROUP` WRITE;
/*!40000 ALTER TABLE `KEYCLOAK_GROUP` DISABLE KEYS */;
INSERT INTO `KEYCLOAK_GROUP` VALUES ('2a1cdba5-7534-4f3b-a2de-e1b8003d051f','my-app',' ','local');
/*!40000 ALTER TABLE `KEYCLOAK_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KEYCLOAK_ROLE`
--

DROP TABLE IF EXISTS `KEYCLOAK_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `KEYCLOAK_ROLE` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_REALM_CONSTRAINT` varchar(255) DEFAULT NULL,
  `CLIENT_ROLE` bit(1) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `CLIENT` varchar(36) DEFAULT NULL,
  `REALM` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_J3RWUVD56ONTGSUHOGM184WW2-2` (`NAME`,`CLIENT_REALM_CONSTRAINT`),
  KEY `IDX_KEYCLOAK_ROLE_CLIENT` (`CLIENT`),
  KEY `IDX_KEYCLOAK_ROLE_REALM` (`REALM`),
  CONSTRAINT `FK_6VYQFE4CN4WLQ8R6KT5VDSJ5C` FOREIGN KEY (`REALM`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KEYCLOAK_ROLE`
--

LOCK TABLES `KEYCLOAK_ROLE` WRITE;
/*!40000 ALTER TABLE `KEYCLOAK_ROLE` DISABLE KEYS */;
INSERT INTO `KEYCLOAK_ROLE` VALUES ('01216dcc-c4c7-49bd-b348-763d91844b49','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-users}','manage-users','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('01510ddd-eac3-4d27-87f5-b2073abceda0','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-clients}','view-clients','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('03d6afd7-f2cf-42c4-a526-e1b6d17745ef','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-authorization}','view-authorization','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('06ad8eee-6bde-42e3-9362-212bb3874b25','local',_binary '\0','${role_uma_authorization}','uma_authorization','local',NULL,NULL),('0e9f14a8-ee0c-4a2a-bdf1-14321379e5f6','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_view-applications}','view-applications','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('10bf3aca-99d5-4961-99fd-9440b12e89cf','local',_binary '\0','${role_default-roles}','default-roles-local','local',NULL,NULL),('1140d4dd-800c-404e-8edd-f462a4683723','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-realm}','manage-realm','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('17beb223-28d4-41e3-967b-8f3c3a67122b','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_view-profile}','view-profile','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('1b7c21a7-4cb1-4130-b8cb-33bcc1712fc4','master',_binary '\0','${role_create-realm}','create-realm','master',NULL,NULL),('1e719afc-3279-4103-9f66-a60a3f8d05c1','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_view-consent}','view-consent','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('1f09ec7b-c8d9-49c4-a74f-6a85cb7756c3','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-identity-providers}','manage-identity-providers','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('20c76a23-b87c-4a58-a881-ee204de3b846','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_manage-consent}','manage-consent','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('213fdfba-4f44-49bc-9eeb-e67532302ea0','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_query-groups}','query-groups','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('216ffa95-308f-490e-91da-6a5be36b664f','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-realm}','manage-realm','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('2ac60d98-7d47-40b2-80f9-14082f3fb035','master',_binary '\0','${role_offline-access}','offline_access','master',NULL,NULL),('2d756545-820d-4cbc-b379-799edda1f215','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_manage-consent}','manage-consent','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('2e08c627-99ce-48ba-8534-18c0e449097b','local',_binary '\0',NULL,'MERCHANT','local',NULL,NULL),('2e7a0c98-bdf1-4fef-bb51-dabb572997c6','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-authorization}','manage-authorization','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('3945086a-c229-40d5-9ba0-4a0d93e25154','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_query-users}','query-users','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('3ab03f72-bd92-4e9d-9fb4-bde7ba19da2c','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-events}','view-events','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('41555952-ccf7-4d44-aece-a4d6da96e82b','47757fda-9024-445a-9d4b-00fc169bb329',_binary '','${role_read-token}','read-token','local','47757fda-9024-445a-9d4b-00fc169bb329',NULL),('416dab4b-6a89-4b83-b93c-54c9cb06e8c2','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-users}','view-users','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('42d5cde3-10be-4922-900e-76eaa8dd1800','master',_binary '\0','${role_default-roles}','default-roles-master','master',NULL,NULL),('477bc87f-70a3-4fdc-92de-d117dab5712c','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-realm}','view-realm','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('48895f8a-ac48-40c5-9ff3-f37f36cbda19','1da632b4-3088-4044-a3db-99b423a1872c',_binary '',NULL,'CUSTOMER','local','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('49f1cf0d-0992-4766-9870-f28b0b16adef','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_query-clients}','query-clients','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('57fa9b29-114e-4925-9f9d-41742331f262','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_impersonation}','impersonation','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('5c0fc91d-b5ae-4573-a317-8019cc5d75be','047d93f5-955c-41c1-9513-75a8745618c3',_binary '',NULL,'CUSTOMER','local','047d93f5-955c-41c1-9513-75a8745618c3',NULL),('5d831418-88a4-43d7-86f0-2ce0dbed5249','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-authorization}','view-authorization','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('5f030686-62a7-4e8d-a5f1-3281f312cde3','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-events}','view-events','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','master',_binary '\0','${role_admin}','admin','master',NULL,NULL),('6428972c-ddd7-477e-979f-96aa6833230d','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-clients}','manage-clients','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('6c3c3fa4-56be-4786-b353-6a99fd096be4','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_delete-account}','delete-account','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('721b0d15-66f4-464d-b76e-780a6628e09e','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_delete-account}','delete-account','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('76f2d4d9-aeea-495c-97a0-ea9f7bedc7dc','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-authorization}','manage-authorization','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('770a9e69-89ce-4165-b334-91a9610922c3','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-users}','view-users','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('7b2f2afa-efa7-4b08-8b0e-dadb113b8c53','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-events}','manage-events','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('7c9d7d82-c3b9-4ec5-990b-6e71b04327f7','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_impersonation}','impersonation','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('7f2fadf0-cf66-44c3-b59b-0b3511828c96','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_create-client}','create-client','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('8021fc26-a418-473a-bebb-60299cfaf572','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_realm-admin}','realm-admin','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('83a7bf70-ed70-4420-81bf-a720bffda7e8','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_view-applications}','view-applications','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('88903b73-c532-4c1f-8095-1b998513fc94','1da632b4-3088-4044-a3db-99b423a1872c',_binary '',NULL,'MERCHANT','local','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('89260c4a-d4bb-42ec-8178-e2eccc260ba2','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-realm}','manage-realm','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('8937eff8-584b-432f-9e58-d17dad9bb7f2','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_query-clients}','query-clients','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('89cf2724-6d1f-4dd5-97b0-c6c78d397591','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_query-realms}','query-realms','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('8ab23308-383e-4739-928d-772d57c59b6d','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_query-users}','query-users','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('8f496ecc-8941-40b1-8d13-7558c1ba6e46','6cc49300-8dc8-488f-9658-0aca8035e02a',_binary '','${role_read-token}','read-token','master','6cc49300-8dc8-488f-9658-0aca8035e02a',NULL),('904dbb57-b146-4106-876a-fad4b87acc8b','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-events}','view-events','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('949b0c8a-df4e-45df-93cd-fd05751d6988','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-identity-providers}','view-identity-providers','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('957ab799-e4e9-4f38-8c2c-d885edd30412','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-events}','manage-events','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('978b6aab-f691-4b98-840e-5cd0a32d18e9','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_create-client}','create-client','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('98dbb4e4-ffbb-4007-9721-25ddcf10c7ac','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-events}','manage-events','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('9f368f44-a5cc-4962-9ce1-bc3dcbda77bf','master',_binary '\0','${role_uma_authorization}','uma_authorization','master',NULL,NULL),('a642f38f-1935-46eb-9268-b3d97b797273','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-realm}','view-realm','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('a6ccea86-ccc9-41ef-bd37-756d6329a11e','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-clients}','view-clients','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('a8e41e99-dc5f-4780-90c1-50788f769c4c','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-identity-providers}','manage-identity-providers','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('ab53923b-5e27-4607-8725-918a8ce16fc7','1da632b4-3088-4044-a3db-99b423a1872c',_binary '',NULL,'uma_protection','local','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('b37ff5b8-190c-465b-bc11-9ec13b5d0646','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_query-realms}','query-realms','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('b46b4f62-0d69-42a7-9481-2eda7746d8da','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-authorization}','view-authorization','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('b91a8a31-b3c2-40b4-879a-08f81746dc61','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_view-consent}','view-consent','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('b92960e3-b610-4524-bb65-5419773e429a','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_manage-account}','manage-account','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('bb2aca38-a8ed-4f44-b79e-2bccc5331f66','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',_binary '',NULL,'CUSTOMER','local','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',NULL),('bda5a968-b1f7-42cc-8d3e-7074934ca4e4','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_create-client}','create-client','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('be415c40-fac3-4aec-a97c-36af2749a464','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-clients}','manage-clients','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('bed1266f-0b1a-4342-9cd0-686c5ce71981','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_query-groups}','query-groups','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('c1556287-405b-4b8a-9836-17a8b1e7aca0','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_view-realm}','view-realm','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('c5e2e223-f202-4bbb-9aef-caa1a31d8e06','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-users}','manage-users','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('d06bbb68-7c74-44f4-a2de-3051a4491fc2','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_query-realms}','query-realms','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('d1d2a346-5419-49f9-8710-371898d9e865','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-users}','view-users','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('d544863a-4afe-419a-8f68-49afb552f336','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-identity-providers}','view-identity-providers','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('d5470f9f-164e-4d73-bf73-449839b4af30','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_view-clients}','view-clients','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('d5ea15a5-bab2-4127-9c95-c2f3e999d005','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-authorization}','manage-authorization','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('d6600a99-fe67-456f-8ec3-bca6a9f8f2a2','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_query-clients}','query-clients','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('db12d2c2-8969-486a-b410-dca0ae52ba07','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_query-users}','query-users','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('dc9ffc08-fef9-419b-a2d8-f9c4f90c1535','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_manage-identity-providers}','manage-identity-providers','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('e1219c8e-40b5-49a2-bf9e-e49849950d91','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_impersonation}','impersonation','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('e4ca969d-7388-4d19-9eea-821ee8947e0f','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',_binary '',NULL,'MERCHANT','local','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',NULL),('ebc04247-2c5f-4695-ab6d-187b27e348a2','86204c34-f02a-4a5b-968b-a1563ad2be76',_binary '','${role_manage-account-links}','manage-account-links','local','86204c34-f02a-4a5b-968b-a1563ad2be76',NULL),('ebd60f62-1d8f-4a10-aab8-5958063a496c','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',_binary '','${role_manage-clients}','manage-clients','master','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',NULL),('f01e2c42-90df-4a42-bd08-4eb0d6d73211','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_view-profile}','view-profile','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('f316daf5-45ac-4008-9bfb-e3696556c735','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_manage-users}','manage-users','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('f53f5178-4c49-4103-b299-18831080e719','local',_binary '\0','${role_offline-access}','offline_access','local',NULL,NULL),('f56b3191-40f3-45e6-9e7d-065dd48af8bb','local',_binary '\0',NULL,'CUSTOMER','local',NULL,NULL),('f7f2a3e5-7153-4f78-8666-a81ad59a4250','8f806415-f35d-48c2-bf8a-0f00003bbc72',_binary '','${role_query-groups}','query-groups','master','8f806415-f35d-48c2-bf8a-0f00003bbc72',NULL),('f9757cc8-7e2d-44e3-a27b-736674328854','5d8641b5-9790-423d-a629-0fce0ad67389',_binary '','${role_view-identity-providers}','view-identity-providers','local','5d8641b5-9790-423d-a629-0fce0ad67389',NULL),('fb0ee30e-91d7-41f1-b3d4-f67e07af2bc6','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_manage-account}','manage-account','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL),('ff2c1a1b-d7f5-4b02-b211-7a6dce6395f0','5f9bc463-02df-4f95-b60e-bec7b41b9b68',_binary '','${role_manage-account-links}','manage-account-links','master','5f9bc463-02df-4f95-b60e-bec7b41b9b68',NULL);
/*!40000 ALTER TABLE `KEYCLOAK_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MIGRATION_MODEL`
--

DROP TABLE IF EXISTS `MIGRATION_MODEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MIGRATION_MODEL` (
  `ID` varchar(36) NOT NULL,
  `VERSION` varchar(36) DEFAULT NULL,
  `UPDATE_TIME` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IDX_UPDATE_TIME` (`UPDATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MIGRATION_MODEL`
--

LOCK TABLES `MIGRATION_MODEL` WRITE;
/*!40000 ALTER TABLE `MIGRATION_MODEL` DISABLE KEYS */;
INSERT INTO `MIGRATION_MODEL` VALUES ('qh5oe','15.0.2',1631085340);
/*!40000 ALTER TABLE `MIGRATION_MODEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OFFLINE_CLIENT_SESSION`
--

DROP TABLE IF EXISTS `OFFLINE_CLIENT_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OFFLINE_CLIENT_SESSION` (
  `USER_SESSION_ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) NOT NULL,
  `OFFLINE_FLAG` varchar(4) NOT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `DATA` longtext,
  `CLIENT_STORAGE_PROVIDER` varchar(36) NOT NULL DEFAULT 'local',
  `EXTERNAL_CLIENT_ID` varchar(255) NOT NULL DEFAULT 'local',
  PRIMARY KEY (`USER_SESSION_ID`,`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`OFFLINE_FLAG`),
  KEY `IDX_US_SESS_ID_ON_CL_SESS` (`USER_SESSION_ID`),
  KEY `IDX_OFFLINE_CSS_PRELOAD` (`CLIENT_ID`,`OFFLINE_FLAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OFFLINE_CLIENT_SESSION`
--

LOCK TABLES `OFFLINE_CLIENT_SESSION` WRITE;
/*!40000 ALTER TABLE `OFFLINE_CLIENT_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `OFFLINE_CLIENT_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OFFLINE_USER_SESSION`
--

DROP TABLE IF EXISTS `OFFLINE_USER_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OFFLINE_USER_SESSION` (
  `USER_SESSION_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `CREATED_ON` int NOT NULL,
  `OFFLINE_FLAG` varchar(4) NOT NULL,
  `DATA` longtext,
  `LAST_SESSION_REFRESH` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`USER_SESSION_ID`,`OFFLINE_FLAG`),
  KEY `IDX_OFFLINE_USS_CREATEDON` (`CREATED_ON`),
  KEY `IDX_OFFLINE_USS_PRELOAD` (`OFFLINE_FLAG`,`CREATED_ON`,`USER_SESSION_ID`),
  KEY `IDX_OFFLINE_USS_BY_USER` (`USER_ID`,`REALM_ID`,`OFFLINE_FLAG`),
  KEY `IDX_OFFLINE_USS_BY_USERSESS` (`REALM_ID`,`OFFLINE_FLAG`,`USER_SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OFFLINE_USER_SESSION`
--

LOCK TABLES `OFFLINE_USER_SESSION` WRITE;
/*!40000 ALTER TABLE `OFFLINE_USER_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `OFFLINE_USER_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `POLICY_CONFIG`
--

DROP TABLE IF EXISTS `POLICY_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `POLICY_CONFIG` (
  `POLICY_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` longtext,
  PRIMARY KEY (`POLICY_ID`,`NAME`),
  CONSTRAINT `FKDC34197CF864C4E43` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `POLICY_CONFIG`
--

LOCK TABLES `POLICY_CONFIG` WRITE;
/*!40000 ALTER TABLE `POLICY_CONFIG` DISABLE KEYS */;
INSERT INTO `POLICY_CONFIG` VALUES ('3c745221-bf17-47e7-9011-bd9150bd7b7e','clients','[\"047d93f5-955c-41c1-9513-75a8745618c3\"]'),('817ac65c-c0b0-4d46-8091-d6a89f8371cb','defaultResourceType','urn:kong:resources:default'),('f57573f1-9f27-4f2d-9399-aa353efd077d','code','// by default, grants any permission associated with this policy\n$evaluation.grant();\n');
/*!40000 ALTER TABLE `POLICY_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROTOCOL_MAPPER`
--

DROP TABLE IF EXISTS `PROTOCOL_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PROTOCOL_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `PROTOCOL` varchar(255) NOT NULL,
  `PROTOCOL_MAPPER_NAME` varchar(255) NOT NULL,
  `CLIENT_ID` varchar(36) DEFAULT NULL,
  `CLIENT_SCOPE_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_PROTOCOL_MAPPER_CLIENT` (`CLIENT_ID`),
  KEY `IDX_CLSCOPE_PROTMAP` (`CLIENT_SCOPE_ID`),
  CONSTRAINT `FK_CLI_SCOPE_MAPPER` FOREIGN KEY (`CLIENT_SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`),
  CONSTRAINT `FK_PCM_REALM` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROTOCOL_MAPPER`
--

LOCK TABLES `PROTOCOL_MAPPER` WRITE;
/*!40000 ALTER TABLE `PROTOCOL_MAPPER` DISABLE KEYS */;
INSERT INTO `PROTOCOL_MAPPER` VALUES ('00d6b4fd-a35b-4668-a157-fbcfc84f149d','updated at','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','upn','openid-connect','oidc-usermodel-property-mapper',NULL,'2399f441-5504-49de-9066-7c690099d20e'),('04f86382-a64b-4d09-9096-7600ffdc656b','audience','openid-connect','oidc-audience-mapper','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',NULL),('10c0b289-03a0-4c35-8b96-0bdace6791ca','realm roles','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'0945f2d8-d930-45ea-a070-a8b13d9f5149'),('17fb60b7-f9d9-407a-93f4-16af0f905689','picture','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','locale','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('1dbebaf8-eb8e-4187-ba42-bb313a68a146','audience','openid-connect','oidc-audience-mapper','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','locale','openid-connect','oidc-usermodel-attribute-mapper','55e8799a-e472-4690-a56f-f07d3920595d',NULL),('2094bd2c-d913-4bc5-af11-da2a49c89d24','roles','openid-connect','oidc-usermodel-realm-role-mapper','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('20aaae68-f2b9-4502-ab01-64aacb177d9f','client roles','openid-connect','oidc-usermodel-client-role-mapper',NULL,'0945f2d8-d930-45ea-a070-a8b13d9f5149'),('20e8dd3e-f055-4436-9809-28c0e843733f','address','openid-connect','oidc-address-mapper',NULL,'bb5d53f5-6ff3-447e-8667-3944dc28a6fb'),('211ee527-0f7b-472b-9055-c63f12e46465','locale','openid-connect','oidc-usermodel-attribute-mapper','84b80f41-cefb-460d-9d0a-89ffacb20fc8',NULL),('26436ea4-6541-401f-960e-a641d9f7de79','email','openid-connect','oidc-usermodel-property-mapper',NULL,'9747c4af-10f4-42ce-adeb-9d73451826d9'),('27850dff-ccde-46b7-b3a8-80f52a99e203','nickname','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('293e59cd-2f10-4ebc-83f8-051677a9a225','zoneinfo','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('311396a8-42af-4f32-b72f-25f4cca65c6e','gender','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','family name','openid-connect','oidc-usermodel-property-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','profile','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('3c358125-754f-42e5-8775-6e82084bdcba','role list','saml','saml-role-list-mapper',NULL,'8520fdd0-4204-4a5d-b7db-5ed8df4c823a'),('41b8eccd-1c74-49bc-9cea-82eb426b4135','allowed web origins','openid-connect','oidc-allowed-origins-mapper',NULL,'fa041adc-b7df-4b0d-a512-4fe37811ad49'),('49a1a266-9213-415a-8f2e-0f34dca27c04','email verified','openid-connect','oidc-usermodel-property-mapper',NULL,'9747c4af-10f4-42ce-adeb-9d73451826d9'),('49a74b88-49ae-403c-b32a-32fad37b4805','groups','openid-connect','oidc-group-membership-mapper','c3e97a1f-c628-452c-bfaa-b0b1bd226c91',NULL),('5be929cf-2843-48b9-b134-d5c8e35dc479','email verified','openid-connect','oidc-usermodel-property-mapper',NULL,'47a8bced-b96d-4a9e-920c-f3f1e90e0f74'),('610cf186-9bc0-4f49-a4d1-cb908522ff38','audience resolve','openid-connect','oidc-audience-resolve-mapper','e9d7b805-7d49-4676-aeab-5d7ce926f7c0',NULL),('61128257-79a1-46db-bbed-412d3b79c54c','phone number','openid-connect','oidc-usermodel-attribute-mapper',NULL,'62f11fbd-04d4-4e3d-a0d8-f5966383904b'),('693df150-da09-4ecb-bef0-74f2413c159e','full name','openid-connect','oidc-full-name-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('6a901660-4d46-4b35-ba18-03d056f19261','website','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('71f69391-8b23-4488-8e1c-12b75f5c2183','middle name','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('75e8e3e1-3b8c-445d-9f2c-98dd58c1d784','audience resolve','openid-connect','oidc-audience-resolve-mapper',NULL,'4b6132da-5079-4cfd-8581-57690401da19'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','groups','openid-connect','oidc-group-membership-mapper','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','profile','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('7aaf5dbf-5dc6-443c-a262-fcebe8e2b57e','allowed web origins','openid-connect','oidc-allowed-origins-mapper',NULL,'ed8830e6-d6b8-4fc0-80f4-f4713245af26'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','picture','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','given name','openid-connect','oidc-usermodel-property-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','client roles','openid-connect','oidc-usermodel-client-role-mapper',NULL,'4b6132da-5079-4cfd-8581-57690401da19'),('90c53b00-fb49-47ba-8276-dd2740be59d3','email','openid-connect','oidc-usermodel-property-mapper',NULL,'47a8bced-b96d-4a9e-920c-f3f1e90e0f74'),('93757435-7734-41e1-8e12-72b8afb28ff2','audience resolve','openid-connect','oidc-audience-resolve-mapper','2907bf1e-c935-4b8a-a9aa-b3756413b4fc',NULL),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','realm roles','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'4b6132da-5079-4cfd-8581-57690401da19'),('94059bcc-4c61-41a6-8220-434427cb542c','audience resolve','openid-connect','oidc-audience-resolve-mapper',NULL,'0945f2d8-d930-45ea-a070-a8b13d9f5149'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','birthdate','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','username','openid-connect','oidc-usermodel-property-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','birthdate','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','groups','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'2399f441-5504-49de-9066-7c690099d20e'),('a7aae793-03e0-412f-9b82-bcefbde48a19','middle name','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('aa49005b-c653-4984-b875-0e9abbdaa98a','phone number verified','openid-connect','oidc-usermodel-attribute-mapper',NULL,'62f11fbd-04d4-4e3d-a0d8-f5966383904b'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','zoneinfo','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('ac3c55fe-87df-472f-9809-d01678db9c3f','locale','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('b1b82dc3-58e6-49d7-9154-9e6306bacf61','full name','openid-connect','oidc-full-name-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','nickname','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','username','openid-connect','oidc-usermodel-property-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','updated at','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','website','openid-connect','oidc-usermodel-attribute-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','given name','openid-connect','oidc-usermodel-property-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('c691244d-bfd5-464b-beca-9af15e4963a3','upn','openid-connect','oidc-usermodel-property-mapper',NULL,'80d6ad2e-32a5-415e-9251-21934602c5d7'),('c86bf90f-6396-47b4-95d9-10f81b09543f','gender','openid-connect','oidc-usermodel-attribute-mapper',NULL,'6a5c0891-915c-4d8d-8c78-c516a9e3d0a7'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','phone number verified','openid-connect','oidc-usermodel-attribute-mapper',NULL,'d4cc8b3e-63b2-47a8-87f1-163377fdd1ce'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','family name','openid-connect','oidc-usermodel-property-mapper',NULL,'04af3e2e-a87a-410a-933a-30f5a4193427'),('da7373e4-538f-42ed-b62a-8d254f93c437','groups','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'80d6ad2e-32a5-415e-9251-21934602c5d7'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','phone number','openid-connect','oidc-usermodel-attribute-mapper',NULL,'d4cc8b3e-63b2-47a8-87f1-163377fdd1ce'),('ea82259e-5f19-4c1d-af25-a56d0245d93a','role list','saml','saml-role-list-mapper',NULL,'f1206ae8-f5c9-4d63-bf04-60c145a41d86'),('f27c9339-ea08-41ab-aa53-114c576b37e3','address','openid-connect','oidc-address-mapper',NULL,'34d869de-d2b2-4fb5-a03c-407c22c59329');
/*!40000 ALTER TABLE `PROTOCOL_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROTOCOL_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `PROTOCOL_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PROTOCOL_MAPPER_CONFIG` (
  `PROTOCOL_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`PROTOCOL_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_PMCONFIG` FOREIGN KEY (`PROTOCOL_MAPPER_ID`) REFERENCES `PROTOCOL_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROTOCOL_MAPPER_CONFIG`
--

LOCK TABLES `PROTOCOL_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `PROTOCOL_MAPPER_CONFIG` DISABLE KEYS */;
INSERT INTO `PROTOCOL_MAPPER_CONFIG` VALUES ('00d6b4fd-a35b-4668-a157-fbcfc84f149d','true','access.token.claim'),('00d6b4fd-a35b-4668-a157-fbcfc84f149d','updated_at','claim.name'),('00d6b4fd-a35b-4668-a157-fbcfc84f149d','true','id.token.claim'),('00d6b4fd-a35b-4668-a157-fbcfc84f149d','String','jsonType.label'),('00d6b4fd-a35b-4668-a157-fbcfc84f149d','updatedAt','user.attribute'),('00d6b4fd-a35b-4668-a157-fbcfc84f149d','true','userinfo.token.claim'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','true','access.token.claim'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','upn','claim.name'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','true','id.token.claim'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','String','jsonType.label'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','username','user.attribute'),('045bd08a-26cf-4bf9-b65e-38c3419584cb','true','userinfo.token.claim'),('04f86382-a64b-4d09-9096-7600ffdc656b','true','access.token.claim'),('04f86382-a64b-4d09-9096-7600ffdc656b','false','id.token.claim'),('04f86382-a64b-4d09-9096-7600ffdc656b','gatekeeper','included.client.audience'),('04f86382-a64b-4d09-9096-7600ffdc656b','false','userinfo.token.claim'),('10c0b289-03a0-4c35-8b96-0bdace6791ca','true','access.token.claim'),('10c0b289-03a0-4c35-8b96-0bdace6791ca','realm_access.roles','claim.name'),('10c0b289-03a0-4c35-8b96-0bdace6791ca','String','jsonType.label'),('10c0b289-03a0-4c35-8b96-0bdace6791ca','true','multivalued'),('10c0b289-03a0-4c35-8b96-0bdace6791ca','foo','user.attribute'),('17fb60b7-f9d9-407a-93f4-16af0f905689','true','access.token.claim'),('17fb60b7-f9d9-407a-93f4-16af0f905689','picture','claim.name'),('17fb60b7-f9d9-407a-93f4-16af0f905689','true','id.token.claim'),('17fb60b7-f9d9-407a-93f4-16af0f905689','String','jsonType.label'),('17fb60b7-f9d9-407a-93f4-16af0f905689','picture','user.attribute'),('17fb60b7-f9d9-407a-93f4-16af0f905689','true','userinfo.token.claim'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','true','access.token.claim'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','locale','claim.name'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','true','id.token.claim'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','String','jsonType.label'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','locale','user.attribute'),('1bcefaf2-99c2-4f97-90e9-fbabf151a26e','true','userinfo.token.claim'),('1dbebaf8-eb8e-4187-ba42-bb313a68a146','true','access.token.claim'),('1dbebaf8-eb8e-4187-ba42-bb313a68a146','false','id.token.claim'),('1dbebaf8-eb8e-4187-ba42-bb313a68a146','kong','included.client.audience'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','true','access.token.claim'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','locale','claim.name'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','true','id.token.claim'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','String','jsonType.label'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','locale','user.attribute'),('2065ae3a-cdb2-46b6-ab9c-da23d70e7d91','true','userinfo.token.claim'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','true','access.token.claim'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','roles','claim.name'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','true','id.token.claim'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','String','jsonType.label'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','true','multivalued'),('2094bd2c-d913-4bc5-af11-da2a49c89d24','true','userinfo.token.claim'),('20aaae68-f2b9-4502-ab01-64aacb177d9f','true','access.token.claim'),('20aaae68-f2b9-4502-ab01-64aacb177d9f','resource_access.${client_id}.roles','claim.name'),('20aaae68-f2b9-4502-ab01-64aacb177d9f','String','jsonType.label'),('20aaae68-f2b9-4502-ab01-64aacb177d9f','true','multivalued'),('20aaae68-f2b9-4502-ab01-64aacb177d9f','foo','user.attribute'),('20e8dd3e-f055-4436-9809-28c0e843733f','true','access.token.claim'),('20e8dd3e-f055-4436-9809-28c0e843733f','true','id.token.claim'),('20e8dd3e-f055-4436-9809-28c0e843733f','country','user.attribute.country'),('20e8dd3e-f055-4436-9809-28c0e843733f','formatted','user.attribute.formatted'),('20e8dd3e-f055-4436-9809-28c0e843733f','locality','user.attribute.locality'),('20e8dd3e-f055-4436-9809-28c0e843733f','postal_code','user.attribute.postal_code'),('20e8dd3e-f055-4436-9809-28c0e843733f','region','user.attribute.region'),('20e8dd3e-f055-4436-9809-28c0e843733f','street','user.attribute.street'),('20e8dd3e-f055-4436-9809-28c0e843733f','true','userinfo.token.claim'),('211ee527-0f7b-472b-9055-c63f12e46465','true','access.token.claim'),('211ee527-0f7b-472b-9055-c63f12e46465','locale','claim.name'),('211ee527-0f7b-472b-9055-c63f12e46465','true','id.token.claim'),('211ee527-0f7b-472b-9055-c63f12e46465','String','jsonType.label'),('211ee527-0f7b-472b-9055-c63f12e46465','locale','user.attribute'),('211ee527-0f7b-472b-9055-c63f12e46465','true','userinfo.token.claim'),('26436ea4-6541-401f-960e-a641d9f7de79','true','access.token.claim'),('26436ea4-6541-401f-960e-a641d9f7de79','email','claim.name'),('26436ea4-6541-401f-960e-a641d9f7de79','true','id.token.claim'),('26436ea4-6541-401f-960e-a641d9f7de79','String','jsonType.label'),('26436ea4-6541-401f-960e-a641d9f7de79','email','user.attribute'),('26436ea4-6541-401f-960e-a641d9f7de79','true','userinfo.token.claim'),('27850dff-ccde-46b7-b3a8-80f52a99e203','true','access.token.claim'),('27850dff-ccde-46b7-b3a8-80f52a99e203','nickname','claim.name'),('27850dff-ccde-46b7-b3a8-80f52a99e203','true','id.token.claim'),('27850dff-ccde-46b7-b3a8-80f52a99e203','String','jsonType.label'),('27850dff-ccde-46b7-b3a8-80f52a99e203','nickname','user.attribute'),('27850dff-ccde-46b7-b3a8-80f52a99e203','true','userinfo.token.claim'),('293e59cd-2f10-4ebc-83f8-051677a9a225','true','access.token.claim'),('293e59cd-2f10-4ebc-83f8-051677a9a225','zoneinfo','claim.name'),('293e59cd-2f10-4ebc-83f8-051677a9a225','true','id.token.claim'),('293e59cd-2f10-4ebc-83f8-051677a9a225','String','jsonType.label'),('293e59cd-2f10-4ebc-83f8-051677a9a225','zoneinfo','user.attribute'),('293e59cd-2f10-4ebc-83f8-051677a9a225','true','userinfo.token.claim'),('311396a8-42af-4f32-b72f-25f4cca65c6e','true','access.token.claim'),('311396a8-42af-4f32-b72f-25f4cca65c6e','gender','claim.name'),('311396a8-42af-4f32-b72f-25f4cca65c6e','true','id.token.claim'),('311396a8-42af-4f32-b72f-25f4cca65c6e','String','jsonType.label'),('311396a8-42af-4f32-b72f-25f4cca65c6e','gender','user.attribute'),('311396a8-42af-4f32-b72f-25f4cca65c6e','true','userinfo.token.claim'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','true','access.token.claim'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','family_name','claim.name'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','true','id.token.claim'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','String','jsonType.label'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','lastName','user.attribute'),('32a60ca8-5b2d-4476-bf8b-ff82176f2ebc','true','userinfo.token.claim'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','true','access.token.claim'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','profile','claim.name'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','true','id.token.claim'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','String','jsonType.label'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','profile','user.attribute'),('3afb20e3-4744-4aae-982b-7be17b0acd8b','true','userinfo.token.claim'),('3c358125-754f-42e5-8775-6e82084bdcba','Role','attribute.name'),('3c358125-754f-42e5-8775-6e82084bdcba','Basic','attribute.nameformat'),('3c358125-754f-42e5-8775-6e82084bdcba','false','single'),('49a1a266-9213-415a-8f2e-0f34dca27c04','true','access.token.claim'),('49a1a266-9213-415a-8f2e-0f34dca27c04','email_verified','claim.name'),('49a1a266-9213-415a-8f2e-0f34dca27c04','true','id.token.claim'),('49a1a266-9213-415a-8f2e-0f34dca27c04','boolean','jsonType.label'),('49a1a266-9213-415a-8f2e-0f34dca27c04','emailVerified','user.attribute'),('49a1a266-9213-415a-8f2e-0f34dca27c04','true','userinfo.token.claim'),('49a74b88-49ae-403c-b32a-32fad37b4805','true','access.token.claim'),('49a74b88-49ae-403c-b32a-32fad37b4805','groups','claim.name'),('49a74b88-49ae-403c-b32a-32fad37b4805','false','full.path'),('49a74b88-49ae-403c-b32a-32fad37b4805','true','id.token.claim'),('49a74b88-49ae-403c-b32a-32fad37b4805','true','userinfo.token.claim'),('5be929cf-2843-48b9-b134-d5c8e35dc479','true','access.token.claim'),('5be929cf-2843-48b9-b134-d5c8e35dc479','email_verified','claim.name'),('5be929cf-2843-48b9-b134-d5c8e35dc479','true','id.token.claim'),('5be929cf-2843-48b9-b134-d5c8e35dc479','boolean','jsonType.label'),('5be929cf-2843-48b9-b134-d5c8e35dc479','emailVerified','user.attribute'),('5be929cf-2843-48b9-b134-d5c8e35dc479','true','userinfo.token.claim'),('61128257-79a1-46db-bbed-412d3b79c54c','true','access.token.claim'),('61128257-79a1-46db-bbed-412d3b79c54c','phone_number','claim.name'),('61128257-79a1-46db-bbed-412d3b79c54c','true','id.token.claim'),('61128257-79a1-46db-bbed-412d3b79c54c','String','jsonType.label'),('61128257-79a1-46db-bbed-412d3b79c54c','phoneNumber','user.attribute'),('61128257-79a1-46db-bbed-412d3b79c54c','true','userinfo.token.claim'),('693df150-da09-4ecb-bef0-74f2413c159e','true','access.token.claim'),('693df150-da09-4ecb-bef0-74f2413c159e','true','id.token.claim'),('693df150-da09-4ecb-bef0-74f2413c159e','true','userinfo.token.claim'),('6a901660-4d46-4b35-ba18-03d056f19261','true','access.token.claim'),('6a901660-4d46-4b35-ba18-03d056f19261','website','claim.name'),('6a901660-4d46-4b35-ba18-03d056f19261','true','id.token.claim'),('6a901660-4d46-4b35-ba18-03d056f19261','String','jsonType.label'),('6a901660-4d46-4b35-ba18-03d056f19261','website','user.attribute'),('6a901660-4d46-4b35-ba18-03d056f19261','true','userinfo.token.claim'),('71f69391-8b23-4488-8e1c-12b75f5c2183','true','access.token.claim'),('71f69391-8b23-4488-8e1c-12b75f5c2183','middle_name','claim.name'),('71f69391-8b23-4488-8e1c-12b75f5c2183','true','id.token.claim'),('71f69391-8b23-4488-8e1c-12b75f5c2183','String','jsonType.label'),('71f69391-8b23-4488-8e1c-12b75f5c2183','middleName','user.attribute'),('71f69391-8b23-4488-8e1c-12b75f5c2183','true','userinfo.token.claim'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','true','access.token.claim'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','groups','claim.name'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','false','full.path'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','true','id.token.claim'),('769f0f3f-4f3e-454c-8068-ebe3c496d21f','true','userinfo.token.claim'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','true','access.token.claim'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','profile','claim.name'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','true','id.token.claim'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','String','jsonType.label'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','profile','user.attribute'),('7850f72d-45e2-4c49-81bb-ae30cc48a61e','true','userinfo.token.claim'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','true','access.token.claim'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','picture','claim.name'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','true','id.token.claim'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','String','jsonType.label'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','picture','user.attribute'),('8331f2c4-f2d3-4fdc-8c76-b35ee4036101','true','userinfo.token.claim'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','true','access.token.claim'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','given_name','claim.name'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','true','id.token.claim'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','String','jsonType.label'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','firstName','user.attribute'),('8d8d28fa-fc81-47e1-95e0-1746e8d488e9','true','userinfo.token.claim'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','true','access.token.claim'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','resource_access.${client_id}.roles','claim.name'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','String','jsonType.label'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','true','multivalued'),('8fdf3e90-8222-43d3-abad-a4ed0609338b','foo','user.attribute'),('90c53b00-fb49-47ba-8276-dd2740be59d3','true','access.token.claim'),('90c53b00-fb49-47ba-8276-dd2740be59d3','email','claim.name'),('90c53b00-fb49-47ba-8276-dd2740be59d3','true','id.token.claim'),('90c53b00-fb49-47ba-8276-dd2740be59d3','String','jsonType.label'),('90c53b00-fb49-47ba-8276-dd2740be59d3','email','user.attribute'),('90c53b00-fb49-47ba-8276-dd2740be59d3','true','userinfo.token.claim'),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','true','access.token.claim'),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','realm_access.roles','claim.name'),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','String','jsonType.label'),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','true','multivalued'),('93dabb5f-ad9e-44a5-92d0-a4eb41b99b58','foo','user.attribute'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','true','access.token.claim'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','birthdate','claim.name'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','true','id.token.claim'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','String','jsonType.label'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','birthdate','user.attribute'),('9431fda7-ef1a-4f6c-9b02-a478164c465c','true','userinfo.token.claim'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','true','access.token.claim'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','preferred_username','claim.name'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','true','id.token.claim'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','String','jsonType.label'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','username','user.attribute'),('96a822b4-bba8-42bb-b44a-99dd19a2e4d5','true','userinfo.token.claim'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','true','access.token.claim'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','birthdate','claim.name'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','true','id.token.claim'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','String','jsonType.label'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','birthdate','user.attribute'),('9a0dee38-a4a4-469d-a7de-ee573e02a6b5','true','userinfo.token.claim'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','true','access.token.claim'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','groups','claim.name'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','true','id.token.claim'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','String','jsonType.label'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','true','multivalued'),('a61e3d44-b62b-4b76-8c15-ec7aa580d6db','foo','user.attribute'),('a7aae793-03e0-412f-9b82-bcefbde48a19','true','access.token.claim'),('a7aae793-03e0-412f-9b82-bcefbde48a19','middle_name','claim.name'),('a7aae793-03e0-412f-9b82-bcefbde48a19','true','id.token.claim'),('a7aae793-03e0-412f-9b82-bcefbde48a19','String','jsonType.label'),('a7aae793-03e0-412f-9b82-bcefbde48a19','middleName','user.attribute'),('a7aae793-03e0-412f-9b82-bcefbde48a19','true','userinfo.token.claim'),('aa49005b-c653-4984-b875-0e9abbdaa98a','true','access.token.claim'),('aa49005b-c653-4984-b875-0e9abbdaa98a','phone_number_verified','claim.name'),('aa49005b-c653-4984-b875-0e9abbdaa98a','true','id.token.claim'),('aa49005b-c653-4984-b875-0e9abbdaa98a','boolean','jsonType.label'),('aa49005b-c653-4984-b875-0e9abbdaa98a','phoneNumberVerified','user.attribute'),('aa49005b-c653-4984-b875-0e9abbdaa98a','true','userinfo.token.claim'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','true','access.token.claim'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','zoneinfo','claim.name'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','true','id.token.claim'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','String','jsonType.label'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','zoneinfo','user.attribute'),('aaaf24c2-628d-4c07-b02e-edcffcf4680b','true','userinfo.token.claim'),('ac3c55fe-87df-472f-9809-d01678db9c3f','true','access.token.claim'),('ac3c55fe-87df-472f-9809-d01678db9c3f','locale','claim.name'),('ac3c55fe-87df-472f-9809-d01678db9c3f','true','id.token.claim'),('ac3c55fe-87df-472f-9809-d01678db9c3f','String','jsonType.label'),('ac3c55fe-87df-472f-9809-d01678db9c3f','locale','user.attribute'),('ac3c55fe-87df-472f-9809-d01678db9c3f','true','userinfo.token.claim'),('b1b82dc3-58e6-49d7-9154-9e6306bacf61','true','access.token.claim'),('b1b82dc3-58e6-49d7-9154-9e6306bacf61','true','id.token.claim'),('b1b82dc3-58e6-49d7-9154-9e6306bacf61','true','userinfo.token.claim'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','true','access.token.claim'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','nickname','claim.name'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','true','id.token.claim'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','String','jsonType.label'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','nickname','user.attribute'),('b1f8ee85-1fa9-4c92-ac08-45203740bbe1','true','userinfo.token.claim'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','true','access.token.claim'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','preferred_username','claim.name'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','true','id.token.claim'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','String','jsonType.label'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','username','user.attribute'),('b42c4af5-326d-4e63-bbf1-9b4364474f72','true','userinfo.token.claim'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','true','access.token.claim'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','updated_at','claim.name'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','true','id.token.claim'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','String','jsonType.label'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','updatedAt','user.attribute'),('b7c31d58-313b-4313-bda3-e62a1c5706ef','true','userinfo.token.claim'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','true','access.token.claim'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','website','claim.name'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','true','id.token.claim'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','String','jsonType.label'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','website','user.attribute'),('b8df6b57-a89d-47f7-b53f-da69cfefdae5','true','userinfo.token.claim'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','true','access.token.claim'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','given_name','claim.name'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','true','id.token.claim'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','String','jsonType.label'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','firstName','user.attribute'),('c38a76b3-60fe-4400-ad43-52f9450c4ec4','true','userinfo.token.claim'),('c691244d-bfd5-464b-beca-9af15e4963a3','true','access.token.claim'),('c691244d-bfd5-464b-beca-9af15e4963a3','upn','claim.name'),('c691244d-bfd5-464b-beca-9af15e4963a3','true','id.token.claim'),('c691244d-bfd5-464b-beca-9af15e4963a3','String','jsonType.label'),('c691244d-bfd5-464b-beca-9af15e4963a3','username','user.attribute'),('c691244d-bfd5-464b-beca-9af15e4963a3','true','userinfo.token.claim'),('c86bf90f-6396-47b4-95d9-10f81b09543f','true','access.token.claim'),('c86bf90f-6396-47b4-95d9-10f81b09543f','gender','claim.name'),('c86bf90f-6396-47b4-95d9-10f81b09543f','true','id.token.claim'),('c86bf90f-6396-47b4-95d9-10f81b09543f','String','jsonType.label'),('c86bf90f-6396-47b4-95d9-10f81b09543f','gender','user.attribute'),('c86bf90f-6396-47b4-95d9-10f81b09543f','true','userinfo.token.claim'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','true','access.token.claim'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','phone_number_verified','claim.name'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','true','id.token.claim'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','boolean','jsonType.label'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','phoneNumberVerified','user.attribute'),('c96a2323-3d21-4e0d-bb0f-50d09ce7a672','true','userinfo.token.claim'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','true','access.token.claim'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','family_name','claim.name'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','true','id.token.claim'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','String','jsonType.label'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','lastName','user.attribute'),('d813acd2-d14e-4993-8e52-19cfd4bf3a15','true','userinfo.token.claim'),('da7373e4-538f-42ed-b62a-8d254f93c437','true','access.token.claim'),('da7373e4-538f-42ed-b62a-8d254f93c437','groups','claim.name'),('da7373e4-538f-42ed-b62a-8d254f93c437','true','id.token.claim'),('da7373e4-538f-42ed-b62a-8d254f93c437','String','jsonType.label'),('da7373e4-538f-42ed-b62a-8d254f93c437','true','multivalued'),('da7373e4-538f-42ed-b62a-8d254f93c437','foo','user.attribute'),('da7373e4-538f-42ed-b62a-8d254f93c437','true','userinfo.token.claim'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','true','access.token.claim'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','phone_number','claim.name'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','true','id.token.claim'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','String','jsonType.label'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','phoneNumber','user.attribute'),('e2c8a82e-4e73-4bbf-9260-55f6aaf85414','true','userinfo.token.claim'),('ea82259e-5f19-4c1d-af25-a56d0245d93a','Role','attribute.name'),('ea82259e-5f19-4c1d-af25-a56d0245d93a','Basic','attribute.nameformat'),('ea82259e-5f19-4c1d-af25-a56d0245d93a','false','single'),('f27c9339-ea08-41ab-aa53-114c576b37e3','true','access.token.claim'),('f27c9339-ea08-41ab-aa53-114c576b37e3','true','id.token.claim'),('f27c9339-ea08-41ab-aa53-114c576b37e3','country','user.attribute.country'),('f27c9339-ea08-41ab-aa53-114c576b37e3','formatted','user.attribute.formatted'),('f27c9339-ea08-41ab-aa53-114c576b37e3','locality','user.attribute.locality'),('f27c9339-ea08-41ab-aa53-114c576b37e3','postal_code','user.attribute.postal_code'),('f27c9339-ea08-41ab-aa53-114c576b37e3','region','user.attribute.region'),('f27c9339-ea08-41ab-aa53-114c576b37e3','street','user.attribute.street'),('f27c9339-ea08-41ab-aa53-114c576b37e3','true','userinfo.token.claim');
/*!40000 ALTER TABLE `PROTOCOL_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM`
--

DROP TABLE IF EXISTS `REALM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM` (
  `ID` varchar(36) NOT NULL,
  `ACCESS_CODE_LIFESPAN` int DEFAULT NULL,
  `USER_ACTION_LIFESPAN` int DEFAULT NULL,
  `ACCESS_TOKEN_LIFESPAN` int DEFAULT NULL,
  `ACCOUNT_THEME` varchar(255) DEFAULT NULL,
  `ADMIN_THEME` varchar(255) DEFAULT NULL,
  `EMAIL_THEME` varchar(255) DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_EXPIRATION` bigint DEFAULT NULL,
  `LOGIN_THEME` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int DEFAULT NULL,
  `PASSWORD_POLICY` text,
  `REGISTRATION_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `RESET_PASSWORD_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `SOCIAL` bit(1) NOT NULL DEFAULT b'0',
  `SSL_REQUIRED` varchar(255) DEFAULT NULL,
  `SSO_IDLE_TIMEOUT` int DEFAULT NULL,
  `SSO_MAX_LIFESPAN` int DEFAULT NULL,
  `UPDATE_PROFILE_ON_SOC_LOGIN` bit(1) NOT NULL DEFAULT b'0',
  `VERIFY_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `MASTER_ADMIN_CLIENT` varchar(36) DEFAULT NULL,
  `LOGIN_LIFESPAN` int DEFAULT NULL,
  `INTERNATIONALIZATION_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_LOCALE` varchar(255) DEFAULT NULL,
  `REG_EMAIL_AS_USERNAME` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_DETAILS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EDIT_USERNAME_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `OTP_POLICY_COUNTER` int DEFAULT '0',
  `OTP_POLICY_WINDOW` int DEFAULT '1',
  `OTP_POLICY_PERIOD` int DEFAULT '30',
  `OTP_POLICY_DIGITS` int DEFAULT '6',
  `OTP_POLICY_ALG` varchar(36) DEFAULT 'HmacSHA1',
  `OTP_POLICY_TYPE` varchar(36) DEFAULT 'totp',
  `BROWSER_FLOW` varchar(36) DEFAULT NULL,
  `REGISTRATION_FLOW` varchar(36) DEFAULT NULL,
  `DIRECT_GRANT_FLOW` varchar(36) DEFAULT NULL,
  `RESET_CREDENTIALS_FLOW` varchar(36) DEFAULT NULL,
  `CLIENT_AUTH_FLOW` varchar(36) DEFAULT NULL,
  `OFFLINE_SESSION_IDLE_TIMEOUT` int DEFAULT '0',
  `REVOKE_REFRESH_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `ACCESS_TOKEN_LIFE_IMPLICIT` int DEFAULT '0',
  `LOGIN_WITH_EMAIL_ALLOWED` bit(1) NOT NULL DEFAULT b'1',
  `DUPLICATE_EMAILS_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `DOCKER_AUTH_FLOW` varchar(36) DEFAULT NULL,
  `REFRESH_TOKEN_MAX_REUSE` int DEFAULT '0',
  `ALLOW_USER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `SSO_MAX_LIFESPAN_REMEMBER_ME` int NOT NULL,
  `SSO_IDLE_TIMEOUT_REMEMBER_ME` int NOT NULL,
  `DEFAULT_ROLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ORVSDMLA56612EAEFIQ6WL5OI` (`NAME`),
  KEY `IDX_REALM_MASTER_ADM_CLI` (`MASTER_ADMIN_CLIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM`
--

LOCK TABLES `REALM` WRITE;
/*!40000 ALTER TABLE `REALM` DISABLE KEYS */;
INSERT INTO `REALM` VALUES ('local',60,300,300,NULL,NULL,NULL,_binary '',_binary '\0',0,NULL,'local',0,NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0','EXTERNAL',1800,36000,_binary '\0',_binary '\0','be9f0e71-625f-4198-a2ae-cffa4a38dd6b',1800,_binary '\0',NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0',0,1,30,6,'HmacSHA1','totp','6e2d142c-752e-41d3-8080-f12664cdb0b4','71b3a190-38a6-49fd-b5db-3414e00e8765','4072fe0f-7f2c-4083-adee-3e78e274a7e4','0df0da2a-7ae2-4b9c-a6cc-74de03562634','a979bb85-af13-45a5-8c7b-4bcc75e896ce',2592000,_binary '\0',900,_binary '',_binary '\0','be0d678d-1bc1-40fe-85b1-8ff79bb050b7',0,_binary '\0',0,0,'10bf3aca-99d5-4961-99fd-9440b12e89cf'),('master',60,300,60,NULL,NULL,NULL,_binary '',_binary '\0',0,NULL,'master',0,NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0','EXTERNAL',1800,36000,_binary '\0',_binary '\0','8f806415-f35d-48c2-bf8a-0f00003bbc72',1800,_binary '\0',NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0',0,1,30,6,'HmacSHA1','totp','206a6fd5-e35a-4c13-81fa-55c9ec9aadc2','7820aec0-0c47-4de7-8c4f-16cccde7b59c','6c9584c6-9c33-4f4c-b180-81d3932b27f3','ca058d70-a38f-429e-94ca-55719ceef211','449265ae-aefd-44df-a3b2-4e76f490e099',2592000,_binary '\0',900,_binary '',_binary '\0','430755bf-e16a-45af-a87c-5c60a2e7113a',0,_binary '\0',0,0,'42d5cde3-10be-4922-900e-76eaa8dd1800');
/*!40000 ALTER TABLE `REALM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_ATTRIBUTE`
--

DROP TABLE IF EXISTS `REALM_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_ATTRIBUTE` (
  `NAME` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`NAME`,`REALM_ID`),
  KEY `IDX_REALM_ATTR_REALM` (`REALM_ID`),
  CONSTRAINT `FK_8SHXD6L3E9ATQUKACXGPFFPTW` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_ATTRIBUTE`
--

LOCK TABLES `REALM_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `REALM_ATTRIBUTE` DISABLE KEYS */;
INSERT INTO `REALM_ATTRIBUTE` VALUES ('_browser_header.contentSecurityPolicy','local','frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';'),('_browser_header.contentSecurityPolicy','master','frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';'),('_browser_header.contentSecurityPolicyReportOnly','local',''),('_browser_header.contentSecurityPolicyReportOnly','master',''),('_browser_header.strictTransportSecurity','local','max-age=31536000; includeSubDomains'),('_browser_header.strictTransportSecurity','master','max-age=31536000; includeSubDomains'),('_browser_header.xContentTypeOptions','local','nosniff'),('_browser_header.xContentTypeOptions','master','nosniff'),('_browser_header.xFrameOptions','local','SAMEORIGIN'),('_browser_header.xFrameOptions','master','SAMEORIGIN'),('_browser_header.xRobotsTag','local','none'),('_browser_header.xRobotsTag','master','none'),('_browser_header.xXSSProtection','local','1; mode=block'),('_browser_header.xXSSProtection','master','1; mode=block'),('actionTokenGeneratedByAdminLifespan','local','43200'),('actionTokenGeneratedByUserLifespan','local','300'),('bruteForceProtected','local','false'),('bruteForceProtected','master','false'),('cibaAuthRequestedUserHint','local','login_hint'),('cibaBackchannelTokenDeliveryMode','local','poll'),('cibaExpiresIn','local','120'),('cibaInterval','local','5'),('client-policies.policies','local','{\"policies\":[]}'),('client-policies.profiles','local','{\"profiles\":[]}'),('clientOfflineSessionIdleTimeout','local','0'),('clientOfflineSessionMaxLifespan','local','0'),('clientSessionIdleTimeout','local','0'),('clientSessionMaxLifespan','local','0'),('defaultSignatureAlgorithm','local','RS256'),('defaultSignatureAlgorithm','master','RS256'),('displayName','master','Keycloak'),('displayNameHtml','master','<div class=\"kc-logo-text\"><span>Keycloak</span></div>'),('failureFactor','local','30'),('failureFactor','master','30'),('maxDeltaTimeSeconds','local','43200'),('maxDeltaTimeSeconds','master','43200'),('maxFailureWaitSeconds','local','900'),('maxFailureWaitSeconds','master','900'),('minimumQuickLoginWaitSeconds','local','60'),('minimumQuickLoginWaitSeconds','master','60'),('oauth2DeviceCodeLifespan','local','600'),('oauth2DevicePollingInterval','local','5'),('offlineSessionMaxLifespan','local','5184000'),('offlineSessionMaxLifespan','master','5184000'),('offlineSessionMaxLifespanEnabled','local','false'),('offlineSessionMaxLifespanEnabled','master','false'),('parRequestUriLifespan','local','60'),('permanentLockout','local','false'),('permanentLockout','master','false'),('quickLoginCheckMilliSeconds','local','1000'),('quickLoginCheckMilliSeconds','master','1000'),('waitIncrementSeconds','local','60'),('waitIncrementSeconds','master','60'),('webAuthnPolicyAttestationConveyancePreference','local','not specified'),('webAuthnPolicyAttestationConveyancePreferencePasswordless','local','not specified'),('webAuthnPolicyAuthenticatorAttachment','local','not specified'),('webAuthnPolicyAuthenticatorAttachmentPasswordless','local','not specified'),('webAuthnPolicyAvoidSameAuthenticatorRegister','local','false'),('webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless','local','false'),('webAuthnPolicyCreateTimeout','local','0'),('webAuthnPolicyCreateTimeoutPasswordless','local','0'),('webAuthnPolicyRequireResidentKey','local','not specified'),('webAuthnPolicyRequireResidentKeyPasswordless','local','not specified'),('webAuthnPolicyRpEntityName','local','keycloak'),('webAuthnPolicyRpEntityNamePasswordless','local','keycloak'),('webAuthnPolicyRpId','local',''),('webAuthnPolicyRpIdPasswordless','local',''),('webAuthnPolicySignatureAlgorithms','local','ES256'),('webAuthnPolicySignatureAlgorithmsPasswordless','local','ES256'),('webAuthnPolicyUserVerificationRequirement','local','not specified'),('webAuthnPolicyUserVerificationRequirementPasswordless','local','not specified');
/*!40000 ALTER TABLE `REALM_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_DEFAULT_GROUPS`
--

DROP TABLE IF EXISTS `REALM_DEFAULT_GROUPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_DEFAULT_GROUPS` (
  `REALM_ID` varchar(36) NOT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`GROUP_ID`),
  UNIQUE KEY `CON_GROUP_ID_DEF_GROUPS` (`GROUP_ID`),
  KEY `IDX_REALM_DEF_GRP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_DEF_GROUPS_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_DEFAULT_GROUPS`
--

LOCK TABLES `REALM_DEFAULT_GROUPS` WRITE;
/*!40000 ALTER TABLE `REALM_DEFAULT_GROUPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_DEFAULT_GROUPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_ENABLED_EVENT_TYPES`
--

DROP TABLE IF EXISTS `REALM_ENABLED_EVENT_TYPES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_ENABLED_EVENT_TYPES` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_TYPES_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NWEDRF5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_ENABLED_EVENT_TYPES`
--

LOCK TABLES `REALM_ENABLED_EVENT_TYPES` WRITE;
/*!40000 ALTER TABLE `REALM_ENABLED_EVENT_TYPES` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_ENABLED_EVENT_TYPES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_EVENTS_LISTENERS`
--

DROP TABLE IF EXISTS `REALM_EVENTS_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_EVENTS_LISTENERS` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_LIST_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NXEV9F5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_EVENTS_LISTENERS`
--

LOCK TABLES `REALM_EVENTS_LISTENERS` WRITE;
/*!40000 ALTER TABLE `REALM_EVENTS_LISTENERS` DISABLE KEYS */;
INSERT INTO `REALM_EVENTS_LISTENERS` VALUES ('local','jboss-logging'),('master','jboss-logging');
/*!40000 ALTER TABLE `REALM_EVENTS_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_LOCALIZATIONS`
--

DROP TABLE IF EXISTS `REALM_LOCALIZATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_LOCALIZATIONS` (
  `REALM_ID` varchar(255) NOT NULL,
  `LOCALE` varchar(255) NOT NULL,
  `TEXTS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`REALM_ID`,`LOCALE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_LOCALIZATIONS`
--

LOCK TABLES `REALM_LOCALIZATIONS` WRITE;
/*!40000 ALTER TABLE `REALM_LOCALIZATIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_LOCALIZATIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_REQUIRED_CREDENTIAL`
--

DROP TABLE IF EXISTS `REALM_REQUIRED_CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_REQUIRED_CREDENTIAL` (
  `TYPE` varchar(255) NOT NULL,
  `FORM_LABEL` varchar(255) DEFAULT NULL,
  `INPUT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`TYPE`),
  CONSTRAINT `FK_5HG65LYBEVAVKQFKI3KPONH9V` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_REQUIRED_CREDENTIAL`
--

LOCK TABLES `REALM_REQUIRED_CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `REALM_REQUIRED_CREDENTIAL` DISABLE KEYS */;
INSERT INTO `REALM_REQUIRED_CREDENTIAL` VALUES ('password','password',_binary '',_binary '','local'),('password','password',_binary '',_binary '','master');
/*!40000 ALTER TABLE `REALM_REQUIRED_CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_SMTP_CONFIG`
--

DROP TABLE IF EXISTS `REALM_SMTP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_SMTP_CONFIG` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`NAME`),
  CONSTRAINT `FK_70EJ8XDXGXD0B9HH6180IRR0O` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_SMTP_CONFIG`
--

LOCK TABLES `REALM_SMTP_CONFIG` WRITE;
/*!40000 ALTER TABLE `REALM_SMTP_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_SMTP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_SUPPORTED_LOCALES`
--

DROP TABLE IF EXISTS `REALM_SUPPORTED_LOCALES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_SUPPORTED_LOCALES` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_SUPP_LOCAL_REALM` (`REALM_ID`),
  CONSTRAINT `FK_SUPPORTED_LOCALES_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_SUPPORTED_LOCALES`
--

LOCK TABLES `REALM_SUPPORTED_LOCALES` WRITE;
/*!40000 ALTER TABLE `REALM_SUPPORTED_LOCALES` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_SUPPORTED_LOCALES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REDIRECT_URIS`
--

DROP TABLE IF EXISTS `REDIRECT_URIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REDIRECT_URIS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_REDIR_URI_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_1BURS8PB4OUJ97H5WUPPAHV9F` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REDIRECT_URIS`
--

LOCK TABLES `REDIRECT_URIS` WRITE;
/*!40000 ALTER TABLE `REDIRECT_URIS` DISABLE KEYS */;
INSERT INTO `REDIRECT_URIS` VALUES ('047d93f5-955c-41c1-9513-75a8745618c3','http://*'),('047d93f5-955c-41c1-9513-75a8745618c3','https://*'),('1da632b4-3088-4044-a3db-99b423a1872c','http://*'),('1da632b4-3088-4044-a3db-99b423a1872c','https://*'),('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','/realms/local/account/*'),('55e8799a-e472-4690-a56f-f07d3920595d','/admin/local/console/*'),('5f9bc463-02df-4f95-b60e-bec7b41b9b68','/realms/master/account/*'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','/admin/master/console/*'),('86204c34-f02a-4a5b-968b-a1563ad2be76','/realms/local/account/*'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','http://*'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','https://*'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','/realms/master/account/*');
/*!40000 ALTER TABLE `REDIRECT_URIS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REQUIRED_ACTION_CONFIG`
--

DROP TABLE IF EXISTS `REQUIRED_ACTION_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REQUIRED_ACTION_CONFIG` (
  `REQUIRED_ACTION_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`REQUIRED_ACTION_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REQUIRED_ACTION_CONFIG`
--

LOCK TABLES `REQUIRED_ACTION_CONFIG` WRITE;
/*!40000 ALTER TABLE `REQUIRED_ACTION_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `REQUIRED_ACTION_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REQUIRED_ACTION_PROVIDER`
--

DROP TABLE IF EXISTS `REQUIRED_ACTION_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REQUIRED_ACTION_PROVIDER` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_ACTION` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ID` varchar(255) DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_REQ_ACT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_REQ_ACT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REQUIRED_ACTION_PROVIDER`
--

LOCK TABLES `REQUIRED_ACTION_PROVIDER` WRITE;
/*!40000 ALTER TABLE `REQUIRED_ACTION_PROVIDER` DISABLE KEYS */;
INSERT INTO `REQUIRED_ACTION_PROVIDER` VALUES ('115f940b-cc46-4215-a15b-849a4b0c4e24','UPDATE_PROFILE','Update Profile','master',_binary '',_binary '\0','UPDATE_PROFILE',40),('5e81e641-254d-40c1-9296-a20cda825137','CONFIGURE_TOTP','Configure OTP','local',_binary '',_binary '\0','CONFIGURE_TOTP',10),('7576fece-f51e-4569-a2d0-ebadd3ab18ee','UPDATE_PROFILE','Update Profile','local',_binary '',_binary '\0','UPDATE_PROFILE',40),('83c3e686-49cb-4e46-95ca-961f25f9e060','update_user_locale','Update User Locale','master',_binary '',_binary '\0','update_user_locale',1000),('89da1f1b-a551-449e-9ebc-bf7d6e66c4c9','UPDATE_PASSWORD','Update Password','master',_binary '',_binary '\0','UPDATE_PASSWORD',30),('95b674ef-4dfe-4d85-b643-865f934b66ed','delete_account','Delete Account','local',_binary '\0',_binary '\0','delete_account',60),('9ab2795d-a4c8-4e27-91fa-7feeef036a99','UPDATE_PASSWORD','Update Password','local',_binary '',_binary '\0','UPDATE_PASSWORD',30),('9bd0541f-043a-4b0a-aa6e-0fcab69ad710','VERIFY_EMAIL','Verify Email','local',_binary '',_binary '\0','VERIFY_EMAIL',50),('a629ba7f-a172-4af5-8347-14bf960f03c7','terms_and_conditions','Terms and Conditions','local',_binary '\0',_binary '\0','terms_and_conditions',20),('ce23ee09-ea1a-492e-b6fa-7e222a6bdc7c','terms_and_conditions','Terms and Conditions','master',_binary '\0',_binary '\0','terms_and_conditions',20),('d7fdfcab-5913-4aff-839e-0b0285dee9fa','update_user_locale','Update User Locale','local',_binary '',_binary '\0','update_user_locale',1000),('e67ae47c-a86b-45c5-94bf-f089f89fb06d','VERIFY_EMAIL','Verify Email','master',_binary '',_binary '\0','VERIFY_EMAIL',50),('f4a93fc0-570c-4c57-9db3-f149eb3e25fe','delete_account','Delete Account','master',_binary '\0',_binary '\0','delete_account',60),('f6283e4d-c90c-4059-af5c-10c772fdcdc1','CONFIGURE_TOTP','Configure OTP','master',_binary '',_binary '\0','CONFIGURE_TOTP',10);
/*!40000 ALTER TABLE `REQUIRED_ACTION_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_ATTRIBUTE`
--

DROP TABLE IF EXISTS `RESOURCE_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `RESOURCE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_5HRM2VLF9QL5FU022KQEPOVBR` (`RESOURCE_ID`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU022KQEPOVBR` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_ATTRIBUTE`
--

LOCK TABLES `RESOURCE_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_POLICY`
--

DROP TABLE IF EXISTS `RESOURCE_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_POLICY` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`POLICY_ID`),
  KEY `IDX_RES_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRPOS53XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRPP213XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_POLICY`
--

LOCK TABLES `RESOURCE_POLICY` WRITE;
/*!40000 ALTER TABLE `RESOURCE_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SCOPE`
--

DROP TABLE IF EXISTS `RESOURCE_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SCOPE` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`SCOPE_ID`),
  KEY `IDX_RES_SCOPE_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_FRSRPOS13XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRPS213XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SCOPE`
--

LOCK TABLES `RESOURCE_SCOPE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER` (
  `ID` varchar(36) NOT NULL,
  `ALLOW_RS_REMOTE_MGMT` bit(1) NOT NULL DEFAULT b'0',
  `POLICY_ENFORCE_MODE` varchar(15) NOT NULL,
  `DECISION_STRATEGY` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER`
--

LOCK TABLES `RESOURCE_SERVER` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER` DISABLE KEYS */;
INSERT INTO `RESOURCE_SERVER` VALUES ('1da632b4-3088-4044-a3db-99b423a1872c',_binary '','0',1);
/*!40000 ALTER TABLE `RESOURCE_SERVER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_PERM_TICKET`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_PERM_TICKET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_PERM_TICKET` (
  `ID` varchar(36) NOT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  `REQUESTER` varchar(255) DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint NOT NULL,
  `GRANTED_TIMESTAMP` bigint DEFAULT NULL,
  `RESOURCE_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5PMT` (`OWNER`,`REQUESTER`,`RESOURCE_SERVER_ID`,`RESOURCE_ID`,`SCOPE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG82SSPMT` (`RESOURCE_SERVER_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG83SSPMT` (`RESOURCE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG84SSPMT` (`SCOPE_ID`),
  KEY `FK_FRSRPO2128CX4WNKOG82SSRFY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSPMT` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG83SSPMT` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG84SSPMT` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`),
  CONSTRAINT `FK_FRSRPO2128CX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_PERM_TICKET`
--

LOCK TABLES `RESOURCE_SERVER_PERM_TICKET` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_PERM_TICKET` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_PERM_TICKET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_POLICY`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_POLICY` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TYPE` varchar(255) NOT NULL,
  `DECISION_STRATEGY` varchar(20) DEFAULT NULL,
  `LOGIC` varchar(20) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRPT700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SERV_POL_RES_SERV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRPO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_POLICY`
--

LOCK TABLES `RESOURCE_SERVER_POLICY` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_POLICY` DISABLE KEYS */;
INSERT INTO `RESOURCE_SERVER_POLICY` VALUES ('3c745221-bf17-47e7-9011-bd9150bd7b7e','public-client-access',NULL,'client','1','0','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('817ac65c-c0b0-4d46-8091-d6a89f8371cb','Default Permission','A permission that applies to the default resource type','resource','1','0','1da632b4-3088-4044-a3db-99b423a1872c',NULL),('f57573f1-9f27-4f2d-9399-aa353efd077d','Default Policy','A policy that grants access only for users within this realm','js','0','0','1da632b4-3088-4044-a3db-99b423a1872c',NULL);
/*!40000 ALTER TABLE `RESOURCE_SERVER_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_RESOURCE`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_RESOURCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_RESOURCE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `ICON_URI` varchar(255) DEFAULT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `OWNER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5HA6` (`NAME`,`OWNER`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_RES_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_RESOURCE`
--

LOCK TABLES `RESOURCE_SERVER_RESOURCE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_RESOURCE` DISABLE KEYS */;
INSERT INTO `RESOURCE_SERVER_RESOURCE` VALUES ('f1c34c6a-0103-4b2d-a1cf-ec49226e1bc7','Default Resource','urn:kong:resources:default',NULL,'1da632b4-3088-4044-a3db-99b423a1872c','1da632b4-3088-4044-a3db-99b423a1872c',_binary '\0',NULL);
/*!40000 ALTER TABLE `RESOURCE_SERVER_RESOURCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_SCOPE`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_SCOPE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `ICON_URI` varchar(255) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRST700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_SCOPE_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRSO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_SCOPE`
--

LOCK TABLES `RESOURCE_SERVER_SCOPE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_URIS`
--

DROP TABLE IF EXISTS `RESOURCE_URIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_URIS` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`VALUE`),
  CONSTRAINT `FK_RESOURCE_SERVER_URIS` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_URIS`
--

LOCK TABLES `RESOURCE_URIS` WRITE;
/*!40000 ALTER TABLE `RESOURCE_URIS` DISABLE KEYS */;
INSERT INTO `RESOURCE_URIS` VALUES ('f1c34c6a-0103-4b2d-a1cf-ec49226e1bc7','/*');
/*!40000 ALTER TABLE `RESOURCE_URIS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE_ATTRIBUTE`
--

DROP TABLE IF EXISTS `ROLE_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ROLE_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ROLE_ATTRIBUTE` (`ROLE_ID`),
  CONSTRAINT `FK_ROLE_ATTRIBUTE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE_ATTRIBUTE`
--

LOCK TABLES `ROLE_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `ROLE_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `ROLE_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCOPE_MAPPING`
--

DROP TABLE IF EXISTS `SCOPE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SCOPE_MAPPING` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`ROLE_ID`),
  KEY `IDX_SCOPE_MAPPING_ROLE` (`ROLE_ID`),
  CONSTRAINT `FK_OUSE064PLMLR732LXJCN1Q5F1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCOPE_MAPPING`
--

LOCK TABLES `SCOPE_MAPPING` WRITE;
/*!40000 ALTER TABLE `SCOPE_MAPPING` DISABLE KEYS */;
INSERT INTO `SCOPE_MAPPING` VALUES ('2907bf1e-c935-4b8a-a9aa-b3756413b4fc','b92960e3-b610-4524-bb65-5419773e429a'),('e9d7b805-7d49-4676-aeab-5d7ce926f7c0','fb0ee30e-91d7-41f1-b3d4-f67e07af2bc6');
/*!40000 ALTER TABLE `SCOPE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCOPE_POLICY`
--

DROP TABLE IF EXISTS `SCOPE_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SCOPE_POLICY` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`POLICY_ID`),
  KEY `IDX_SCOPE_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRASP13XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`),
  CONSTRAINT `FK_FRSRPASS3XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCOPE_POLICY`
--

LOCK TABLES `SCOPE_POLICY` WRITE;
/*!40000 ALTER TABLE `SCOPE_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCOPE_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERNAME_LOGIN_FAILURE`
--

DROP TABLE IF EXISTS `USERNAME_LOGIN_FAILURE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USERNAME_LOGIN_FAILURE` (
  `REALM_ID` varchar(36) NOT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FAILED_LOGIN_NOT_BEFORE` int DEFAULT NULL,
  `LAST_FAILURE` bigint DEFAULT NULL,
  `LAST_IP_FAILURE` varchar(255) DEFAULT NULL,
  `NUM_FAILURES` int DEFAULT NULL,
  PRIMARY KEY (`REALM_ID`,`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERNAME_LOGIN_FAILURE`
--

LOCK TABLES `USERNAME_LOGIN_FAILURE` WRITE;
/*!40000 ALTER TABLE `USERNAME_LOGIN_FAILURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERNAME_LOGIN_FAILURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ATTRIBUTE`
--

DROP TABLE IF EXISTS `USER_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ATTRIBUTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_ATTRIBUTE` (`USER_ID`),
  KEY `IDX_USER_ATTRIBUTE_NAME` (`NAME`,`VALUE`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU043KQEPOVBR` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ATTRIBUTE`
--

LOCK TABLES `USER_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `USER_ATTRIBUTE` DISABLE KEYS */;
INSERT INTO `USER_ATTRIBUTE` VALUES ('color','blue','5ced3fd1-588e-4b21-adb2-9137a8081ae4','3857dee4-9578-404a-948b-0e44a67e19d7'),('phone','+37369784512','5ced3fd1-588e-4b21-adb2-9137a8081ae4','696f3468-4c8d-418b-8c70-a5cd23898ae3'),('company','Chij&Co','c76178e9-8786-4fc8-a9b7-081013372cdf','7e8608d9-921b-4063-85cb-eb5dcbd12bf5'),('industry','food','c76178e9-8786-4fc8-a9b7-081013372cdf','9392e80b-22a9-47e9-be57-6d0616c655b0'),('music','jazz','5ced3fd1-588e-4b21-adb2-9137a8081ae4','9fbedbb2-eee5-4f00-af18-85dc36f4ab5f'),('phone','+373968542','c76178e9-8786-4fc8-a9b7-081013372cdf','af490dc4-f160-4d6b-b0b6-91022415e49b'),('animal','cat','5ced3fd1-588e-4b21-adb2-9137a8081ae4','e45d409e-b4d8-4132-bb91-da4b09ffbb06');
/*!40000 ALTER TABLE `USER_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_CONSENT`
--

DROP TABLE IF EXISTS `USER_CONSENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_CONSENT` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_JKUWUVD56ONTGSUHOGM8UEWRT` (`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`USER_ID`),
  KEY `IDX_USER_CONSENT` (`USER_ID`),
  CONSTRAINT `FK_GRNTCSNT_USER` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_CONSENT`
--

LOCK TABLES `USER_CONSENT` WRITE;
/*!40000 ALTER TABLE `USER_CONSENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_CONSENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_CONSENT_CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `USER_CONSENT_CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_CONSENT_CLIENT_SCOPE` (
  `USER_CONSENT_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`),
  KEY `IDX_USCONSENT_CLSCOPE` (`USER_CONSENT_ID`),
  CONSTRAINT `FK_GRNTCSNT_CLSC_USC` FOREIGN KEY (`USER_CONSENT_ID`) REFERENCES `USER_CONSENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_CONSENT_CLIENT_SCOPE`
--

LOCK TABLES `USER_CONSENT_CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `USER_CONSENT_CLIENT_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_CONSENT_CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ENTITY`
--

DROP TABLE IF EXISTS `USER_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `EMAIL_CONSTRAINT` varchar(255) DEFAULT NULL,
  `EMAIL_VERIFIED` bit(1) NOT NULL DEFAULT b'0',
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FEDERATION_LINK` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint DEFAULT NULL,
  `SERVICE_ACCOUNT_CLIENT_LINK` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_DYKN684SL8UP1CRFEI6ECKHD7` (`REALM_ID`,`EMAIL_CONSTRAINT`),
  UNIQUE KEY `UK_RU8TT6T700S9V50BU18WS5HA6` (`REALM_ID`,`USERNAME`),
  KEY `IDX_USER_EMAIL` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ENTITY`
--

LOCK TABLES `USER_ENTITY` WRITE;
/*!40000 ALTER TABLE `USER_ENTITY` DISABLE KEYS */;
INSERT INTO `USER_ENTITY` VALUES ('461c341c-ed1d-4d26-aab4-1b053c7d6ec2',NULL,'2e9b2790-c7f7-438f-80b7-8b71b1b4efe9',_binary '\0',_binary '',NULL,NULL,NULL,'master','admin',1631085343573,NULL,0),('5ced3fd1-588e-4b21-adb2-9137a8081ae4','johndoe@email.com','johndoe@email.com',_binary '\0',_binary '',NULL,'John','Doe','local','johndoe',1631085776337,NULL,0),('7d2965a8-2e24-4381-a687-02a6ef446f04',NULL,'3522ae94-f33c-4a54-b80e-f29b2b1665ba',_binary '\0',_binary '',NULL,NULL,NULL,'local','service-account-kong',1633082229839,'1da632b4-3088-4044-a3db-99b423a1872c',0),('c76178e9-8786-4fc8-a9b7-081013372cdf','adamsmith@email.com','adamsmith@email.com',_binary '\0',_binary '',NULL,'Adam','Smith','local','adamsmith',1631086001844,NULL,0);
/*!40000 ALTER TABLE `USER_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_CONFIG`
--

DROP TABLE IF EXISTS `USER_FEDERATION_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_CONFIG` (
  `USER_FEDERATION_PROVIDER_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FK_T13HPU1J94R2EBPEKR39X5EU5` FOREIGN KEY (`USER_FEDERATION_PROVIDER_ID`) REFERENCES `USER_FEDERATION_PROVIDER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_CONFIG`
--

LOCK TABLES `USER_FEDERATION_CONFIG` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_MAPPER`
--

DROP TABLE IF EXISTS `USER_FEDERATION_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `FEDERATION_PROVIDER_ID` varchar(36) NOT NULL,
  `FEDERATION_MAPPER_TYPE` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_MAP_FED_PRV` (`FEDERATION_PROVIDER_ID`),
  KEY `IDX_USR_FED_MAP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_FEDMAPPERPM_FEDPRV` FOREIGN KEY (`FEDERATION_PROVIDER_ID`) REFERENCES `USER_FEDERATION_PROVIDER` (`ID`),
  CONSTRAINT `FK_FEDMAPPERPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_MAPPER`
--

LOCK TABLES `USER_FEDERATION_MAPPER` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `USER_FEDERATION_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_MAPPER_CONFIG` (
  `USER_FEDERATION_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_FEDMAPPER_CFG` FOREIGN KEY (`USER_FEDERATION_MAPPER_ID`) REFERENCES `USER_FEDERATION_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_MAPPER_CONFIG`
--

LOCK TABLES `USER_FEDERATION_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_PROVIDER`
--

DROP TABLE IF EXISTS `USER_FEDERATION_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_PROVIDER` (
  `ID` varchar(36) NOT NULL,
  `CHANGED_SYNC_PERIOD` int DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `FULL_SYNC_PERIOD` int DEFAULT NULL,
  `LAST_SYNC` int DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  `PROVIDER_NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_PRV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_1FJ32F6PTOLW2QY60CD8N01E8` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_PROVIDER`
--

LOCK TABLES `USER_FEDERATION_PROVIDER` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_PROVIDER` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_GROUP_MEMBERSHIP`
--

DROP TABLE IF EXISTS `USER_GROUP_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_GROUP_MEMBERSHIP` (
  `GROUP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_USER_GROUP_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_USER_GROUP_USER` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_GROUP_MEMBERSHIP`
--

LOCK TABLES `USER_GROUP_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `USER_GROUP_MEMBERSHIP` DISABLE KEYS */;
INSERT INTO `USER_GROUP_MEMBERSHIP` VALUES ('2a1cdba5-7534-4f3b-a2de-e1b8003d051f','5ced3fd1-588e-4b21-adb2-9137a8081ae4'),('2a1cdba5-7534-4f3b-a2de-e1b8003d051f','c76178e9-8786-4fc8-a9b7-081013372cdf');
/*!40000 ALTER TABLE `USER_GROUP_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_REQUIRED_ACTION`
--

DROP TABLE IF EXISTS `USER_REQUIRED_ACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_REQUIRED_ACTION` (
  `USER_ID` varchar(36) NOT NULL,
  `REQUIRED_ACTION` varchar(255) NOT NULL DEFAULT ' ',
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_USER_REQACTIONS` (`USER_ID`),
  CONSTRAINT `FK_6QJ3W1JW9CVAFHE19BWSIUVMD` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_REQUIRED_ACTION`
--

LOCK TABLES `USER_REQUIRED_ACTION` WRITE;
/*!40000 ALTER TABLE `USER_REQUIRED_ACTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_REQUIRED_ACTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `USER_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ROLE_MAPPING` (
  `ROLE_ID` varchar(255) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_USER_ROLE_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_C4FQV34P1MBYLLOXANG7B1Q3L` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ROLE_MAPPING`
--

LOCK TABLES `USER_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `USER_ROLE_MAPPING` DISABLE KEYS */;
INSERT INTO `USER_ROLE_MAPPING` VALUES ('42d5cde3-10be-4922-900e-76eaa8dd1800','461c341c-ed1d-4d26-aab4-1b053c7d6ec2'),('636ebdf3-cefc-4681-b9a0-1b6eb7288e1c','461c341c-ed1d-4d26-aab4-1b053c7d6ec2'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','5ced3fd1-588e-4b21-adb2-9137a8081ae4'),('48895f8a-ac48-40c5-9ff3-f37f36cbda19','5ced3fd1-588e-4b21-adb2-9137a8081ae4'),('bb2aca38-a8ed-4f44-b79e-2bccc5331f66','5ced3fd1-588e-4b21-adb2-9137a8081ae4'),('f56b3191-40f3-45e6-9e7d-065dd48af8bb','5ced3fd1-588e-4b21-adb2-9137a8081ae4'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','7d2965a8-2e24-4381-a687-02a6ef446f04'),('ab53923b-5e27-4607-8725-918a8ce16fc7','7d2965a8-2e24-4381-a687-02a6ef446f04'),('10bf3aca-99d5-4961-99fd-9440b12e89cf','c76178e9-8786-4fc8-a9b7-081013372cdf'),('2e08c627-99ce-48ba-8534-18c0e449097b','c76178e9-8786-4fc8-a9b7-081013372cdf'),('88903b73-c532-4c1f-8095-1b998513fc94','c76178e9-8786-4fc8-a9b7-081013372cdf'),('e4ca969d-7388-4d19-9eea-821ee8947e0f','c76178e9-8786-4fc8-a9b7-081013372cdf');
/*!40000 ALTER TABLE `USER_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_SESSION`
--

DROP TABLE IF EXISTS `USER_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_SESSION` (
  `ID` varchar(36) NOT NULL,
  `AUTH_METHOD` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `LAST_SESSION_REFRESH` int DEFAULT NULL,
  `LOGIN_USERNAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `STARTED` int DEFAULT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  `USER_SESSION_STATE` int DEFAULT NULL,
  `BROKER_SESSION_ID` varchar(255) DEFAULT NULL,
  `BROKER_USER_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_SESSION`
--

LOCK TABLES `USER_SESSION` WRITE;
/*!40000 ALTER TABLE `USER_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_SESSION_NOTE`
--

DROP TABLE IF EXISTS `USER_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_SESSION_NOTE` (
  `USER_SESSION` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` text,
  PRIMARY KEY (`USER_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51D3472` FOREIGN KEY (`USER_SESSION`) REFERENCES `USER_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_SESSION_NOTE`
--

LOCK TABLES `USER_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `USER_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WEB_ORIGINS`
--

DROP TABLE IF EXISTS `WEB_ORIGINS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `WEB_ORIGINS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_WEB_ORIG_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_LOJPHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WEB_ORIGINS`
--

LOCK TABLES `WEB_ORIGINS` WRITE;
/*!40000 ALTER TABLE `WEB_ORIGINS` DISABLE KEYS */;
INSERT INTO `WEB_ORIGINS` VALUES ('047d93f5-955c-41c1-9513-75a8745618c3','*'),('1da632b4-3088-4044-a3db-99b423a1872c','*'),('55e8799a-e472-4690-a56f-f07d3920595d','+'),('84b80f41-cefb-460d-9d0a-89ffacb20fc8','+'),('c3e97a1f-c628-452c-bfaa-b0b1bd226c91','*');
/*!40000 ALTER TABLE `WEB_ORIGINS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-01 13:44:59
