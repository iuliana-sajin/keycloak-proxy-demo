Database configuration [official documentation](https://www.keycloak.org/docs/latest/server_installation/#_database)

1. Data source configuration was changed in ***standalone***-*files: 
```xml
<datasource jndi-name="java:jboss/datasources/KeycloakDS" pool-name="KeycloakDS" enabled="true" use-java-context="true" use-ccm="false">
    <connection-url>jdbc:mysql://${env.DB_ADDR:mysql}:${env.DB_PORT:3306}/${env.DB_DATABASE:keycloak}${env.JDBC_PARAMS:}</connection-url>
    <driver>mysql</driver>
    <pool>
        <flush-strategy>IdleConnections</flush-strategy>
        <min-pool-size>1</min-pool-size>
        <max-pool-size>200</max-pool-size>
        <initial-pool-size>10</initial-pool-size>
        <use-strict-min>true</use-strict-min>
    </pool>
    <timeout>
        <idle-timeout-minutes>15</idle-timeout-minutes>
    </timeout>
    <security>
        <user-name>${env.DB_USER:keycloak}</user-name>
        <password>${env.DB_PASSWORD:password}</password>
    </security>
    <validation>
        <check-valid-connection-sql>SELECT 1</check-valid-connection-sql>
        <background-validation>true</background-validation>
        <background-validation-millis>6000</background-validation-millis>
    </validation>
</datasource>
```
To increase timeout for slow `MYSQL` running (when keycloak-liquibase works):
```xml
<coordinator-environment default-timeout="5000"
statistics-enabled=
"${wildfly.transactions.statistics-enabled:${wildfly.statistics-enabled:false}}"/>
```
2. Migration configuration.   
[Link](https://www.keycloak.org/docs/latest/server_installation/#database-configuration) to detailed information about configuration options 
```xml
<spi name="connectionsJpa">
    <provider name="default" enabled="true">
        <properties>
            <property name="dataSource" value="java:jboss/datasources/KeycloakDS" />
            <property name="initializeEmpty" value="true" />
            <property name="migrationStrategy" value="update" />
            <property name="migrationExport" value="${jboss.home.dir}/keycloak-database-update.sql" />
        </properties>
    </provider>
</spi>
```
Also can be used `json`-configuration to init database: 
***!NOTE*** Users and secrets are neither exported or imported
```dockerfile
    ...
    volumes:
      - ./keycloak/imports:/opt/jboss/keycloak/imports
    command:
      - "-Dkeycloak.migration.action=import"
      - "-Dkeycloak.migration.provider=singleFile"
      - "-Dkeycloak.migration.file=/opt/jboss/keycloak/imports/local-realm.json"
      - "-Dkeycloak.migration.strategy=IGNORE_EXISTING"
```

3. CORS support
```xml
  <subsystem xmlns="urn:jboss:domain:undertow:6.0" default-server="default-server" default-virtual-host="default-host" default-servlet-container="default" default-security-domain="other">
  <buffer-cache name="default"/>
  <server name="default-server">
    <http-listener name="default" socket-binding="http" redirect-socket="https" proxy-address-forwarding="${env.PROXY_ADDRESS_FORWARDING:false}" enable-http2="true"/>
    <https-listener name="https" socket-binding="https" proxy-address-forwarding="${env.PROXY_ADDRESS_FORWARDING:false}" security-realm="ApplicationRealm" enable-http2="true"/>
    <host name="default-host" alias="localhost">
      <location name="/" handler="welcome-content"/>
      <http-invoker security-realm="ApplicationRealm"/>
      <filter-ref name="server-header"/>
      <filter-ref name="x-powered-by-header"/>
      <filter-ref name="Access-Control-Allow-Origin"/>
      <filter-ref name="Access-Control-Allow-Methods"/>
      <filter-ref name="Access-Control-Allow-Headers"/>
      <filter-ref name="Access-Control-Allow-Credentials"/>
      <filter-ref name="Access-Control-Max-Age"/>
    </host>
  </server>
  <servlet-container name="default">
    <jsp-config/>
    <websockets/>
  </servlet-container>
  <handlers>
    <file name="welcome-content" path="${jboss.home.dir}/welcome-content"/>
  </handlers>
  <filters>
    <response-header name="server-header" header-name="Server" header-value="WildFly/10"/>
    <response-header name="x-powered-by-header" header-name="X-Powered-By" header-value="Undertow/1"/>
    <response-header name="Access-Control-Allow-Origin" header-name="Access-Control-Allow-Origin" header-value="*"/>
    <response-header name="Vary" header-name="Vary" header-value="Origin"/>
    <response-header name="Access-Control-Allow-Methods" header-name="Access-Control-Allow-Methods" header-value="GET, POST, OPTIONS, PUT, DELETE"/>
    <response-header name="Access-Control-Allow-Headers" header-name="Access-Control-Allow-Headers" header-value="accept,authorization,locale,content-type,x-requested-with"/>
    <response-header name="Access-Control-Allow-Credentials" header-name="Access-Control-Allow-Credentials" header-value="true"/>
    <response-header name="Access-Control-Max-Age" header-name="Access-Control-Max-Age" header-value="1"/>
  </filters>
</subsystem>
```
4. Initial data and roles
   **Keycloak boils down to three simple terms:**
- realm:
  secures and manages security metadata for a set of users, application, and registered auth clients.
- client:
  clients are entities that can request authentication of a user within a realm.
- role:
  identify a type or category of user. Keycloak often assigns access and permissions to specific roles rather than
  individual users for a fine-grained access control.
  **Keycloak offers three types of roles:**
- Realm-level roles are in global namespace shared by all clients.
- Client roles have basically a namespace dedicated to a client.
- A composite role is a role that has one or more additional roles associated with it.
