# Add Kong Admin API as services:
curl --location --request POST 'http://kongproxy:8001/services/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "admin-api",
    "host": "localhost",
    "port": 8001
}'

# Add Admin API route
curl --location --request POST 'http://kongproxy:8001/services/admin-api/routes' \
--header 'Content-Type: application/json' \
--data-raw '{
    "paths": ["/admin-api"]
}'

# Check
curl kongproxy:8000/admin-api/

# Enable Key Auth Plugin
curl -X POST http://kongproxy:8001/services/admin-api/plugins \
    --data "name=key-auth"

# Add Konga as Consumer
curl --location --request POST 'http://kongproxy:8001/consumers/' \
--form 'username=konga' \
--form 'custom_id=cebd360d-3de6-4f8f-81b2-31575fe9846a'

# Create API Key for Konga
# ID from response 1d2b43a1-1fff-4545-91f4-8e4daa3d3ad1
#curl --location --request POST 'http://kongproxy:8001/consumers/1d2b43a1-1fff-4545-91f4-8e4daa3d3ad1/key-auth'
