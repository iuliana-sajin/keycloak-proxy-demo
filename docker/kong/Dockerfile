FROM kong:2.5.0
ENV JWT_PLUGIN_VERSION=1.1.0-1
ENV OIDC_PLUGIN_VERSION=1.2.3-2

#https://stackoverflow.com/questions/63351505/using-kong-api-gateway-key-auth-plugin-with-keycloak-protected-rest-apis
USER root
# the package manager for Lua modules
RUN apk update && apk add git unzip luarocks
RUN luarocks install lua-resty-jwt
RUN luarocks install lua-resty-openidc

RUN git clone https://github.com/revomatico/kong-oidc.git \
     && cd kong-oidc \
     && luarocks make
RUN luarocks pack kong-oidc ${OIDC_PLUGIN_VERSION} \
 && luarocks install kong-oidc-${OIDC_PLUGIN_VERSION}.all.rock

#This project is a fork of https://github.com/gbbirkisson/kong-plugin-jwt-keycloak
#that has been improved with functionality for working with kong-oidc plugin.
#The kong-oidc plugin adds an additional X-Userinfo, X-Access-Token and X-Id-Token headers to the upstream request,
#which can be consumed in jwt-keycloak plugin. All of them are base64 encoded.
RUN git clone https://github.com/PSheshenya/kong-plugin-jwt-keycloak.git \
 && cd kong-plugin-jwt-keycloak \
 && luarocks make
RUN luarocks pack kong-plugin-jwt-keycloak ${JWT_PLUGIN_VERSION} \
 && luarocks install kong-plugin-jwt-keycloak-${JWT_PLUGIN_VERSION}.all.rock

USER kong