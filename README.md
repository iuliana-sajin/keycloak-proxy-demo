## Project description 

### Application:
- demo application is written using java. maven and spring-boot;   
- all `FE` resources are under path `src/main/resources/static`;   
- to start application separately execute (maven should be installed):   
```shell
mvn clean spring-boot:run
```
- application runs on port `8081` 

Application API:  
- `/customers/**` - secured, accessible only by users with role `CUSTOMER`  
- `/customers` - returns customers page;  
- `/customers/me` - returns information about current user; `X-Auth-Token`;   
- `/customers/list` - returns existing customers;  

- `/merchants/**` - secured, accessible only by users with role `MERCHANT`
- `/merchants` - returns merchants page;
- `/merchants/me` - returns information about current user; `X-Auth-Token`;
- `/merchants/list` - returns existing merchants;

- `/common/info` -  secured, accessible by users with role `CUSTOMER`/`MERCHANT`;

- `/info` - unsecured;

### Keycloak
User `admin`   
Password `admin`   
Run on port `8080`   
Url `http://keyadmin:8080/auth/` --> Administration console  
Both clients for `Gatekeeper` and `Kong` are already configured   

Kong configuration:
- client id :`Kong`;
- secret: `f4c2b016-f073-427d-b3bd-a6a06ee98d7d`;
- realm :`local`

Configured users:  
1. Username `johndoe`, password `password`, role `CUSTOMER`;
2. Username `adamSmith`, password `password`, role `MERCHANT`;

### Konga 
User `admin`  
Password `password`  
Run on port `1337`  
Url `http://konga:1337/#!/login`   

All endpoints from `Application API` are already configured (Services + Routes + OIDC plugin)  

All services under `/customers/**` are configured **without** redirects, 401 is returned  
Other services (`/merchants/**`, and `/common/**`) use redirects to Keycloak login  

Kong is running on port `8000`/`8001`  

## Before starting 
Add to `etc/hosts`:
```shell
127.0.0.1       keyadmin
localhost       keyadmin
127.0.0.1       keyapp
localhost       keyapp
127.0.0.1       kongproxy
localhost       kongproxy
127.0.0.1       konga
localhost       konga
127.0.0.1       gateproxy
localhost       gateproxy
```

## Container management

Run `docker-compose` command under `docker` folder or specify relative path to `yaml`-file 

1. Build and start containers:    
   `docker-compose -f docker-compose.yaml -f kong-docker-compose.yaml -p kg  up --remove-orphans`   
   or start existing containers  
   `docker-compose start`   
   - to rebuild images use flag `--build` 
   - to run in daemon mode use flag `-d`
   
2. Stop containers:  
   `docker-compose stop -t 3600`     
   To avoid having the SonarQube instance killed by the Docker daemon after 10 seconds,   
   it is best to configure a timeout to stop the container with `-t `  

3. Remove containers:  
   `docker-compose rm`

#### Troubleshouting
Set the recommended values for the current session by running the following commands as root on the host
```shell
sysctl -w vm.max_map_count=262144
sysctl -w fs.file-max=65536 
```

## Database Dump

1. Keycloak database dump:
```shell
 docker exec <container_name> /usr/bin/mysqldump -u root --password=root_password keycloak > keyclaok_dump.sql
```
2. Kong database dump:
```shell
  docker exec -it <container_name> pg_dump kong  -U kong > kong_dump.sql 
```
3. Konga database dump:
```shell
  docker exec -it <container_name> pg_dump konga -U kong > konga_dump.sql
```

4. Kong setup dump (to run in dbless mode)
```shell
  docker run --network=kg_kongnetwork -v /home/path/:/mnt/deck/ kong/deck --kong-addr=http://kongproxy:8001 dump -o /mnt/deck/dump.yaml
```