package com.muume.key;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.muume.key.dto.KeyUser;
import com.muume.key.utils.ParseRequestUtils;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class TokenDecodeTest {
    String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJqTnBTZnhUTnM2cmdicVN5d19RSHpscXgxZ3BCQTVv" +
            "TVROZlY5M0ROV3lZIn0.eyJleHAiOjE2MzE4NjU1MDEsImlhdCI6MTYzMTg2NTIwMSwiYXV0aF90aW1lIjoxNjMxODY1MjAxL" +
            "CJqdGkiOiIzNWFmNDU3OC00MDViLTQ2NzgtYjMyYi02MTE2ZTQyMzIyNmMiLCJpc3MiOiJodHRwOi8va2V5YWRtaW46ODA4MC" +
            "9hdXRoL3JlYWxtcy9sb2NhbCIsImF1ZCI6WyJnYXRla2VlcGVyIiwiYWNjb3VudCIsImtvbmciXSwic3ViIjoiNWNlZDNmZDE" +
            "tNTg4ZS00YjIxLWFkYjItOTEzN2E4MDgxYWU0IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZ2F0ZWtlZXBlciIsInNlc3Npb25f" +
            "c3RhdGUiOiJiZWUyNGRiNy0wNzg1LTQ5NjYtYTQ3Ni0xZWYzYTA2ZjMzYzkiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zI" +
            "jpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiQ1VTVE9NRVIiLCJ1bWFfYXV0aG9yaX" +
            "phdGlvbiIsImRlZmF1bHQtcm9sZXMtbG9jYWwiXX0sInJlc291cmNlX2FjY2VzcyI6eyJnYXRla2VlcGVyIjp7InJvbGVzIjp" +
            "bIkNVU1RPTUVSIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3Mi" +
            "LCJ2aWV3LXByb2ZpbGUiXX0sImtvbmciOnsicm9sZXMiOlsiQ1VTVE9NRVIiXX19LCJzY29wZSI6Im9wZW5pZCBlbWFpbCBwc" +
            "m9maWxlIiwic2lkIjoiYmVlMjRkYjctMDc4NS00OTY2LWE0NzYtMWVmM2EwNmYzM2M5IiwiZW1haWxfdmVyaWZpZWQiOmZhbH" +
            "NlLCJuYW1lIjoiSm9obiBEb2UiLCJncm91cHMiOlsibXktYXBwIl0sInByZWZlcnJlZF91c2VybmFtZSI6ImpvaG5kb2UiLCJ" +
            "naXZlbl9uYW1lIjoiSm9obiIsImZhbWlseV9uYW1lIjoiRG9lIiwiZW1haWwiOiJqb2huZG9lQGVtYWlsLmNvbSJ9.ZQ2Nzg2" +
            "DUXLcnTXHyHs4gZ9I9PKY0-PNdGEm9zl7RaA4OGTKcF4CEj5LTGwPsB3h6eNfVIQv__zNqo4XkHWcjbh103hqg_aloL254_6H" +
            "RZ3bARoXAhj8YTS9PjP6LGLOciF32YkZ3FWqSGiVwjyCtLRctpYFHm1BmF1mEOfKnbZOJDDeT_L6HA-0mn3PKT2Lg-88jZq_o" +
            "gpA2wbMQzXyhfUrzPeYIPca6VcuRN80rDuqYYc3vMojy7IDlDutFjr0q3eXTDoZR5HrQ5sOoOQYsZmpOEeBLNgn-qC84fXEGQ" +
            "a3D0Z_n_5xFz9fnY3BwTms4y2nk8-anZMP6E6fSuSZvA";

    @Test
    public void testDecode() throws Exception {
        DecodedJWT decoded = JWT.decode(token);
        Map<String, Claim> claims = decoded.getClaims();
        claims.forEach((key, value) -> System.out.println(key + ", " + value));
        System.out.println("END CLAIMS");
        System.out.println("Header=" + decoded.getHeader());
        System.out.println("Payload=" + decoded.getPayload());
        System.out.println("Token=" + decoded.getToken());
        System.out.println("Signature=" + decoded.getSignature());
        System.out.println("Algorithm=" + decoded.getAlgorithm());
        System.out.println("Content type=" + decoded.getContentType());
        System.out.println("Type=" + decoded.getType());
        System.out.println("Subject=" + decoded.getSubject());
        System.out.println("Audience=" + decoded.getAudience());

        Claim realmAccess = decoded.getClaim("realm_access");
        Map<String, Object> stringObjectMap = realmAccess.asMap();
        System.out.println(stringObjectMap.get("roles"));
    }

    @Test
    public void testDecode_toUser() throws Exception {
        DecodedJWT decoded = JWT.decode(token);
        KeyUser user = ParseRequestUtils.getUser(decoded);

        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo("bee24db7-0785-4966-a476-1ef3a06f33c9");
        assertThat(user.getUsername()).isEqualTo("johndoe");
        assertThat(user.getFirstName()).isEqualTo("John");
        assertThat(user.getLastName()).isEqualTo("Doe");
        assertThat(user.getEmail()).isEqualTo("johndoe@email.com");
        assertThat(user.getRealmRoles()).hasSize(4)
                .containsExactlyInAnyOrder("offline_access",
                        "CUSTOMER",
                        "uma_authorization",
                        "default-roles-local");
        assertThat(user.getGroups()).hasSize(1)
                .contains("my-app");
        assertThat(user.isEmailVerified()).isFalse();
    }
}
