package com.muume.key.controller;

import com.muume.key.dto.KeyUser;
import com.muume.key.model.Merchant;
import com.muume.key.persistance.MerchantRepository;
import com.muume.key.utils.ParseRequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/merchants")
public class MerchantController {

    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping(value = "/list")
    @ResponseBody
    public List<Merchant> listAll(HttpServletRequest request) {
        List<Merchant> merchants = new ArrayList<>();
        merchantRepository.findAll().forEach(merchants::add);
        return merchants;
    }

    @GetMapping(value = "/me")
    @ResponseBody
    public KeyUser me(HttpServletRequest request) {
        return ParseRequestUtils.getUser(request);
    }
}
