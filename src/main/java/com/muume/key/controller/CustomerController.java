package com.muume.key.controller;

import com.muume.key.dto.KeyUser;
import com.muume.key.model.Customer;
import com.muume.key.persistance.CustomerRepository;
import com.muume.key.utils.ParseRequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(value = "/list")
    @ResponseBody
    public List<Customer> listAll(HttpServletRequest request) throws Exception {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findAll().forEach(customers::add);
        return customers;
    }

    @GetMapping(value = "/me")
    @ResponseBody
    public KeyUser me(HttpServletRequest request) {
        return ParseRequestUtils.getUser(request);
    }
}
