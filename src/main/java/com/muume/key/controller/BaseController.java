package com.muume.key.controller;

import com.muume.key.dto.KeyUser;
import com.muume.key.utils.ParseRequestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BaseController {

    @GetMapping(path = "/info")
    @ResponseBody
    public String getInfo() {
        return "Some generic info. Unsecured";
    }

    @GetMapping(path = "/common/info")
    @ResponseBody
    public KeyUser commonUserInfo(HttpServletRequest request) {
        return ParseRequestUtils.getUser(request);
    }
}
