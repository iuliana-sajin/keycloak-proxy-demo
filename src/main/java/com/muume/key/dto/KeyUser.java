package com.muume.key.dto;

import com.auth0.jwt.interfaces.Claim;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyUser implements Serializable {
    private String id;
    private String username;
    private String name;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> realmRoles;
    private List<String> groups;
    private boolean emailVerified;

    private KeyUser() {
    }

    private KeyUser(Builder builder) {
        this.id = builder.id;
        this.username = builder.username;
        this.name = builder.name;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.realmRoles = builder.realmRoles;
        this.groups = builder.groups;
        this.emailVerified = builder.emailVerified;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public List<String> getGroups() {
        return groups;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    @Override
    public String toString() {
        return "KeyUser{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", groups=" + groups +
                ", emailVerified=" + emailVerified +
                '}';
    }

    public static class Builder {
        private String id;
        private String username;
        private String name;
        private String firstName;
        private String lastName;
        private String email;
        private List<String> realmRoles;
        private List<String> groups;
        private boolean emailVerified;

        public Builder setId(Claim id) {
            this.id = Optional.ofNullable(id)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setUsername(Claim username) {
            this.username = Optional.ofNullable(username)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setName(Claim name) {
            this.name = Optional.ofNullable(name)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setFirstName(Claim firstName) {
            this.firstName = Optional.ofNullable(firstName)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setLastName(Claim lastName) {
            this.lastName = Optional.ofNullable(lastName)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setEmail(Claim email) {
            this.email = Optional.ofNullable(email)
                    .map(Claim::asString)
                    .orElse(null);
            return this;
        }

        public Builder setRealmRoles(Claim roles) {
            this.realmRoles = Optional.ofNullable(roles)
                    .map(Claim::asMap)
                    .map(map -> (List<String>)map.get("roles"))
                    .orElse(null);
            return this;
        }

        public Builder setGroups(Claim groups) {
            this.groups = Optional.ofNullable(groups)
                    .map(c -> c.asList(String.class))
                    .orElse(null);
            return this;
        }

        public Builder setEmailVerified(Claim emailVerified) {
            this.emailVerified = Optional.ofNullable(emailVerified)
                    .map(Claim::asBoolean)
                    .orElse(false);
            return this;
        }

        public KeyUser build() {
            return new KeyUser(this);
        }
    }
}
