package com.muume.key.dto;

public class JwtClaims {

    public static final String KEY_USER_ID = "sid";
    public static final String KEY_USERNAME = "preferred_username";
    public static final String FIRST_NAME= "given_name";
    public static final String LAST_NAME = "family_name";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String EMAIL_VERIFIED = "email_verified";
    public static final String REALM_ROLES = "realm_access";
    public static final String GROUPS = "groups";
}
