package com.muume.key.persistance;

import com.muume.key.model.Customer;
import com.muume.key.model.Merchant;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
