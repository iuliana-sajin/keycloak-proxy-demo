package com.muume.key.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.muume.key.dto.KeyUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Optional;

import static com.muume.key.dto.JwtClaims.*;

public class ParseRequestUtils {
    public static final String KEYCLOAK_X_AUTH_TOKEN = "authorization";
    private static final Logger log = LoggerFactory.getLogger(ParseRequestUtils.class);

    public static String getHeaderValue(@Nonnull HttpServletRequest request,
                                       @Nonnull String headerName) {
        Objects.requireNonNull(request, "Http Request to parse");
        Objects.requireNonNull(headerName, "Http Header Name to find");

        return request.getHeader(headerName);
    }

    private static DecodedJWT getKeycloakJWT(@Nonnull HttpServletRequest request) {
        String headerValue = getHeaderValue(request, KEYCLOAK_X_AUTH_TOKEN);
        return Optional.ofNullable(headerValue)
                .map(value -> value.split(" "))
                .filter(p -> p.length > 1)
                .map(p -> p[1])
                .map(JWT::decode)
                .orElse(null);
    }

    @Nullable
    public static KeyUser getUser(DecodedJWT decodedToken) {
        if(decodedToken != null && decodedToken.getClaims() != null) {
           return new KeyUser.Builder()
                   .setId(decodedToken.getClaim(KEY_USER_ID))
                   .setUsername(decodedToken.getClaim(KEY_USERNAME))
                   .setName(decodedToken.getClaim(NAME))
                   .setFirstName(decodedToken.getClaim(FIRST_NAME))
                   .setLastName(decodedToken.getClaim(LAST_NAME))
                   .setEmail(decodedToken.getClaim(EMAIL))
                   .setRealmRoles(decodedToken.getClaim(REALM_ROLES))
                   .setGroups(decodedToken.getClaim(GROUPS))
                   .setEmailVerified(decodedToken.getClaim(EMAIL_VERIFIED))
                   .build();
        }
        return null;
    }

    @Nullable
    public static KeyUser getUser(@Nonnull HttpServletRequest servletRequest) {
        DecodedJWT keycloakJWT = getKeycloakJWT(servletRequest);
        return getUser(keycloakJWT);
    }

    public static void printHeaders(@Nonnull HttpServletRequest servletRequest) {
        Enumeration<String> headerNames = servletRequest.getHeaderNames();
        if(headerNames != null) {
            log.info("----------- REQUEST HEADERS ---------------------");
            while(headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                log.info("NAME={}, VALUE={}", name, servletRequest.getHeader(name));
            }
        }
    }

    public static void printCookies(@Nonnull HttpServletRequest servletRequest) {
        Cookie[] cookies = servletRequest.getCookies();
        if(cookies != null) {
            log.info("----------- REQUEST COOKIES ---------------------");
            for (Cookie cookie: cookies) {
                log.info("NAME={}, VALUE={}", cookie.getName(), cookie.getValue());
            }
        }
    }
}
