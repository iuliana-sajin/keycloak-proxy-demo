package com.muume.key.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "merchants")
public class Merchant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "company_name")
    private String companyName;
    private String email;
    private String phone;
    private String address;

    private  Merchant() {}

    private Merchant(Builder builder) {
        this.id = builder.id;
        this.companyName = builder.companyName;
        this.email = builder.email;
        this.phone = builder.email;
        this.address = builder.address;
    }

    public long getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public static class Builder {
        private long id;
        private String companyName;
        private String email;
        private String phone;
        private String address;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setCompanyName(String companyName) {
            this.companyName = companyName;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Merchant build() {
            return new Merchant(this);
        }
    }

}
