const config = {
    GATEKEEPER_PROXY: 'http://gateproxy:3000',
    KONGO_PROXY: 'http://kongproxy:8000'
};

var ROOT_URL = config.KONGO_PROXY;
var keycloak;

function getAuthHeaders() {
    if(keycloak) {
        return {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + keycloak.token
        }
    } else {
        console.info("No keycloak ");
        return {}
    }
}

$(document).ajaxError(function (e, xhr, settings) {
    if (xhr.status == 401) {
        keycloak.login();
//        window.location.href = config.KONGO_PROXY + "/";
    }
});

// https://www.keycloak.org/docs/latest/securing_apps/#_javascript_adapter
function init() {
    keycloak = Keycloak({
        url: "http://keyadmin:8080/auth",
        realm: "local",
        clientId: "pubkong",
        "cors-enabled":true
    });
    keycloak
    .init({onLoad: 'check-sso',
           silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html',
           checkLoginIframe: false })
    .success(function(authenticated) {
                 console.info(authenticated ? 'authenticated' : 'not authenticated');
             }).error(function(err) {
                 console.info('Keycloak failed to initialize');
                 console.info(err);
             });
}

function loadUser(url, func) {
    console.info("Selected proxy " + ROOT_URL);
    var url =  ROOT_URL + url + "/me";
    console.info("Send request to " + url);
    $.ajax({
          type: 'GET',
          url: url,
          headers: getAuthHeaders(),
          responseType:'application/json',
          success: function(data) {
            console.info("Response get /me");
            console.info(data);
            func(data)
          }
    });
}

function loadList(url, func) {
    console.info("Selected proxy " + ROOT_URL);
    var url =  ROOT_URL + url + "/list";
    console.info("Send request to " + url);

    $.ajax({
        type: 'GET',
        url: url,
        headers: getAuthHeaders(),
        responseType:'application/json',
        success: function(data) {
        console.info("Response get /list");
        console.info(data);
        func(data)
        }
    });
}

function setCustomersData(data) {
    if(data) {
        var trHTML = '';
        $.each(data, function (i, item) {
        trHTML += '<tr><td>' + item.id + '</td>' +
                   '<td>' + item.firstName + '</td>' +
                   '<td>' + item.lastName + '</td>'+
                   '<td>' + item.email + '</td>'+
                   '<td>' + item.phone + '</td>'+
                   '</tr>';
        });
        $('#customers').append(trHTML);
    }
}

function setUserData(data) {
    if(data) {
        $("#username").text(data.username);
        $("#keycloakId").text(data.id);
        $("#fullName").text(data.name);
        $("#firstName").text(data.firstName);
        $("#lastName").text(data.lastName);
        $("#email").text(data.email);
        $("#roles").text(data.realmRoles);
        $("#groups").text(data.groups);
    }
}

function setMerchantsData(data) {
    if(data) {
        var trHTML = '';
        $.each(data, function (i, item) {
        trHTML += '<tr><td>' + item.id + '</td>' +
                  '<td>' + item.companyName + '</td>' +
                  '<td>' + item.address + '</td>'+
                  '<td>' + item.email + '</td>'+
                  '<td>' + item.phone + '</td>'+
                  '</tr>';
        });
        $('#merchants').append(trHTML);
        }
    }

function loadCustomers() {
    let path = "/customers";
    $("#content").load("customers.html");
    loadUser(path, setUserData);
    loadList(path, setCustomersData)
}

function loadMerchants() {
    let path = "/merchants";
    $("#content").load("merchants.html");
    loadUser(path, setUserData);
    loadList(path, setMerchantsData)
}

function loadCommonInfo() {
    $("#content").load("common.html");
    console.info("Selected proxy " + ROOT_URL);
    var url =  ROOT_URL + "/common";
    console.info("Send request to " + url);

    $.ajax({
        type: 'GET',
        url: url,
        headers: getAuthHeaders(),
        success: function(data) {
            console.info(data);
             if(data) {
                 setUserData(data);
             }
          }
    });
}

function goBack() {
    $("#content").load("menu.html");
}

$( document ).ready(function() {
    $("#content").load("menu.html")
});