CREATE TABLE merchants(
id  INT PRIMARY KEY,
company_name VARCHAR(255),
email VARCHAR(255),
phone VARCHAR(255),
address VARCHAR(255)
);

INSERT INTO merchants(id, company_name, email, phone, address)
    VALUES
    (1, 'King Company', 'king@email.com', '+37369784511', 'Moldova,Chishinau, str Vieru, 71'),
    (2, 'Duke Company', 'duke@email.com', '+37369784512', 'Moldova,Chishinau, str Vieru, 72'),
    (3, 'Graf Company', 'graf@email.com', '+37369784513', 'Moldova,Chishinau, str Vieru, 73'),
    (4, 'Baron Company', 'baron@email.com', '+37369784514', 'Moldova,Chishinau, str Vieru, 74'),
    (5, 'Viscount Company', 'viscount@email.com', '+37369784515', 'Moldova,Chishinau, str Vieru, 75');

CREATE TABLE customers(
id  INT PRIMARY KEY,
first_name VARCHAR(255),
last_name VARCHAR(255),
phone VARCHAR(255),
email VARCHAR(255)
);

INSERT INTO customers(id, first_name, last_name, phone, email)
    VALUES
    (1, 'John', 'Doe', '+37369365211', 'johnDoe@email.com'),
    (2, 'Adam', 'Smith', '+37369365212', 'adamSmith@email.com'),
    (3, 'Sabrina', 'Spellman', '+37369365213', 'sabrinaSpellman@email.com');